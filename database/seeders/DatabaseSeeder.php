<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	public static $seeders = [];
	
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints(); // برای غیرفعال کردن کلیدهای خارجی

        foreach (self::$seeders as $seeder) {
            $this->call($seeder);
        }

        Schema::enableForeignKeyConstraints(); // برای فعال کردن کلیدهای خارجی
    }
}
