<div>
    @php $is_error = null; @endphp
    @error($name)
        @php
            $is_error = 'is-invalid';
        @endphp
    @enderror
    <input name="{{ $name }}" {{ $attributes->merge(['class' => "form-control $is_error"]) }}>
    <x-validation-error field="{{ str_replace(']', '', str_replace('[', '.', $name)) }}"/>
</div>
