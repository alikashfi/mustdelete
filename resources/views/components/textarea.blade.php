@php $is_error = null; @endphp
@error($name)
@php
    $is_error = 'is-invalid';
@endphp
@enderror
<textarea name="{{ $name }}" {{ $attributes->merge(['class' => "form-control $is_error"]) }}>{!! isset($value) ? $value : old($name) !!}</textarea>
<x-validation-error field="{{ $name }}"/>
