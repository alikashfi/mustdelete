@php $is_error = null; @endphp
@error($name)
    @php
        $is_error = 'is-invalid';
    @endphp
@enderror
<select name="{{ $name }}" {{ $attributes->merge(['class' => "form-select $is_error"]) }}>
    {{ $slot }}
</select>
<x-validation-error field="{{ $name }}" />
