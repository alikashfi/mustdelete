<div class="form-check  {{ $class }}">
    <input name="{{ $name }}" class="form-check-input @error($name) is-invalid @enderror" {{ $select }} {{ $attributes() }}>
    <x-validation-error field="{{ $name }}"/>
    <label class="form-check-label" for="{{ $name . "-id" }}">
        {{ $placeholder }}
    </label>

</div>
