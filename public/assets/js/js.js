/* region Toast */
function notification(message, type = 'success'){
    $.RezaToast(type, message, {
        has_icon:true,
        has_close_btn:true,
        stack: true,
        fullscreen:false,
        timeout:5000,
        sticky:false,
        has_progress:true,
        rtl:false,
    });
}
/* endregion Toast */


$( document ).ready(function() {

    /* ----------- file-upload-preview */
    $('input[type="file"].file-upload').on('change',function(){
        $('.file-upload-preview').removeClass('d-none');
        $('#filename').html($(this).val().replace(/.*(\/|\\)/, ''));
        let filename = $(this).val().replace(/.*(\/|\\)/, '');
        let dot = filename.length > 50 ? '...' : '';
        $('#filename').html(filename.substring(0,50) + dot);
    });
    /* ----------- file-upload-preview */

    /* region deleteItemTable */
    $(document).on('click', '.deleteItem', function(e) {
        let _this = $(this);
        let route = $(_this).data('route');
        e.preventDefault();
        if (confirm('آیا از انجام اینکار اطمینان دارید؟')) {

            $.ajax({
                type: 'POST',
                url: route,
                dataType : 'json',
                data: {
                    _method: "delete",
                    _token: $('meta[name="_token"]').attr('content')
                },
                success: function(data, textStatus, xhr)
                {
                    if (xhr.status == 200) {
                        notification(data.message);
                        $(_this).closest('tr').remove();
                    }
                },
                error:function(req, status, error)
                {
                    notification('خطایی رخ داد!', 'error');
                }
            });
        }
    });
    /* endregion deleteItemTable */

    /* region UpdateStatusTable */
    $(document).on('click', '.updateStatus', function(e) {
        let _this = $(this);
        let route = $(_this).data('route');
        let status = $(_this).data('status');
        let field = $(_this).data('field');
        e.preventDefault();
        if (confirm('آیا از انجام اینکار اطمینان دارید؟')) {

            $.ajax({
                type: 'POST',
                url: route,
                dataType : 'json',
                data: {
                    _method: "PATCH",
                    _token: $('meta[name="_token"]').attr('content')
                },
                success: function(data, textStatus, xhr)
                {
                    if (xhr.status == 200) {
                        $(_this).closest('tr').find('#' + field).html(status);
                        notification(data.message);
                    }
                },
                error:function(req, status, error)
                {
                    notification('خطایی رخ داد!', 'error');
                }
            });
        }
    });
    /* endregion UpdateStatusTable */

    /* region select paymentType */
    $(document).on('change', '#payment-type-select #online-payment, #upload-payment-file', function(e) {
        let _this = $(this);

        if (_this.val() == "file") {
            $('#payment-box #file').removeClass('d-none');
            $('#payment-box #online').addClass('d-none');
        } else {
            $('#payment-box #online').removeClass('d-none');
            $('#payment-box #file').addClass('d-none');
        }
    });
    /* endregion select paymentType */
});
