(function(){
    "use strict";
    $.RezaToast = function(type, message, options){
        var defaultOptions = {
            appendTo: "body",
            stack: false,
            position_class: "toast-bottom-right",
            fullscreen:false,
            width: 300,
            spacing:20,
            timeout: 4000,
            has_close_btn:true,
            has_icon:true,
            sticky:false,
            border_radius:6,
            has_progress:false,
            rtl:false
        }

        var class_type = null;
        var icon_type = null;
        switch (type) {
            case 'success':
                class_type = 'success';
                icon_type = '<i class="bi bi-check-circle fs-1 px-10"></i>';
                break;
            case 'error':
                class_type = "error";
                icon_type = '<i class="bi bi-exclamation-circle fs-1 px-10"></i>';
                break;
            case 'warning':
                class_type = "warning";
                icon_type = '<i class="bi bi-exclamation-triangle-fill fs-1 px-10"></i>';
                break;
            case 'info':
                class_type = "info";
                icon_type = '<i class="bi bi-bell-fill fs-1 px-10"></i>';
                break;
            default : class_type = "success";
        }

        var $element = null;

        var $options = $.extend(true, {}, defaultOptions, options);

        var spacing = $options.spacing;

        var css = {
            "position":($options.appendTo == "body") ? "fixed" : "absolute",
            "width":$options.width,
            "display":"none",
            "border-radius":$options.border_radius,
            "z-index":99999
        }

        $element = $('<div class="reza-toast ' + class_type + ' ' + $options.position_class + '"></div>');
        $('<div class="d-flex justify-content-between align-items-center px-2 py-1 border-bottom">' +
            '<div class="d-flex justify-content-center align-items-center">' +
            '<i class="bi bi-chat-right-dots d-flex fs-6"></i>'+
            '<span class="ms-2">Notification</span>'+
            '</div>' +
            '<i class="cursor-pointer bi bi-x close-icon d-flex fs-4"></i>' +
            '</div>')
            .appendTo($element);
        $('<div class="d-flex justify-content-between align-items-center px-2 py-2">' +
            '<span class="toast-text-justify pe-3">'+ message +'</span>' +
            icon_type +
            '</div>')
            .appendTo($element);

        if($options.fullscreen){
            $element.addClass( "fullscreen" );
        }

        if($options.rtl){
            $element.addClass( "rtl" );
        }


        if($options.has_progress && $options.timeout > 0){
            $('<div class="toast-progress"></div>').appendTo($element);
        }

        if($options.sticky){
            $options.spacing = 0;
            spacing = 0;

            switch($options.position_class){
                case "toast-top-left" : {
                    css["top"] = 0;
                    css["left"] = 0;
                    break;
                }
                case "toast-top-right" : {
                    css["top"] = 0;
                    css["left"] = 0;
                    break;
                }
                case "toast-top-center" : {
                    css["top"] = 0;
                    css["left"] = css["right"] = 0;
                    css["width"] = "100%";
                    break;
                }
                case "toast-bottom-left" : {
                    css["bottom"] = 0;
                    css["left"] = 0;
                    break;
                }
                case "toast-bottom-right" : {
                    css["bottom"] = 0;
                    css["right"] = 0;
                    break;
                }
                case "toast-bottom-center" : {
                    css["bottom"] = 0;
                    css["left"] = css["right"] = 0;
                    css["width"] = "100%";
                    break;
                }
                default : {
                    break;
                }
            }
        }

        if($options.stack){
            if($options.position_class.indexOf("toast-top") !== -1 ){
                $($options.appendTo).find('.reza-toast').each(function(){
                    css["top"] = parseInt($(this).css("top")) + this.offsetHeight + spacing;
                });
            } else if($options.position_class.indexOf("toast-bottom") !== -1 ){
                $($options.appendTo).find('.reza-toast').each(function(){
                    css["bottom"] = parseInt($(this).css("bottom")) + this.offsetHeight + spacing;
                });
            }
        }

        $element.css(css);

        $element.appendTo($options.appendTo);

        if($element.fadeIn) {
            $element.fadeIn();
        }else {
            $alert.css({display: 'block', opacity: 1});
        }

        function removeToast(){
            $.RezaToast.remove( $element );
        }

        if($options.timeout > 0){
            setTimeout(removeToast, $options.timeout);
            if($options.has_progress){
                $(".toast-progress", $element).animate({"width":"100%"}, $options.timeout);
            }
        }

        $(".toast-close", $element).click(removeToast)

        return $element;
    }

    $.RezaToast.remove = function( $element ){
        "use strict";
        if($element.fadeOut)
        {
            $element.fadeOut(function(){
                return $element.remove();
            });
        }
        else{
            $element.remove();
        }
    }
})();
