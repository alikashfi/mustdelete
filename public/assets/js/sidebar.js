function sidebarStatus(status) {
    let bodyDirection = $('body').data('dir');
    let sidebarShowType = bodyDirection == 'fa' ? 'menu-show-right' : 'menu-show-left';

    var mySideBar = $('#reza-sidebar');

    var sideBar = new bootstrap.Offcanvas(mySideBar);

    if (status == true) {
        sideBar.show();
        $('body').addClass(sidebarShowType);

    } else {
        $('#reza-sidebar').removeClass('show');
        $('#reza-sidebar').removeClass('is-show');
        $('body').removeClass(sidebarShowType);

    }
}

function sidebar() {
    let windowSize = $(window).width();
    if (parseInt(windowSize) <= 768) {
        $('#reza-sidebar').attr('data-bs-backdrop', true);
        sidebarStatus(false);
    }
    if (parseInt(windowSize) >= 768 && parseInt(windowSize) <= 992) {
        $('#reza-sidebar').attr('data-bs-backdrop', false);
        sidebarStatus(true);
    }
    if (parseInt(windowSize) >= 992) {
        $('#reza-sidebar').attr('data-bs-backdrop', false);
        sidebarStatus(true);
    }
}

$( document ).ready(function() {

    /* region SideBar customized */
    sidebar();

    $( window ).resize(function() {
        sidebar();
    });


    $(document).on('click', '#btn-reza-sidebar', function (e) {
        let windowSize = $(window).width();
        let bodyDirection = $('body').data('dir');
        let sidebarShowType = bodyDirection == 'fa' ? 'menu-show-right' : 'menu-show-left';

        let sideBarVisibility = $('#reza-sidebar').hasClass('show');

        if (parseInt(windowSize) >= 768 && parseInt(windowSize) <= 992) {
            if (sideBarVisibility) {
                $('body').addClass(sidebarShowType);
            } else {
                $('body').removeClass(sidebarShowType);
            }
        }

        if (parseInt(windowSize) >= 992) {
            if (sideBarVisibility) {
                $('body').addClass(sidebarShowType);
            } else {
                $('body').removeClass(sidebarShowType);
            }
        }
    });
    /* endregion SideBar customized */

});
