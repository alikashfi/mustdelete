<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth', 'verified'],
    'prefix' => 'discounts'
], function () {

    Route::get('/', [
        'uses' =>  'RezaDadashi\Discount\Http\Controllers\DiscountController@index',
        'as' => 'discounts.index'
    ]);

    Route::post('/', [\RezaDadashi\Discount\Http\Controllers\DiscountController::class, 'store'])->name('discounts.store');

    Route::delete('/{discount}', [\RezaDadashi\Discount\Http\Controllers\DiscountController::class, 'destroy'])->name('discounts.destroy');

    Route::get('/{discount}/edit', [\RezaDadashi\Discount\Http\Controllers\DiscountController::class, 'edit'])->name('discounts.edit');

    Route::patch('/{discount}/update', [\RezaDadashi\Discount\Http\Controllers\DiscountController::class, 'update'])->name('discounts.update');

    Route::get('/{discountCode}/{request}/check', [\RezaDadashi\Discount\Http\Controllers\DiscountController::class, 'check'])
        ->middleware('throttle:6,1') // 6 request for 1 minutes
        ->name('discounts.check');
});
