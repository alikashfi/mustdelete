@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('discounts.index') }}">@lang('discounts_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-8">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('discounts_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="dropdown bg-white border border-2 col-lg-4 my-2">
                            <div class="dropdown-toggle d-flex align-items-center justify-content-between p-2" data-bs-toggle="dropdown">@lang('discounts_search')</div>
                            <div class="dropdown-menu col-4 w-100 mt-1 p-3 shadow">
                                <form method="GET">
                                    <input type="text" name="code" value="{{ request('code') }}" placeholder="@lang('discounts_code')" class="form-control mb-3">
                                    <input type="number" name="percent" value="{{ request('percent') }}" placeholder="@lang('discounts_percent')" class="form-control mb-3">
                                    <input type="text" name="description" value="{{ request('description') }}" placeholder="@lang('discounts_description')" class="form-control mb-3">


                                    <div class="text-end">
                                        <button class="btn btn-primary" type="submit">
                                            <span>@lang('common_search')</span>
                                            <i class="bi bi-search text-white"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead class="">
                                    <tr>
                                        <th scope="col">@lang('discounts_code')</th>
                                        <th scope="col">@lang('discounts_percent')</th>
                                        <th scope="col">@lang('discounts_time_limitation')</th>
                                        <th scope="col">@lang('discounts_description')</th>
                                        <th scope="col">@lang('discounts_usage_limitation')</th>
                                        <th scope="col">@lang('discounts_number_uses')</th>
                                        <th scope="col">@lang('common_actions')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($discounts as $discount)
                                        <tr>
                                            <td>{{ $discount->code }}</td>
                                            <td>
                                                <span class="badge bg-danger">% {{ $discount->percent }}</span>
                                                <span class="badge {{ $discount->type == \RezaDadashi\Discount\Models\Discount::TYPE_ALL ? 'bg-success' : 'bg-warning' }}">
                                                    برای @lang("discount_" . $discount->type)
                                                </span>
                                            </td>
                                            <td>{{ $discount->expire_at ? getJalali($discount->expire_at) : '-' }}</td>
                                            <td>{{ $discount->description }}</td>
                                            <td>{{ $discount->usage_limitation ? $discount->usage_limitation : '-' }}</td>
                                            <td>{{ $discount->uses }}</td>
                                            <td>
                                                <a class="bi bi-pencil-square text-success fs-6 px-1" href="{{ route('discounts.edit', $discount->id) }}" title="ویرایش"></a>
                                                <a data-route="{{ route('discounts.destroy', $discount->id) }}" class="deleteItem bi bi-trash text-danger fs-6 px-1" href="javascript:;" title="حذف"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-4 mt-3 mt-lg-0">

            @include('Discount::create')

        </div>

    </div>
@endsection

