@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('discounts.index') }}">@lang('discounts_title')</a></li>
    <li class="breadcrumb-item"><a href="#">@lang('discounts_edit')</a></li>
@endsection

@section('content')
    <div class="p-3 card">
        <div class="card border-primary">
            <div class="card-header bg-primary text-white">
                @lang('discounts_edit')
            </div>
            <div class="card-body">
                <form action="{{ route('discounts.update', $discount->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="my-2">
                        <label for="title" class="form-label">@lang('discounts_code')</label>
                        <x-input type="text" value="{{ $discount->code }}" name="code" class="text-end"/>
                    </div>

                    <div class="my-2">
                        <label for="title" class="form-label">@lang('discounts_percent')</label>
                        <x-input type="number" value="{{ $discount->percent }}" name="percent" class="text-end"/>
                    </div>

                    <div class="my-2">
                        <label for="title" class="form-label">@lang('discounts_usage_limitation')</label>
                        <x-input type="number" value="{{ $discount->usage_limitation }}" name="usage_limitation"/>
                    </div>

                    <div class="my-2">
                        <label for="expire_at" class="form-label">@lang('discounts_time_limitation')</label>
                        <x-input id="expire_at" type="text"
                                 value="{{ $discount->expire_at ? \Morilog\Jalali\Jalalian::fromCarbon($discount->expire_at)->format('Y/m/d H:i') : null }}"
                                 name="expire_at"/>
                    </div>

                    <div class="bg-light p-3 border">@lang('discounts_for')</div>

                    <div id="select-type" class="bg-light mt-2 p-3 border">
                        <div class="form-check my-2">
                            <input class="@error('type') is-invalid @enderror form-check-input text-end" type="radio" name="type"
                                   id="all"
                                   value="all" {{ $discount->type == \RezaDadashi\Discount\Models\Discount::TYPE_ALL ? 'checked' : null }}>
                            <label class="form-check-label cursor-pointer" for="all">@lang('discount_all')</label>
                            <x-validation-error field="type"/>
                        </div>
                        <div class="form-check">
                            <input class="@error('type') is-invalid @enderror form-check-input" type="radio" name="type"
                                   value="special"
                                   id="user" {{ $discount->type == \RezaDadashi\Discount\Models\Discount::TYPE_SPECIAL ? 'checked' : null }}>
                            <label class="form-check-label cursor-pointer" for="user">@lang('discount_special')</label>
                            <x-validation-error field="type"/>
                        </div>
                        <div id="select-user" class="mt-3 visually-hidden">
                            <label for="users" class="form-label">@lang('discounts_select_users')</label>
                            <x-select name="users[]" id="users" class="select2 @error('users') is-invalid @error"
                                      multiple="multiple">
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}" {{ $discount->users->contains($user->id) ? 'selected' : null }}>{{ $user->name }}</option>
                                @endforeach
                            </x-select>
                            <x-validation-error field="users"/>
                        </div>
                    </div>

                    <div class="my-2">
                        <label for="title" class="form-label">@lang('discounts_description')</label>
                        <x-textarea value="{{ $discount->description }}" name="description"/>
                    </div>

                    <div class="text-end">
                        <button type="submit" class="btn btn-outline-success mt-2">@lang('common_update')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script src="{{ asset('/assets/persianDatePicker/js/persianDatepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/js/select2.min.js') }}"></script>
    <script>
        $("#expire_at").persianDatepicker({
            formatDate: "YYYY/0M/0D 0h:0m"
        });
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '#select-type #all, #select-type #user', function (e) {
                let _this = $(this);

                if (_this.val() == "all") {
                    $('#select-type #select-user').addClass('d-none');
                } else {
                    $('#select-type #select-user').removeClass('d-none');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endsection

@section("css")
    <link rel="stylesheet" href="{{ asset('/assets/persianDatePicker/css/persianDatepicker-default.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/select2.min.css') }}">
@endsection
