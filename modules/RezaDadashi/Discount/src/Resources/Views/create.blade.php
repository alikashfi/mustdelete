<div class="p-3 card">
    <div class="card border-success">
        <div class="card-header bg-success text-white">
            @lang('discounts_create')
        </div>
        <div class="card-body">
            <form action="{{ route('discounts.store') }}" method="post" autocomplete="off">
                @csrf
                <div class="my-2">
                    <label for="code" class="form-label">
                        @lang('discounts_code')
                        <span class="required">*</span>
                    </label>
                    <x-input id="code" type="text" value="{{ old('code') }}" name="code" class="text-end" />
                </div>

                <div class="my-2">
                    <label for="percent" class="form-label">
                        @lang('discounts_percent')
                        <span class="required">*</span>
                    </label>
                    <x-input id="percent" type="number" value="{{ old('percent') }}" name="percent" class="text-end" />
                </div>

                <div class="my-2">
                    <label for="title" class="form-label">@lang('discounts_usage_limitation')</label>
                    <x-input type="number" value="{{ old('usage_limitation') }}" name="usage_limitation" />
                </div>

                <div class="my-2">
                    <label for="expire_at" class="form-label">@lang('discounts_time_limitation')</label>
                    <x-input id="expire_at" type="text" value="{{ old('expired_at') }}" name="expire_at" />
                </div>

                <div class="bg-light p-3 border">
                    @lang('discounts_for')
                    <span class="required">*</span>
                </div>

                <div id="select-type" class="bg-light mt-2 p-3 border">
                    <div class="form-check my-2">
                        <input class="@error('type') is-invalid @enderror form-check-input" type="radio" name="type" id="all" value="all" @if(old('type') == 'all') checked @endif>
                        <label class="form-check-label cursor-pointer" for="all">@lang('discount_all')</label>
                        <x-validation-error field="type" />
                    </div>
                    <div class="form-check">
                        <input class="@error('type') is-invalid @enderror form-check-input" type="radio" name="type" value="special" id="user" @if(old('type') == 'special') checked @endif>
                        <label class="form-check-label cursor-pointer" for="user">@lang('discount_special')</label>
                        <x-validation-error field="type" />
                    </div>


                    <div id="select-user" class="mt-3 @if(is_null(old('users'))) visually-hidden @endif">
                        <label for="users" class="form-label">@lang('discounts_select_users')</label>
                        <x-select name="users[]" id="users" class="select2 @error('users') is-invalid @error" multiple="multiple">
                            @foreach($users as $user)
                                <option @if(!is_null(old('users')) && in_array($user->id, old('users'))) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </x-select>
                        <x-validation-error field="users" />
                    </div>
                </div>

                <div class="my-2">
                    <label for="title" class="form-label">@lang('discounts_description')</label>
                    <x-textarea value="{{ old('description') }}" name="description" />
                </div>

                <div class="text-end">
                    <button type="submit" class="btn btn-success mt-2">@lang('common_add')</button>
                </div>
            </form>
        </div>
    </div>
</div>


@section("js")
    <script src="{{ asset('/assets/persianDatePicker/js/persianDatepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/js/select2.min.js') }}"></script>
    <script>
        $("#expire_at").persianDatepicker({
            formatDate: "YYYY/0M/0D 0h:0m"
        });
    </script>
    <script>
        $( document ).ready(function() {
            $(document).on('change', '#select-type #all, #select-type #user', function(e) {
                let _this = $(this);

                if (_this.val() == "all") {
                    $('#select-type #select-user').addClass('visually-hidden');
                } else {
                    $('#select-type #select-user').removeClass('visually-hidden');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection

@section("css")
    <link rel="stylesheet" href="{{ asset('/assets/persianDatePicker/css/persianDatepicker-default.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/select2.min.css') }}">
@endsection
