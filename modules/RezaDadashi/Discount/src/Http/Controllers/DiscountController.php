<?php

namespace RezaDadashi\Discount\Http\Controllers;

use App\Http\Controllers\Controller;
use Morilog\Jalali\Jalalian;
use RezaDadashi\Core\Responses\AjaxResponses;
use RezaDadashi\Discount\Http\Requests\DiscountRequest;
use RezaDadashi\Discount\Models\Discount;
use RezaDadashi\Discount\Repositories\DiscountRepository;
use RezaDadashi\Discount\Services\DiscountService;
use RezaDadashi\Request\Repositories\RequestRepository;
use RezaDadashi\User\Repositories\UserRepository;

class DiscountController extends Controller
{
    private $userRepository;
    private $discountRepository;
    private $requestRepository;

    public function __construct(UserRepository $userRepository, DiscountRepository $discountRepository, RequestRepository $requestRepository)
    {
        $this->userRepository = $userRepository;
        $this->discountRepository = $discountRepository;
        $this->requestRepository = $requestRepository;
    }

    public function index()
    {
        /** Check Permission */
        $this->authorize('manage', Discount::class);

        $users = $this->userRepository->all();
        $discounts = $this->discountRepository
            ->searchCode(request('code'))
            ->searchPercent(request('percent'))
            ->searchDescription(request('description'))
            ->paginateLatest();
        return view('Discount::index', compact('users', 'discounts'));
    }

    public function store(DiscountRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Discount::class);

        $discount = $this->discountRepository->create([
            'code' => $request->code,
            'user_id' => auth()->id(),
            'percent' => $request->percent,
            'usage_limitation' => $request->usage_limitation,
            'expire_at' => $request->expire_at ? Jalalian::fromFormat("Y/m/d H:i", $request->expire_at)->toCarbon() : null,
            'description' => $request->description,
            'type' => $request->type,
            'uses' => 0,
        ]);

        if ($discount->type == Discount::TYPE_SPECIAL) {
            $discount->users()->sync($request->users);
        }

        newFeedback();
        return back();
    }

    public function destroy($discountId)
    {
        /** Check Permission */
        $this->authorize('manage', Discount::class);

        $discount = $this->discountRepository->findOrFail($discountId);
        $this->discountRepository->delete($discount);

        return AjaxResponses::successResponse();
    }

    public function edit($discountId)
    {
        /** Check Permission */
        $this->authorize('manage', Discount::class);

        $discount = $this->discountRepository->findOrFail($discountId);
        $users = $this->userRepository->all();

        return view('Discount::edit', compact('discount', 'users'));
    }

    public function update($discountId, DiscountRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Discount::class);

        $this->discountRepository->update(
            ['id' => $discountId],
            [
                'code' => $request->code,
                'percent' => $request->percent,
                'usage_limitation' => $request->usage_limitation,
                'expire_at' => $request->expire_at ? Jalalian::fromFormat("Y/m/d H:i", $request->expire_at)->toCarbon() : null,
                'description' => $request->description,
                'type' => $request->type,
            ]
        );

        $discount = $this->discountRepository->findOrFail($discountId);

        if ($discount->type == Discount::TYPE_SPECIAL) {
            $discount->users()->sync($request->users);
        } else {
            $discount->users()->sync([]);
        }

        newFeedback();
        return redirect()->route('discounts.index');
    }

    public function check($discountCode, $requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);
        $discount = $this->discountRepository->getByDiscountCode($discountCode);

        if ($discount) {
            $discountPercent = $discount->percent;
            $response = [
                'status' => 'valid',
                'payableAmount' => number_format($request->getFinalDepositAmount($discount->code)),
                'payableAmountDollar' => number_format($request->getFinalDepositAmountDollar($discount->code)),
                'discountPercent' => $discountPercent,
            ];

            return response()->json($response);
        }

        return response()->json([
            'status' => 'invalid'
        ])->setStatusCode(422);
    }
}
