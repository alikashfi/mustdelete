<?php

namespace RezaDadashi\Discount\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use RezaDadashi\Core\Rules\ValidJalaliDate;
use RezaDadashi\Core\Rules\ValidString;
use RezaDadashi\Discount\Models\Discount;

class DiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => ['required', 'min:4', 'max:50', 'unique:discounts,code', new ValidString()],
            'percent' => ['required', 'numeric', 'min:1', 'max:100'],
            'usage_limitation' => ['nullable', 'numeric', 'min:1', 'max:1000000'],
            'expire_at' => ['nullable', new ValidJalaliDate()],
            'type' => ['required', Rule::in(Discount::$types)],
            'users' => ['required_if:type,special', 'array'],
        ];

        if (request()->method == 'PATCH') {
            $rules['code'] = ['required', 'min:4', 'max:50', 'unique:discounts,code,' . request()->route('discount'), new ValidString()];
        }

        return $rules;
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'code' => 'کد تخفیف',
            'percent' => 'درصد تخفیف',
            'type' => 'نوع تخفیف',
            'users' => 'انتخاب کاربر'
        ] : [];
    }
}
