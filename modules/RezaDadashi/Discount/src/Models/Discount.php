<?php

namespace RezaDadashi\Discount\Models;

use Illuminate\Database\Eloquent\Model;
use RezaDadashi\User\Models\User;

class Discount extends Model
{
    protected $guarded = [];

    protected $casts = [
        'expire_at' => 'datetime'
    ];

    const TYPE_ALL = 'all';
    const TYPE_SPECIAL = 'special';
    public static $types = [
        self::TYPE_ALL,
        self::TYPE_SPECIAL
    ];

    public function users()
    {
        return $this->morphedByMany(User::class, 'discountable');
    }
}
