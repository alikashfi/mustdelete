<?php

namespace RezaDadashi\Discount\Services;

class DiscountService
{
    public static function calculateDiscountAmount($total, $percent)
    {
        return round(($total / 100) * $percent);
    }
}
