<?php

namespace RezaDadashi\Discount\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use RezaDadashi\Discount\Models\Discount;
use RezaDadashi\Discount\Policies\DiscountPolicy;
use RezaDadashi\RolePermissions\Models\Permission;

class DiscountServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/discount_routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Discount');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');

        Gate::policy(Discount::class, DiscountPolicy::class);
    }

    public function boot()
    {
        config()->set('sidebar.items.discount', [
            'icon' => 'bi bi-bookmark-star-fill',
            'title' => ['fa' => 'تخفیف ها', 'en' => 'Discount'],
            'url' => route('discounts.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_DISCOUNTS
            ]
        ]);
    }
}
