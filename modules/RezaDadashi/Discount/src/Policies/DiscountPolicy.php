<?php

namespace RezaDadashi\Discount\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Models\User;

class DiscountPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage($user)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_DISCOUNTS)) return true;
    }
}
