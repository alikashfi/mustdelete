<?php

namespace RezaDadashi\Discount\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Discount\Models\Discount;

class DiscountRepository extends Repository
{
    public function model()
    {
        return Discount::class;
    }

    public function used(Discount $discount)
    {
        if ($discount->usage_limitation > 0) {
            $discount->decrement('usage_limitation');
        }
        $discount->increment('uses');
        return $discount;
    }

    public function findByCode($code)
    {
        return Discount::query()->where('code', $code)->first();
    }

    public function getByDiscountCode($code)
    {
        return Discount::query()
            ->where(function ($query) {
                return $query
                    ->whereDate('expire_at', '>', now())
                    ->orWhereNull('expire_at');
            })
            ->where('code', $code)
            ->where(function ($query) {
                return $query->whereHas('users', function ($query) {
                    return $query->where('discountable_id', auth()->id());
                })->orWhereDoesntHave('users');
            })
            ->where(function ($query){
                $query->Where('usage_limitation', '>', '0')
                    ->orWhereNull('usage_limitation');
            })
            ->first();
    }

    public function searchCode($code = null)
    {
        if ($code)
            $this->query->where('code', $code);
        return $this;
    }

    public function searchPercent($percent = null)
    {
        if ($percent)
            $this->query->where('percent', $percent);
        return $this;
    }

    public function searchDescription($description = null)
    {
        if ($description)
            $this->query->where('description', 'like', '%'. $description .'%');
        return $this;
    }
}
