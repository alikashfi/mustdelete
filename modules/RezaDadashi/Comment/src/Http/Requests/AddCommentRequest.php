<?php

namespace RezaDadashi\Comment\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class AddCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => ['required', 'max:500'],
            'request_id' => ['required', 'numeric'],
            'media_id' => ['nullable', 'mimes:doc,docx'],
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'body' => 'متن پیام',
            'media_id' => 'فایل',
        ] : [];
    }
}
