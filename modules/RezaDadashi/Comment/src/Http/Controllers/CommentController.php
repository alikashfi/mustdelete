<?php

namespace RezaDadashi\Comment\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use RezaDadashi\Comment\Http\Requests\AddCommentRequest;
use RezaDadashi\Comment\Models\Comment;
use RezaDadashi\Comment\Notifications\commentCreatedNotification;
use RezaDadashi\Comment\Repositories\CommentRepository;
use RezaDadashi\Media\Models\Media;
use RezaDadashi\Media\Services\MediaFileService;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Repositories\RequestRepository;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Repositories\UserRepository;

class CommentController extends Controller
{
    private $requestRepository;
    private $commentRepository;
    private $userRepository;

    public function __construct(RequestRepository $requestRepository, CommentRepository $commentRepository, UserRepository $userRepository)
    {
        $this->requestRepository = $requestRepository;
        $this->commentRepository = $commentRepository;
        $this->userRepository = $userRepository;
    }

    private function authUserCanComment(Request $request, $auth)
    {
        $acceptAuth = Comment::$commentsStep;

        if (auth()->user()->can(Permission::PERMISSION_MANAGE_REQUESTS)) {
            return true;
        }

        if (! in_array($auth, $acceptAuth)) {
            newFeedback('notification_not_detect', 'error');
            return false;
        }

        if ($auth == Comment::COMMENT_SUPERVISOR_CUSTOMER) { // ناظر - مشتری
            if (!$request->user_id) {
                newFeedback('notification_customer_this_request_has_been_deleted', 'error');
                return false;
            }
            if (!$request->supervisor_id) {
                newFeedback('notification_no_supervisor_has_been_appointed_for_this_request', 'error');
                return false;
            }
            if (auth()->id() != $request->user_id && auth()->id() != $request->supervisor_id) {
                newFeedback('notification_you_are_not_the_moderator_or_client_of_this_request', 'error');
                return false;
            }
            if ($request->supervisor_id == $request->user_id) {
                newFeedback('notification_you_are_the_supervisor_and_customer', 'error');
                return false;
            }
        }

        if ($auth == Comment::COMMENT_SUPERVISOR_EXPERT) { // ناظر - کارشناس
            if (!$request->owner_id) {
                newFeedback('notification_no_expert_has_been_appointed_for_this_request', 'error');
                return false;
            }
            if (auth()->id() != $request->supervisor_id && auth()->id() != $request->owner_id) {
                newFeedback('notification_you_are_not_an_supervisor_or_expert_on_this_request', 'error');
                return false;
            }
            if ($request->supervisor_id == $request->owner_id) {
                newFeedback('notification_you_are_the_expert_and_supervisor', 'error');
                return false;
            }
        }

        return true;
    }

    public function showCommentForm($requestId, $step)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        $unreadComments = $this->commentRepository->getUnreadCommentByReceiverIdAndRequestId(auth()->id(), $requestId);
        if ($unreadComments) {
            $this->commentRepository->updateToReadCommentByReceiverIdAndRequestId(auth()->id(), $requestId);
        }

        if (! $this->authUserCanComment($request, $step)) {
            return back();
        }

        $messages = $this->commentRepository->getCommentByRequestIdAndStep($requestId, $step);

        return view('Comment::comment', compact('request', 'messages'));
    }

    private function upload($fieldName)
    {
        $mediaId = 0;
        if (\request()->hasFile($fieldName)) {  //check the file present or not
            $image = \request()->file($fieldName); //get the file
            $name = md5(time() . time()).'.'.$image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/uploads/word_file'); //public path folder dir
            $image->move($destinationPath, $name);  //mve to destination you mentioned

            $media = Media::create([
                'user_id' => auth()->id(),
                'files' => ['doc' => $name],
                'type' => 'doc',
                'filename' => $image->getClientOriginalName(),
                'is_private' => 0,
            ]);
            $mediaId = $media->id;
        }
        return $mediaId;
    }

    public function comment($requestId, $step, AddCommentRequest $request)
    {
        $findRequest = $this->requestRepository->findOrFail($requestId);

        $message = $request->body;

        $rejectedItems = [
            'com', 'COM',
            'ir', 'IR',
            'org', 'ORG',
            'info', 'INFO',
            '@gmail', '@GMAIL',
            '@yahoo', '@YAHOO'
        ];
        $message = str_replace($rejectedItems, '*', $message);


        //filter invalid patterns...
        $message = preg_replace('/[0-9]+/', '*', $message); //numbers
        $message = preg_replace('!\s+!', ' ', $message); //spaces

        if (! $this->authUserCanComment($findRequest, $step)) {
            return back();
        }




        $mediaId = null;
        if ($request->hasFile('media_id')) {
            $mediaId = $this->upload('media_id');
        }

        $receiverId = 0;
        if ($step == Comment::COMMENT_SUPERVISOR_CUSTOMER) {
            if (auth()->id() == $findRequest->user_id) { // this user has customer
                $receiverId = $findRequest->supervisor_id;
            } else { // this user has supervisor
                $receiverId = $findRequest->user_id;
            }
        } else if ($step == Comment::COMMENT_SUPERVISOR_EXPERT) {
            if (auth()->id() == $findRequest->supervisor_id) { // this user has supervisor
                $receiverId = $findRequest->owner_id;
            } else { // this user has expert or owner
                $receiverId = $findRequest->supervisor_id;
            }
        }


        if ($receiverId) {
            /** Send Notification To Customer */
            $notifiableUser = $this->userRepository->findOrFail($receiverId);
            Notification::send($notifiableUser, (new commentCreatedNotification($findRequest)));

            $this->commentRepository->create([
                'user_id' => auth()->id(),
                'receiver_id' => $receiverId,
                'request_id' => $requestId,
                'media_id' => $mediaId,
                'step' => $step,
                'body' => $message,
            ]);
        }

        return back();
    }
}
