<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('receiver_id');
            $table->foreignId('request_id');
            $table->foreignId('media_id')->nullable();
            $table->enum('step', \RezaDadashi\Comment\Models\Comment::$commentsStep);
            $table->text('body');
            $table->timestamp('read_at')->nullable();
            $table->timestamps();

            // اگر کاربر از جدول کاربران حذف شد، این کامنت هم حذف می شود.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            // اگر کاربر(دریافت کننده) از جدول کاربران حذف شد، این کامنت هم حذف می شود.
            $table->foreign('receiver_id')->references('id')->on('users')->onDelete('cascade');

            // اگر درخواست از جدول درخواست ها حذف شد، این کامنت هم حذف می شود.
            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade');

            // اگر فایل به هردلیلی حذف شد، ستون media_id در این جدول null می شود.
            $table->foreign('media_id')->references('id')->on('media')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
