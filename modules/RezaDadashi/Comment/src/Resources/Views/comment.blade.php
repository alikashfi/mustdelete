@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('requests.show', $request->id) }}">درخواست شماره {{ $request->id }}</a></li>
    <li class="breadcrumb-item"><a href="#">گفتگوها</a></li>
@endsection

@section('js')
    <script>
        $( document ).ready(function() {
            $('body').addClass('comment-chat-body');

            var commentAreat = document.getElementById("commentArea");
            commentAreat.scrollTop = commentAreat.scrollHeight;
        });
    </script>
@endsection

@section('content')
    <div class="row d-flex flex-column justify-content-between rounded-3 comment-chat-area">
        <div id="commentArea" class="messages p-2">
            @foreach($messages as $message)
                <div class="d-flex {{ $message->user->id != auth()->id() ? 'flex-row-reverse' : null }}  justify-content-start align-items-center my-1">
                    <img width="50" height="50" class="rounded-circle border border-2 border-white" src="{{ $message->user->thumb }}" alt="">
                    <div class="position-relative">
                        <div class="rounded-pill {{ $message->user->id == auth()->id() ? 'bg-success' : 'bg-info' }}  px-3 py-1 mx-2 my-2 mb-2">
                            <div>{!! nl2br($message->body) !!}</div>
                            <div>
                                @if($message->file)
                                    <a href="{{ $message->downloadLink() }}">
                                        {{ (new \RezaDadashi\Media\Repositories\MediaRepository())->find($message->media_id)->filename }}
                                        <i class="bi bi-paperclip"></i>
                                    </a>
                                @endif
                            </div>
                            <span class="fs-12">{{ $message->user->name }} / </span>
                            <span class="text-black fs-12 fw-200">
                                @if($message->created_at->diffInMinutes(now()) <= 10)
                                    {{ $message->created_at->diffForHumans() }}
                                @else
                                    {{ getJalali($message->created_at) }}
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

{{--        <div class="bg-danger text-white p-2">--}}
{{--            <i class="bi bi-megaphone px-2"></i>@lang('conversation_rules')--}}
{{--        </div>--}}

        <div class="_p-3 actions-area p-0">
            <form class="w-100" action="{{ route('comments.comment', [request()->route('request'), request()->route('auth')]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="request_id" value="{{ request()->route('request') }}">

                <div class="input-group">
                    <button class="btn" type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-send-fill" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M15.964.686a.5.5 0 0 0-.65-.65l-.095.038L.767 5.854l-.001.001-.452.18a.5.5 0 0 0-.082.887l.41.26.001.002 4.563 2.903.432.275.275.432 2.903 4.563.002.002.26.41a.5.5 0 0 0 .886-.083l.181-.453L15.926.78l.038-.095Zm-1.833 1.89.471-1.178-1.178.471L5.93 9.363l.338.215a.5.5 0 0 1 .154.154l.215.338 7.494-7.494Z"></path>
                        </svg>
                    </button>
                    <input name="body" placeholder="Type your message" class="form-control" required autofocus>
                    <label class="input-group-text" for="media-id">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-paperclip" viewBox="0 0 16 16">
                            <path d="M4.5 3a2.5 2.5 0 0 1 5 0v9a1.5 1.5 0 0 1-3 0V5a.5.5 0 0 1 1 0v7a.5.5 0 0 0 1 0V3a1.5 1.5 0 1 0-3 0v9a2.5 2.5 0 0 0 5 0V5a.5.5 0 0 1 1 0v7a3.5 3.5 0 1 1-7 0V3z"/>
                        </svg>

                        <input type="file" id="media-id" name="media_id" class="d-none" accept=".doc,.docx">
                    </label>
                </div>
            </form>
        </div>
    </div>
@endsection
