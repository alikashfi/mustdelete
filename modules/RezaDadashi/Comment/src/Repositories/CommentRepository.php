<?php

namespace RezaDadashi\Comment\Repositories;

use Illuminate\Support\Facades\DB;
use RezaDadashi\Comment\Models\Comment;
use RezaDadashi\Core\Repositories\Repository;

class CommentRepository extends Repository
{
    public function model()
    {
        return Comment::class;
    }

    public function getCommentByRequestIdAndStep($requestId, $step)
    {
        return $this->query
            ->where('request_id', $requestId)
            ->where('step', $step)
            ->get();
    }

    public function getUnreadCommentByReceiverIdAndRequestId($userId, $requestId = null)
    {
        $query = Comment::query()
            ->where('receiver_id', $userId)
            ->whereNull('read_at');

        if ($requestId)
            $query->where('request_id', $requestId);

        return $query
            ->orderBy('request_id')
            ->get();
    }

    public function updateToReadCommentByReceiverIdAndRequestId($userId, $requestId = null)
    {
        $query = Comment::query()
            ->where('receiver_id', $userId)
            ->whereNull('read_at');

        if ($requestId)
            $query->where('request_id', $requestId);

        $query->update([
            'read_at' => now()
        ]);
    }
}
