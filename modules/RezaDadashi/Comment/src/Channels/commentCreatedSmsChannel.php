<?php

namespace RezaDadashi\Comment\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Request\Models\Request;

class commentCreatedSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $requestDetail = $notification->toSms($notifiable);
        $requestId = $requestDetail['request_id'];

        $showCommentLink = createRequestDirectLink($requestId, $notifiable->token, Request::DIRECT_LINK_ACTION_USER_LOGIN);

        $textSend = "***************************************************** \n
        پیام جدیدی در گفتگوی با شناسه $requestId ثبت شد \n
        برای مشاهده وارد لینک زیر شوید. \n
        $showCommentLink
        \n *****************************************************
        ";
        Log::info($textSend);
    }
}
