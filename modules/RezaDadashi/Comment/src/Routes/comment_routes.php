<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth', 'verified'],
    'prefix' => 'comments'
], function ($router) {

    Route::get('/{request}/{auth}', [\RezaDadashi\Comment\Http\Controllers\CommentController::class, 'showCommentForm'])
        ->name('comments.link');

    Route::post('/{request}/{auth}', [\RezaDadashi\Comment\Http\Controllers\CommentController::class, 'comment'])
        ->name('comments.comment');

});
