<?php

namespace RezaDadashi\Comment\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use RezaDadashi\Comment\Models\Comment;
use RezaDadashi\Comment\Policies\CommentPolicy;

class CommentServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadRoutesFrom(__DIR__ . '/../Routes/comment_routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Comment');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');

        Gate::policy(Comment::class, CommentPolicy::class);
    }

    public function boot()
    {

    }
}
