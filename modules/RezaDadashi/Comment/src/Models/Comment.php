<?php

namespace RezaDadashi\Comment\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use RezaDadashi\Media\Models\Media;
use RezaDadashi\User\Models\User;

class Comment extends Model
{
    protected $guarded = [];

    protected $casts = [
        'read_at' => 'datetime',
    ];

    const COMMENT_SUPERVISOR_CUSTOMER = 'supervisor-customer';
    const COMMENT_SUPERVISOR_EXPERT = 'supervisor-expert';

    public static $commentsStep = [
        self::COMMENT_SUPERVISOR_CUSTOMER,
        self::COMMENT_SUPERVISOR_EXPERT,
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function file()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }

    public function downloadLink()
    {
        if ($this->media_id)
            return '/uploads/word_file/'. $this->file->files['doc'];
    }

    public function getThumbAttribute()
    {
        //
    }
}
