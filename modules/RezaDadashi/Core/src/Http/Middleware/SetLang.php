<?php

namespace RezaDadashi\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SetLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = 'fa';

        // Authentic Languages
        $languages = config()->get('translationLang.languages');

        // if session have lang, set lang to it.
        if ($session = Session::get('lang')) $lang = $session;

        // if url have ? lang, set lang to it.
        if ($query = request()->query('lang')) $lang = $query;

        // if session or query does not have valid key lang, set default lang to fa
        if (!array_key_exists($lang, $languages)) $lang = 'fa';

        // session put
        Session::put('lang', $lang);

        app()->setLocale($lang);

        return $next($request);
    }
}
