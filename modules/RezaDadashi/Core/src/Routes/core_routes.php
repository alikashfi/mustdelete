<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'web'
], function ($router) {

    Route::redirect('/', '/register');

    Route::get('/lang', function () {

        $defaultLang = 'fa';
        $requestLang = request();
        $lang = $requestLang->lang ? $requestLang->lang : $defaultLang;

        // Authentic Languages
        $languages = config()->get('translationLang.languages');

        if (!array_key_exists($lang, $languages)) $lang = $defaultLang;

        app()->setLocale($lang);

        return back();
    })->name('changeLang');
});
