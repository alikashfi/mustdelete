<?php

namespace RezaDadashi\Core\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use RezaDadashi\Core\Http\Middleware\SetLang;

class CoreServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/core_routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Core');
        $this->mergeConfigFrom(__DIR__ . '/../Config/translation.php', 'translationLang');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');
    }

    public function boot()
    {
        /** Push Middleware */
        $this->app['router']->pushMiddlewareToGroup('web', SetLang::class); //Set Language

        Schema::defaultStringLength(191); // Default String Length Database

    }
}
