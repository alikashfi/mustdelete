<?php

namespace RezaDadashi\Core\Responses;

use Illuminate\Http\Response;

class AjaxResponses
{
    public static function successResponse()
    {
        return response()->json([
            'message' => 'عملیات با موفقیت انجام شد.',
        ], Response::HTTP_OK);
    }

    public static function failedResponse()
    {
        return response()->json([
            'message' => 'عملیات با شکست مواجه شد!',
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
