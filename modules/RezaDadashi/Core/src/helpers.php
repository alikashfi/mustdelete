<?php

if (! function_exists('newFeedback')) {
    function newFeedback($message = 'success_message', $type = 'success')
    {
        $session = session()->has('feedbacks') ? session()->get('feedbacks') : [];

        $session[] = [
            'message' => trans($message),
            'type' => $type
        ];

        session()->flash('feedbacks', $session);
    }
}

if (! function_exists('wordCount')) {
    function wordCount($file) {
        $file = explode(" ", $file);
        $filtered_array = array_filter($file);
        return count($filtered_array);
    }
}

if (! function_exists('array_in_string')) {
    function array_in_string($str, array $arr) {
        $count = 0;
        foreach($arr as $arr_value) {
            if (stripos($str, $arr_value) !== false) {
                $count++;
            }
        }
        return $count;
    }
}

if (! function_exists('getDiff')) {
    function getDiff($oldObject, $newObject, array $rejectKeys = []) {
        $defaultRejectKey = ['created_at', 'updated_at'];
        $shouldRemovedKey = array_merge($defaultRejectKey, $rejectKeys);

        $new = collect($newObject->toArray())->forget($shouldRemovedKey);
        $old = collect($oldObject->toArray())->forget($shouldRemovedKey);

        $diff = $new->diff($old);

        return $diff->all();

    }
}


if (! function_exists('dateFromJalali')) {
    function dateFromJalali($date, $format = 'Y/m/d')
    {
        // 1400/07/27 : output = 2021-10-19 00:00:00
        return $date ? \Morilog\Jalali\Jalalian::fromFormat($format, $date)->toCarbon() : null;
    }
}

if (! function_exists('getJalaliFromFormat')) {
    function getJalaliFromFormat($date, $format = 'Y-m-d')
    {
        // 2021-10-21
        return \Morilog\Jalali\Jalalian::fromCarbon(\Carbon\Carbon::createFromFormat($format, $date))->format($format);
    }
}

if (! function_exists('getJalali')) {
    function getJalali(\Carbon\Carbon $carbon)
    {
        return \Morilog\Jalali\Jalalian::fromCarbon($carbon);
    }
}

if (! function_exists('passwordGenerator')) {
    function passwordGenerator(){
        $numbers = '1234567890';
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        $special = '!@#$%&*';
        return substr(str_shuffle($numbers), 0, 3).substr(str_shuffle($letters), 0, 3).substr(str_shuffle($special), 0, 2);
    }
}

if (! function_exists('createRequestDirectLink')) {
    function createRequestDirectLink($requestId, $userToken, $type) {
        $type = str_replace(['/', '/\/', '\\'], '', md5(md5(md5($type))));
        $key = str_replace(['/', '/\/', '\\'], '', md5($requestId));
        return url('/') . "/request/direct/$requestId/$userToken/$type?key=$key";
    }
}

if (! function_exists('checkRequestDirectLink')) {
    function checkRequestDirectLink($requestId, $userToken, $type, $key) {
        $roleUser = null;
        $acceptedType = [
            '06ed5f15a83bef3d13ce774f5fbc4b7e' => \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_SUPERVISOR,
            'cf7127d565d63d2bdef2966ac0f3f636' => \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_OWNER,
            '569282bba19675d15cc12e12d62b04af' => \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_USER_LOGIN,
            'e0640b088a8b517d92276feb56815a50' => \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_DOWNLOAD_CUSTOMER_FILE,
            '1bb161d20b6d1b58918a1ec914900b85' => \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_DOWNLOAD_FINAL_FILE,
        ];
        if (! is_numeric($requestId)) return false;
        if (is_null($userToken)) return false;
        if (is_null($type)) return false;
        if (! array_key_exists($type, $acceptedType)) return false;

        foreach ($acceptedType as $typeItemKey => $typeItemValue) {
           if ($type == $typeItemKey) $roleUser = $typeItemValue;
        }

        if (is_null($key)) return false;
        if (str_replace(['/', '/\/', '\\'], '', md5($requestId)) != $key) return false;

        return $roleUser;
    }
}

if (! function_exists('generateToken')) {
    function generateToken() {
        $token = crypt(md5(md5(time() . uniqid())) . md5(sha1(md5(time()))), PASSWORD_DEFAULT);
        return str_replace('/', '', $token);
    }
}

if (! function_exists('iranianMobile')) {
    function iranianMobile($mobile) {
        if (str_starts_with($mobile, '98'))
            return true;
        return false;
    }
}

