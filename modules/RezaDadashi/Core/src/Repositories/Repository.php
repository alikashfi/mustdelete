<?php

namespace RezaDadashi\Core\Repositories;

abstract class Repository
{
    protected $model;
    protected $query;
    abstract public function model();

    public function __construct()
    {
        $this->model = app($this->model());
        $this->query = ($this->model)::query();
    }

    public function all()
    {
        return $this->model->all();
    }

    public function latest()
    {
        return $this->query->latest()->get();
    }

    public function paginate($limit = 15)
    {
        return $this->query->paginate($limit);
    }

    public function paginateLatest($limit = 15)
    {
        return $this->query->orderBy('id', 'desc')->paginate($limit);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function update(array $where, array $data)
    {
        return $this->model->where($where)->update($data);
    }

    public function delete($model)
    {
        return $model->delete();
    }

    public function exists($id)
    {
        return $this->model->where('id', $id)->exists();
    }
}
