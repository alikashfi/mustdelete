<?php

namespace RezaDadashi\Core\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidString implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('~^[a-z-A-Z_][a-z-A-Z_0-9\-_]{3,254}$~', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //return 'This Field Must Contain Only Chars [a-z, A-Z, 0-9, _ or -] with min length of 4';
        return 'این فیلد فقط می تواند شامل کاراکتر های (a-Z) (0-9) (_ -) باشد.';
    }
}
