@if(session()->has('feedbacks'))
    <script>
        @foreach(session()->get('feedbacks') as $message)
        notification("{{ $message['message'] }}", "{{ $message['type'] }}");
        @endforeach
    </script>
@endif
