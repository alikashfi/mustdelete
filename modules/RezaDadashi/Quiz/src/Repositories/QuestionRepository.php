<?php

namespace RezaDadashi\Quiz\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Quiz\Models\Question;

class QuestionRepository extends Repository
{
    public function model()
    {
        return Question::class;
    }

    public function getOneRandomQuestion($type)
    {
        return Question::query()
            ->where('type', $type)
            ->inRandomOrder()
            ->first();
    }
}
