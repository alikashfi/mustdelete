<?php

namespace RezaDadashi\Quiz\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Quiz\Models\Quiz;

class QuizRepository extends Repository
{
    public function model()
    {
        return Quiz::class;
    }

    public function searchUser($name = null)
    {
        if ($name)
            $this->query->whereHas('user', function ($query) use($name) {
                return $query->where('name', 'like', '%'. $name .'%');
            });
        return $this;
    }

    public function searchQuestionType($type = null)
    {
        if ($type)
            $this->query->whereHas('question', function ($query) use($type) {
                return $query->where('type', $type);
            });
        return $this;
    }

    public function searchStatus($status = null)
    {
        if ($status)
            $this->query->where('status', $status);
        return $this;
    }
}
