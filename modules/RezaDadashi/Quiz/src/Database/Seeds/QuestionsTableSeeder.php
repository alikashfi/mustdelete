<?php

namespace RezaDadashi\Quiz\Database\Seeds;

use Illuminate\Database\Seeder;
use RezaDadashi\Quiz\Models\Question;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Question::$defaultQuestions as $question) {
            Question::query()->firstOrCreate(
                ['question' => $question['question']],
                [
                    'user_id' => $question['user_id'],
                    'type' => $question['type'],
                    'question' => $question['question'],
                    'answer' => $question['answer'],
                    'rules' => $question['rules']
                ]
            );
        }
    }
}
