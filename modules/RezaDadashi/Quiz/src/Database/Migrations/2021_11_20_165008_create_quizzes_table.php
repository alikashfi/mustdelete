<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('supervisor_id')->nullable();
            $table->foreignId('question_id')->nullable();
            $table->text('answer');
            $table->enum('status', \RezaDadashi\Quiz\Models\Quiz::$statuses)
                ->default(\RezaDadashi\Quiz\Models\Quiz::STATUS_PENDING);
            $table->timestamps();

            // اگر کاربر به هردلیلی حذف شد، آزمون های کاربر هم حذف شود.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');

            // اگر ناظر به هردلیلی حذف شد، ستون supervisor_id نال شود.
            $table->foreign('supervisor_id')->references('id')->on('users')->onDelete('SET NULL');

            // اگر سوال به هردلیلی حذف شد، ستون question_id نال شود.
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
