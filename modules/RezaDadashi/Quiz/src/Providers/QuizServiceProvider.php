<?php

namespace RezaDadashi\Quiz\Providers;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use RezaDadashi\Quiz\Database\Seeds\QuestionsTableSeeder;
use RezaDadashi\Quiz\Models\Question;
use RezaDadashi\Quiz\Models\Quiz;
use RezaDadashi\Quiz\Policies\QuestionPolicy;
use RezaDadashi\Quiz\Policies\QuizPolicy;
use RezaDadashi\RolePermissions\Models\Permission;

class QuizServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadRoutesFrom(__DIR__ . '/../Routes/quiz_routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Quiz');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');

        Gate::policy(Quiz::class, QuizPolicy::class);
        Gate::policy(Question::class, QuestionPolicy::class);

        DatabaseSeeder::$seeders[] = QuestionsTableSeeder::class;
    }

    public function boot()
    {
        config()->set('sidebar.items.quizzes', [
            'icon' => 'bi bi-patch-question-fill',
            'title' => ['fa' => 'آزمون ها', 'en' => 'Quizzes'],
            'url' => route('quizzes.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_QUIZZES
            ]
        ]);

        config()->set('sidebar.items.cooperation', [
            'icon' => 'bi bi-patch-question-fill',
            'title' => ['fa' => 'درخواست همکاری', 'en' => 'Cooperation Request'],
            'url' => route('quizzes.showQuizForm'),
        ]);
    }
}
