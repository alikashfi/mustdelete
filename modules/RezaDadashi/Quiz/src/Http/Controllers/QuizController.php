<?php

namespace RezaDadashi\Quiz\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use RezaDadashi\Core\Responses\AjaxResponses;
use RezaDadashi\Quiz\Http\Requests\CheckQuizRequest;
use RezaDadashi\Quiz\Http\Requests\DecisionQuizRequest;
use RezaDadashi\Quiz\Http\Requests\GetQuizRequest;
use RezaDadashi\Quiz\Http\Requests\QuestionStoreRequest;
use RezaDadashi\Quiz\Http\Requests\QuizIntroductionRequest;
use RezaDadashi\Quiz\Models\Question;
use RezaDadashi\Quiz\Models\Quiz;
use RezaDadashi\Quiz\Repositories\QuestionRepository;
use RezaDadashi\Quiz\Repositories\QuizRepository;
use RezaDadashi\Quiz\Services\QuizService;
use RezaDadashi\RolePermissions\Models\Role;
use RezaDadashi\User\Repositories\UserRepository;


class QuizController extends Controller
{
    private $quizRepository;
    private $questionRepository;
    private $userRepository;

    public function __construct(QuizRepository $quizRepository, QuestionRepository $questionRepository, UserRepository $userRepository)
    {
        $this->quizRepository = $quizRepository;
        $this->questionRepository = $questionRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        /** Check Permission */
        $this->authorize('manage', Quiz::class);

        $quizzes = $this->quizRepository
            ->searchUser(request('user'))
            ->searchQuestionType(request('question'))
            ->searchStatus(request('status'))
            ->paginateLatest();

        return view('Quiz::Admin.index', compact('quizzes'));
    }

    public function showQuizForm()
    {
        return view('Quiz::Front.quizType');
    }

    public function getQuiz(GetQuizRequest $request)
    {
        $quizType = $request->type;
        $question = $this->questionRepository->getOneRandomQuestion($quizType);
        if (!$question) {
            newFeedback('notification_no_quiz_has_been_registered_for_your_request', 'error');
            return back();
        }

        QuizService::storeQuestion($question->getAttributes());

        return redirect()->route('quizzes.quiz');
    }

    public function quiz()
    {
        $questionDetails = QuizService::getQuestion();

        if (!$questionDetails) return redirect()->route('quizzes.showQuizForm');

        $question = $this->questionRepository->findOrFail($questionDetails['id']);

        return view('Quiz::Front.quiz', compact('question'));
    }

    public function check(CheckQuizRequest $request)
    {
        QuizService::storeQuiz($request->all());

        $output = preg_replace('!\s+!', ' ', $request->answer);
        $answer = strtolower($output);

        $question = $this->questionRepository->findOrFail($request->question_id);
        $results = [];


        //check the all quiz
        $rules = $question->rules;

        $min2NotExistReject = $rules['min_2_not_exists_reject'];
        $min2ExistReject = $rules['min_2_exists_reject'];

        $min1NotExistReject = $rules['min_1_not_exists_reject'];
        $min1ExistReject = $rules['min_1_exists_reject'];

        $results['min_2_not_exists_reject'] = array_in_string($answer, $min2NotExistReject);
        $results['min_2_exists_reject'] = array_in_string($answer, $min2ExistReject);
        $results['min_1_not_exists_reject'] = array_in_string($answer, $min1NotExistReject);
        $results['min_1_exists_reject'] = array_in_string($answer, $min1ExistReject);

        if ($results['min_2_not_exists_reject'] < 2) {

            if (auth()->check()) {
                $this->createQuiz(auth()->id(), $request->question_id, $answer, Quiz::STATUS_FAILED);
            }

            QuizService::delete();
            newFeedback('notification_failed_this_quiz', 'error');
            return redirect()->route('quizzes.showQuizForm');
        }

        if ($results['min_2_exists_reject'] > 2) {

            if (auth()->check()) {
                $this->createQuiz(auth()->id(), $request->question_id, $answer, Quiz::STATUS_FAILED);
            }

            QuizService::delete();
            newFeedback('notification_failed_this_quiz', 'error');
            return redirect()->route('quizzes.showQuizForm');
        }

        if ($results['min_1_not_exists_reject'] < 1) {

            if (auth()->check()) {
                $this->createQuiz(auth()->id(), $request->question_id, $answer, Quiz::STATUS_FAILED);
            }

            QuizService::delete();
            newFeedback('notification_failed_this_quiz', 'error');
            return redirect()->route('quizzes.showQuizForm');
        }

        if ($results['min_1_exists_reject'] > 1) {

            if (auth()->check()) {
                $this->createQuiz(auth()->id(), $request->question_id, $answer, Quiz::STATUS_FAILED);
            }

            QuizService::delete();
            newFeedback('notification_failed_this_quiz', 'error');
            return redirect()->route('quizzes.showQuizForm');
        }


        if (auth()->check()) {
            $this->createQuiz(auth()->id(), $request->question_id, $answer);
            QuizService::delete();
            newFeedback();
            return redirect()->route('home');
        }

        return redirect(route('quizzes.introduction'));
    }

    public function introduction(Request $request)
    {
        return view('Quiz::Front.introduction');
    }

    public function storeIntroduction(QuizIntroductionRequest $request)
    {
        $user = $this->userRepository->create([
            'name' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
        ]);

        $quizData = QuizService::getQuiz();
        $this->createQuiz($user->id, $quizData['question_id'], $quizData['answer']);
        QuizService::delete();

        event(new Registered($user));

        auth()->loginUsingId($user->id);

        newFeedback();
        return redirect()->route('home');
    }

    public function destroy($quizId)
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $quiz = $this->quizRepository->findOrFail($quizId);
        $this->quizRepository->delete($quiz);

        return AjaxResponses::successResponse();
    }

    public function show($quizId)
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $quiz = $this->quizRepository->findOrFail($quizId);

        return view('Quiz::Admin.show', compact('quiz'));
    }

    public function decision($quizId, DecisionQuizRequest $request)
    {
        $quiz = $this->quizRepository->findOrFail($quizId);

        /** Check Permission */
        $this->authorize('decision', $quiz);

        if ($request->decision) { //Accept

            $this->quizRepository->update(
                ['id' => $quizId],
                ['status' => Quiz::STATUS_END]
            );

            if ($quiz->question->type == Question::QUESTION_TRANSLATE) {
                $quiz->user->assignRole([Role::ROLE_TRANSLATOR_ENGLISH_TO_PERSIAN, Role::ROLE_TRANSLATOR_PERSIAN_TO_ENGLISH]);
                newFeedback('notification_translation_permission_successfully_completed', 'success');
                return back();
            } else if($quiz->question->type == Question::QUESTION_EDITING) {
                $quiz->user->assignRole([Role::ROLE_TRANSLATOR_ENGLISH_EDITOR]);
                newFeedback('notification_editor_permission_successfully_completed', 'success');
                return back();
            }
        }

        $this->quizRepository->update(
            ['id' => $quizId],
            ['status' => Quiz::STATUS_FAILED]
        );

        newFeedback('notification_quiz_was_failed', 'success');
        return back();
    }

    private function createQuiz($userId, $questionId, $answer, $status = Quiz::STATUS_PENDING)
    {
        return $this->quizRepository->create([
            'user_id' => $userId,
            'question_id' => $questionId,
            'answer' => $answer,
            'status' => $status
        ]);
    }

    public function showQuestions()
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $questions = $this->questionRepository->paginateLatest();

        return view('Quiz::Admin.questions.index', compact('questions'));
    }

    public function showQuestion($questionId)
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $question = $this->questionRepository->findOrFail($questionId);

        return view('Quiz::Admin.questions.show', compact('question'));
    }

    public function storeQuestion(QuestionStoreRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $this->questionRepository->create([
            'user_id' => auth()->id(),
            'type' => $request->type,
            'question' => $request->question,
            'answer' => $request->answer,
            'rules' => [
                'min_2_not_exists_reject' => explode("\r\n", $request->min_2_not_exists_reject),
                'min_2_exists_reject' => explode("\r\n", $request->min_2_exists_reject),
                'min_1_not_exists_reject' => explode("\r\n", $request->min_1_not_exists_reject),
                'min_1_exists_reject' => explode("\r\n", $request->min_1_exists_reject),
            ]
        ]);

        return redirect()->route('quizzes.showQuestions');
    }

    public function showEditQuestionForm($questionId)
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $question = $this->questionRepository->findOrFail($questionId);

        return view('Quiz::Admin.questions.edit', compact('question'));
    }

    public function updateQuestion($questionId, QuestionStoreRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $this->questionRepository->findOrFail($questionId);

        $this->questionRepository->update(
            ['id' => $questionId],
            [
                'type' => $request->type,
                'question' => $request->question,
                'answer' => $request->answer,
                'rules' => [
                    'min_2_not_exists_reject' => explode("\r\n", $request->min_2_not_exists_reject),
                    'min_2_exists_reject' => explode("\r\n", $request->min_2_exists_reject),
                    'min_1_not_exists_reject' => explode("\r\n", $request->min_1_not_exists_reject),
                    'min_1_exists_reject' => explode("\r\n", $request->min_1_exists_reject),
                ]
            ]
        );

        newFeedback();
        return redirect()->route('quizzes.showQuestions');
    }

    public function destroyQuestion($questionId)
    {
        /** Check Permission */
        $this->authorize('manage', Question::class);

        $question = $this->questionRepository->findOrFail($questionId);
        $this->questionRepository->delete($question);

        return AjaxResponses::successResponse();
    }
}
