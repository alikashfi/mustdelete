<?php

namespace RezaDadashi\Quiz\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use RezaDadashi\Quiz\Models\Question;

class CheckQuizRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_id' => ['required', 'numeric', 'exists:questions,id'],
            'answer' => ['required'],
        ];
    }
}
