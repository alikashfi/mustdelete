<?php

namespace RezaDadashi\Quiz\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use RezaDadashi\Quiz\Models\Question;

class QuestionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', Rule::in(Question::$questions)],
            'question' => ['required', 'min:5'],
            'answer' => ['required', 'min:5'],
            'min_2_not_exists_reject' => ['required', 'min:1'],
            'min_2_exists_reject' => ['required', 'min:1'],
            'min_1_not_exists_reject' => ['required', 'min:1'],
            'min_1_exists_reject' => ['required', 'min:1'],
        ];
    }
}
