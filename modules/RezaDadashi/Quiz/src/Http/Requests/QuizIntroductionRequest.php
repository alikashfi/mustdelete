<?php

namespace RezaDadashi\Quiz\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use RezaDadashi\User\Rules\ValidMobile;

class QuizIntroductionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guest() == true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'string', 'max:14', 'unique:users', new ValidMobile()],
        ];
    }
}
