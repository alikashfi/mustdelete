<?php

namespace RezaDadashi\Quiz\Services;

use Illuminate\Support\Facades\Session;

class QuizService
{
    private static $quiz = 'quiz';
    private static $question = 'question';

    public static function storeQuestion(array $question)
    {
        Session::put(self::$question, $question);
    }

    public static function storeQuiz(array $quiz)
    {
        Session::put(self::$quiz, $quiz);
    }

    public static function getQuiz()
    {
        return Session::get(self::$quiz);
    }

    public static function getQuestion()
    {
        return Session::get(self::$question);
    }

    public static function delete()
    {
        Session::forget([self::$quiz, self::$question]);
    }
}
