<?php

namespace RezaDadashi\Quiz\Models;

use Illuminate\Database\Eloquent\Model;
use RezaDadashi\User\Models\User;

class Quiz extends Model
{
    protected $guarded = [];

    const STATUS_PENDING = 'pending';
    const STATUS_FAILED = 'failed';
    const STATUS_END = 'end';

    public static $statuses = [
        self::STATUS_PENDING,
        self::STATUS_FAILED,
        self::STATUS_END,
    ];

    public function getCssClass()
    {
        switch ($this->status) {
            case self::STATUS_PENDING:
                return "badge bg-warning";
                break;

            case self::STATUS_FAILED:
                return "badge bg-danger";
                break;

            case self::STATUS_END:
                return "badge bg-success";
                break;

            default:
                return null;
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function supervisor()
    {
        return $this->belongsTo(User::class, 'supervisor_id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
