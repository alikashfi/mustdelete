<?php

namespace RezaDadashi\Quiz\Models;

use Illuminate\Database\Eloquent\Model;
use RezaDadashi\User\Models\User;

class Question extends Model
{
    protected $guarded = [];

    protected $casts = [
        'rules' => 'array'
    ];

    const QUESTION_TRANSLATE = 'translation';
    const QUESTION_EDITING = 'editing';

    public static $questions = [
        self::QUESTION_TRANSLATE,
        self::QUESTION_EDITING
    ];

    public static $defaultQuestions = [
        [
            'user_id' => 1,
            'type' => self::QUESTION_TRANSLATE,
            'question' => 'یکی از مسائل مهم در طول تاریخ فلسفه خود یا ماهیت شخص است. به طور کلی، برجسته‌ترین نظریه‌های خود عبارتست از: نظریه خود به مثابه جوهری غیرمادی، و نظریه بقچه‌ای خود. طبق نظریه نخست شخص عبارت است از جوهری غیرمادی که وجودش مستقل از بدن است.طرفداران این نظریه معتقدند خود امریست که ما وجودش را بی‌واسطه درک می‌کنیم، و از طرف دیگر وحدت آگاهی فقط در صورت وجود خود تبیین‌ پذیر است.اما طرفداران نظریه بقچه‌ای ادعای تجربه بی‌واسطه خود را رد می‌کنند و معتقدند اصلاً چنین جوهری وجود ندارد. از طرف دیگر، ایشان معتقدند برای تبیین وحدت آگاهی نیازی به مفروض‌گرفتن جوهری غیر مادی نیست چراکه می‌توان وحدت آگاهی را بر اساس خاطره و تداوم روان‌شناختی تبیین کرد.بر اساس این نظریه شخص عبارتست از مجموعه‌ای واحد از رخدادهای ذهنی که به طور علّی و معلولی با سیستم عصبی و به طور خاص با مغز مرتبط شده‌اند. در تاریخ فلسفه غرب برای اولین بار دیویدهیوم این دیدگاه را به صورت یک نظریه مطرح و از آن دفاع کرد.این نظریه شباهت‌ هایی نیز با نظریه نفی خود در آیین بودادارد که آن خود مستلزم نگارش یک مقاله مستقل است.در آیین بودا یکی از پیامدهای تمرین مستمر مراقبه، شهود این واقعیت است که خود وجود ندارد؛ و تحقق این بینش در باب خود زمینه را برای زیست اخلاقی فراهم می‌کند.انشاءالله عبدالکریمی، دانشیار گروه فلسفه اخلاق، پردیس شهید نوراللهی، دانشگاه الزهرا (س) (نویسنده مسئول)',
            'answer' => 'The self or human essence has been an important issue throughout the history of philosophy.Typically, the most outstanding theories of the self, include: the theory of self as an incorporeal essence, and the bundle theory.According to the first theory, the self is defined as an incorporeal essence existing independent of the body. The proponents of the theory argue that the existence of the self could be understood immediately.On the other hand, unity of consciousness could be elucidated only in the presence of the self.However, the proponents of the bundle theory reject the claim of immediate experiencing of the self and believe that such an essence does not exist at all.Furthermore, they believe that there is no need to assume an incorporeal essence to elucidate the unity of consciousness, as it could be elucidated on the basis of memory and psychological continuity.This theory defines the self as a united series of subjective events which are causally and effectually associated with nervous system, and the brain in particular.David Hume was the first who proposed and held this perspective as a theory in the history of western philosophy.This theory has similarities to Buddhist theory of self-denial, the explanation of which requires to be addressed in a separate paper.In Buddhism, continuous practice of meditation can lead to the intuition of the fact that the self does not exist.Realization of this intuition could pave the way for living the moral life.Inshallah Abdulkarimi, Associate professor, Department of moral philosophy, Shahid Noorollahi campus, Alzahra University, Tehran, Iran (Corresponding Author)',
            'rules' => [
                'min_2_not_exists_reject' => [
                    'history of philosophy',
                    'explained',
                    'buddhism',
                    'unity of consciousness',
                ],
                'min_2_exists_reject' => [
                    'bioethics',
                    'its existence',
                    'buddha',
                    'philosophy of',
                    'pardis',
                ],
                'min_1_not_exists_reject' => [
                    'history of philosophy',
                    'explained',
                    'buddhism',
                    'unity of consciousness',
                ],
                'min_1_exists_reject' => [
                    'bioethics',
                    'its existence',
                    'buddha',
                    'philosophy of',
                    'pardis',
                ],
            ]
        ],
        [
            'user_id' => 1,
            'type' => self::QUESTION_EDITING,
            'question' => 'The self or human essence has been an important issue throughout the history of philosophy.Typically, the most outstanding theories of the self, include: the theory of self as an incorporeal essence, and the bundle theory.According to the first theory, the self is defined as an incorporeal essence existing independent of the body. The proponents of the theory argue that the existence of the self could be understood immediately.On the other hand, unity of consciousness could be elucidated only in the presence of the self.However, the proponents of the bundle theory reject the claim of immediate experiencing of the self and believe that such an essence does not exist at all.Furthermore, they believe that there is no need to assume an incorporeal essence to elucidate the unity of consciousness, as it could be elucidated on the basis of memory and psychological continuity.This theory defines the self as a united series of subjective events which are causally and effectually associated with nervous system, and the brain in particular.David Hume was the first who proposed and held this perspective as a theory in the history of western philosophy.This theory has similarities to Buddhist theory of self-denial, the explanation of which requires to be addressed in a separate paper.In Buddhism, continuous practice of meditation can lead to the intuition of the fact that the self does not exist.Realization of this intuition could pave the way for living the moral life.Inshallah Abdulkarimi, Associate professor, Department of moral philosophy, Shahid Noorollahi campus, Alzahra University, Tehran, Iran (Corresponding Author)',
            'answer' => 'The self or human essence has been an important issue throughout the history of philosophy.Typically, the most outstanding theories of the self, include: the theory of self as an incorporeal essence, and the bundle theory.According to the first theory, the self is defined as an incorporeal essence existing independent of the body. The proponents of the theory argue that the existence of the self could be understood immediately.On the other hand, unity of consciousness could be elucidated only in the presence of the self.However, the proponents of the bundle theory reject the claim of immediate experiencing of the self and believe that such an essence does not exist at all.Furthermore, they believe that there is no need to assume an incorporeal essence to elucidate the unity of consciousness, as it could be elucidated on the basis of memory and psychological continuity.This theory defines the self as a united series of subjective events which are causally and effectually associated with nervous system, and the brain in particular.David Hume was the first who proposed and held this perspective as a theory in the history of western philosophy.This theory has similarities to Buddhist theory of self-denial, the explanation of which requires to be addressed in a separate paper.In Buddhism, continuous practice of meditation can lead to the intuition of the fact that the self does not exist.Realization of this intuition could pave the way for living the moral life.Inshallah Abdulkarimi, Associate professor, Department of moral philosophy, Shahid Noorollahi campus, Alzahra University, Tehran, Iran (Corresponding Author)',
            'rules' => [
                'min_2_not_exists_reject' => [
                    'history of philosophy',
                    'explained',
                    'buddhism',
                    'unity of consciousness',
                ],
                'min_2_exists_reject' => [
                    'bioethics',
                    'its existence',
                    'buddha',
                    'philosophy of',
                    'pardis',
                ],
                'min_1_not_exists_reject' => [
                    'history of philosophy',
                    'explained',
                    'buddhism',
                    'unity of consciousness',
                ],
                'min_1_exists_reject' => [
                    'bioethics',
                    'its existence',
                    'buddha',
                    'philosophy of',
                    'pardis',
                ],
            ]
        ]
    ];

    public function getCssClass()
    {
        switch ($this->type) {
            case self::QUESTION_TRANSLATE:
                return "badge bg-success";
                break;

            case self::QUESTION_EDITING:
                return "badge bg-warning";
                break;

            default:
                return null;
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function quizzes()
    {
        return $this->hasMany(Quiz::class, 'question_id');
    }
}
