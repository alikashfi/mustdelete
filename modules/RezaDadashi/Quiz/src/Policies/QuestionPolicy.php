<?php

namespace RezaDadashi\Quiz\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use RezaDadashi\RolePermissions\Models\Permission;

class QuestionPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function manage($user)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_QUIZZES)) return true;
    }
}
