<?php

namespace RezaDadashi\Quiz\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use RezaDadashi\Quiz\Models\Quiz;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\RolePermissions\Models\Permission;

class QuizPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function manage($user)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_QUIZZES)) return true;
    }

    public function decision($user, Quiz $quiz)
    {
        if (
            $quiz->status = Quiz::STATUS_PENDING &&
            $user->hasPermissionTo(Permission::PERMISSION_MANAGE_QUIZZES)
        ) return true;
    }
}
