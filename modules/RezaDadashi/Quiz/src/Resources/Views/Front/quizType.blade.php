@extends('Quiz::Front.master')

@section('content')
    <section class="min-vh-100 d-flex align-items-center justify-content-center">

        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="my-5 col-md-8 col-sm-12 bg-light py-5 px-4 d-flex justify-content-center align-items-center flex-column rounded shadow">

                    <div class="p-3 card w-100 mb-3">
                        {{-- Step 1 --}}
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">@lang('quiz_step_1_title')</strong>
                                <strong class="fs-6 fw-300">@lang('quiz_step_1_description')</strong>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                               data-bs-target="#step1" aria-expanded="false"></i>
                        </div>
                        <div class="collapse show border-top border-danger mt-3" id="step1">
                            <form action="{{ route('quizzes.getQuiz') }}" method="post">
                                @csrf
                                <div class="my-2">
                                    <label for="quiz-type" class="form-label">
                                        <i class="bi bi-patch-question px-1"></i>
                                        @lang('quiz_step_1_description')
                                        <span class="required">*</span>
                                    </label>
                                    <div class="input-group mb-3">
                                        <x-select name="type" id="quiz-type" class="form-select" required>
                                            <option value="">-- @lang('quiz_step_1_description') --</option>
                                            @foreach(\RezaDadashi\Quiz\Models\Question::$questions as $questionType)
                                                <option
                                                    @if(old('type') == $questionType) selected @endif
                                                value="{{ $questionType }}">
                                                    @lang($questionType)
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <button class="btn btn-success" type="submit">@lang('common_continue')</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                    <div class="p-3 card w-100 mb-3">
                        {{-- Step 2 --}}
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">@lang('quiz_step_2_title')</strong>
                                <strong class="fs-6 fw-300">@lang('quiz_step_2_description')</strong>
                            </div>
                            {{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
                            {{--                               data-bs-target="#step2" aria-expanded="false"></i>--}}
                        </div>
                    </div>

                    <div class="p-3 card w-100">
                        {{-- Step 3 --}}
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">@lang('quiz_step_3_title')</strong>
                                <strong class="fs-6 fw-300">@lang('quiz_step_3_description')</strong>
                            </div>
                            {{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
                            {{--                               data-bs-target="#step2" aria-expanded="false"></i>--}}
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </section>
@endsection
