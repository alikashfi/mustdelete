@extends('Quiz::Front.master')

@section('content')
    <section class="min-vh-100 d-flex align-items-center justify-content-center">

        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="my-5 col-md-8 col-sm-12 bg-light py-5 px-4 d-flex justify-content-center align-items-center flex-column rounded shadow">

                    <div class="p-3 card w-100 mb-3">
                        {{-- Step 1 --}}
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">@lang('quiz_step_1_title')</strong>
                                <strong class="fs-6 fw-300">@lang('quiz_step_1_description')</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step1" aria-expanded="false"></i>--}}
                        </div>
                    </div>

                    <div class="p-3 card w-100 mb-3">
                        {{-- Step 2 --}}
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">@lang('quiz_step_2_title')</strong>
                                <strong class="fs-6 fw-300">@lang('quiz_step_2_description')</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step2" aria-expanded="false"></i>--}}
                        </div>
                    </div>

                    <div class="p-3 card w-100">
                        {{-- Step 3 --}}
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">@lang('quiz_step_3_title')</strong>
                                <strong class="fs-6 fw-300">@lang('quiz_step_3_description')</strong>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                                data-bs-target="#step3" aria-expanded="false"></i>
                        </div>
                        @if(auth()->guest())
                            <div class="collapse show border-top border-danger mt-3" id="step3">
                                <form action="{{ route('quizzes.storeIntroduction') }}" method="post" autocomplete="off">
                                    @csrf
                                    <div class="row pt-3">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="my-2">
                                                <label for="mobile" class="form-label">
                                                    <i class="bi bi-telephone px-1"></i>
                                                    @lang('common_mobile_number')
                                                    <span class="required">*</span>
                                                </label>
                                                <x-input type="number" name="mobile" value="{{ old('mobile') }}" id="mobile" required />
                                                <div
                                                    class="form-text">@lang('register_step_1_mobile_number_description')</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="my-2">
                                                <label for="email" class="form-label">
                                                    <i class="bi bi-envelope px-1"></i>
                                                    @lang('common_email')
                                                </label>
                                                <x-input type="email" name="email" value="{{ old('email') }}" id="email" />
                                            </div>
                                        </div>

                                        <div class="my-2">
                                            <label for="name" class="form-label">
                                                <i class="bi bi-person px-1"></i>
                                                @lang('common_name')
                                                <span class="required">*</span>
                                            </label>
                                            <x-input type="text" name="name" value="{{ old('name') }}" id="name" required />
                                        </div>

                                        <div class="text-end">
                                            <button type="submit" class="btn btn-primary">@lang('common_record')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>


                </div>
            </div>
        </div>

    </section>
@endsection
