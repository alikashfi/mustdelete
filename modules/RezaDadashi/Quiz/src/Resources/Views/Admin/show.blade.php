@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('quizzes.index') }}">@lang('quizzes_title')</a></li>
    <li class="breadcrumb-item"><a href="#">@lang('quizzes_view')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('quizzes_view')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="row mt-2">

                            <div class="col-lg-4 d-flex align-items-center">
                                <img width="80" height="80" class="rounded-circle border border-3" src="{{ $quiz->user->thumb }}">
                                <a href="{{ route('users.show', $quiz->user->id) }}" class="p-2 text-decoration-none">{{ $quiz->user->name }}</a>
                            </div>

                            <div class="col-lg-4">
                                <div class="d-flex flex-column">
                                    <div class="d-flex align-items-center">
                                        <i class="bi bi-stack fs-3"></i>
                                        <span class="px-2 text-muted">@lang('quizzes_status'):</span>
                                        <span class="{{ $quiz->getCssClass() }}">@lang('quiz_status_' . $quiz->status)</span>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <i class="bi bi-question-diamond-fill fs-3"></i>
                                        <span class="px-2 text-muted">@lang('quizzes_type'):</span>
                                        <span>@lang($quiz->question->type)</span>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <i class="bi bi-patch-question fs-3"></i>
                                        <span class="px-2 text-muted">@lang('quizzes_view_question'):</span>
                                        <a href="{{ route('quizzes.showQuestion', $quiz->question->id) }}" class="text-decoration-none">
                                            <i class="bi bi-eye-fill fs-3"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="d-flex align-items-center">
                                    <i class="bi bi-pencil fs-3"></i>
                                    <span class="px-2 text-muted">@lang('quizzes_view_user_answer'):</span>
                                </div>
                                <div class="text-justify">
                                    {!! nl2br($quiz->answer) !!}
                                </div>
                            </div>

                        </div>

                        @if($quiz->status == \RezaDadashi\Quiz\Models\Quiz::STATUS_PENDING)
                            <div class="row mt-4 justify-content-center">
                                <form id="decision-quiz" method="post" action="{{ route('quizzes.decision', $quiz->id) }}">
                                    @csrf
                                    <div class="text-center">

                                        <input type="radio" onchange="this.form.submit()" class="btn-check" id="accept-quiz" name="decision" value="1" autocomplete="off">
                                        <label class="btn btn-success" for="accept-quiz">@lang('quizzes_btn_accept')</label>

                                        <input type="radio" onchange="this.form.submit()" class="btn-check" id="reject-quiz" name="decision" value="0" autocomplete="off">
                                        <label class="btn btn-danger" for="reject-quiz">@lang('quizzes_btn_reject')</label>
                                    </div>
                                </form>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
