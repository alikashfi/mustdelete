@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('quizzes.index') }}">@lang('quizzes_title')</a></li>
    <li class="breadcrumb-item"><a href="{{ route('quizzes.showQuestions') }}">@lang('quizzes_questions_title')</a></li>
    <li class="breadcrumb-item"><a href="#">@lang('questions_question_edit')</a></li>
@endsection

@section('content')
<div class="p-3 card">
    <div class="card border-primary">
        <div class="card-header bg-primary text-white">
            @lang('questions_question_edit')
        </div>
        <div class="card-body">
            <form action="{{ route('quizzes.updateQuestion', $question->id) }}" method="post" autocomplete="off">
                @csrf
                @method('PATCH')
                <div class="my-2">
                    <label for="type" class="form-label">@lang('questions_question_type')</label>
                    <x-select name="type" id="type">
                        @foreach(\RezaDadashi\Quiz\Models\Question::$questions as $questionType)
                            <option {{ $question->type == $questionType ? 'selected' : null }} value="{{ $questionType }}">@lang($questionType)</option>
                        @endforeach
                    </x-select>
                </div>

                <div class="my-2">
                    <label for="question" class="form-label">@lang('questions_question_question')</label>
                    <x-textarea name="question" rows="5" id="question" value="{{ $question->question }}" />
                </div>

                <div class="my-2">
                    <label for="answer" class="form-label">@lang('questions_question_answer')</label>
                    <x-textarea name="answer" rows="5" id="answer" value="{{ $question->answer }}" />
                </div>

                <div class="my-2">
                    <label for="min_2_not_exists_reject" class="form-label">@lang('absence_least_two_these_words_reject')</label>
                    <textarea class="form-control" name="min_2_not_exists_reject" rows="5" id="min_2_not_exists_reject">@foreach($question->rules['min_2_not_exists_reject'] as $word){{ $word . "\r\n" }}@endforeach</textarea>
                    <x-validation-error field="min_2_not_exists_reject" />
                </div>

                <div class="my-2">
                    <label for="min_2_exists_reject" class="form-label">@lang('reject_are_least_two_these_words')</label>
                    <textarea class="form-control" name="min_2_exists_reject" rows="5" id="min_2_exists_reject">@foreach($question->rules['min_2_exists_reject'] as $word){{ $word . "\r\n" }}@endforeach</textarea>
                    <x-validation-error field="min_2_exists_reject" />
                </div>

                <div class="my-2">
                    <label for="min_1_not_exists_reject" class="form-label">@lang('absence_least_one_these_words_reject')</label>
                    <textarea class="form-control" name="min_1_not_exists_reject" rows="5" id="min_1_not_exists_reject">@foreach($question->rules['min_1_not_exists_reject'] as $word){{ $word . "\r\n" }}@endforeach</textarea>
                    <x-validation-error field="min_1_not_exists_reject" />
                </div>

                <div class="my-2">
                    <label for="min_1_exists_reject" class="form-label">@lang('reject_is_least_one_these_words')</label>
                    <textarea class="form-control" name="min_1_exists_reject" rows="5" id="min_1_exists_reject">@foreach($question->rules['min_1_exists_reject'] as $word){{ $word . "\r\n" }}@endforeach</textarea>
                    <x-validation-error field="min_1_exists_reject" />
                </div>

                <div class="text-end">
                    <a class="btn btn-danger mt-2" href="{{ route('quizzes.showQuestions') }}">@lang('common_cancel')</a>
                    <button type="submit" class="btn btn-success mt-2">@lang('common_update')</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
