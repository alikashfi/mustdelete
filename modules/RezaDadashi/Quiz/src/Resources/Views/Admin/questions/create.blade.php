<div class="p-3 card">
    <div class="card border-primary">
        <div class="card-header bg-primary text-white">
            @lang('questions_create_new_question')
        </div>
        <div class="card-body">
            <form action="{{ route('quizzes.storeQuestion') }}" method="post" autocomplete="off">
                @csrf

                <div class="my-2">
                    <label for="type" class="form-label">@lang('questions_question_type')</label>
                    <x-select name="type" id="type">
                        @foreach(\RezaDadashi\Quiz\Models\Question::$questions as $questionType)
                            <option {{ old('type') == $questionType ? 'selected' : null }} value="{{ $questionType }}">@lang($questionType)</option>
                        @endforeach
                    </x-select>
                </div>

                <div class="my-2">
                    <label for="question" class="form-label">@lang('questions_question_question')</label>
                    <x-textarea name="question" rows="5" id="question" />
                </div>

                <div class="my-2">
                    <label for="answer" class="form-label">@lang('questions_question_answer')</label>
                    <x-textarea name="answer" rows="5" id="answer" />
                </div>

                <div class="my-2">
                    <label for="min_2_not_exists_reject" class="form-label">@lang('absence_least_two_these_words_reject')</label>
                    <x-textarea name="min_2_not_exists_reject" rows="5" id="min_2_not_exists_reject" />
                </div>

                <div class="my-2">
                    <label for="min_2_exists_reject" class="form-label">@lang('reject_are_least_two_these_words')</label>
                    <x-textarea name="min_2_exists_reject" rows="5" id="min_2_exists_reject" />
                </div>

                <div class="my-2">
                    <label for="min_1_not_exists_reject" class="form-label">@lang('absence_least_one_these_words_reject')</label>
                    <x-textarea name="min_1_not_exists_reject" rows="5" id="min_1_not_exists_reject" />
                </div>

                <div class="my-2">
                    <label for="min_1_exists_reject" class="form-label">@lang('reject_is_least_one_these_words')</label>
                    <x-textarea name="min_1_exists_reject" rows="5" id="min_1_exists_reject" />
                </div>

                <div class="text-end">
                    <button type="submit" class="btn btn-outline-primary mt-2">@lang('common_add')</button>
                </div>

            </form>
        </div>
    </div>
</div>
