@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('quizzes.index') }}">@lang('quizzes_title')</a></li>
    <li class="breadcrumb-item"><a href="{{ route('quizzes.showQuestions') }}">@lang('quizzes_questions_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-8">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('quizzes_questions_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">
                        <div class="table-responsive pt-2">
                            <table class="table align-middle">
                                <thead>
                                <tr>
                                    <th scope="col">@lang('questions_id')</th>
                                    <th scope="col">@lang('questions_question_maker')</th>
                                    <th scope="col">@lang('questions_question_type')</th>
                                    <th scope="col">@lang('questions_question_question')</th>
                                    <th scope="col">@lang('questions_question_answer')</th>
                                    <th scope="col">@lang('common_actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($questions as $question)
                                        <tr>
                                            <td scope="row">{{ $question->id }}</td>
                                            <td scope="row">{{ $question->user->name }}</td>
                                            <td scope="row">
                                                <span class="{{ $question->getCssClass() }}">@lang($question->type)</span>
                                            </td>
                                            <td scope="row">{{ \Illuminate\Support\Str::limit($question->question, 8, '...') }}</td>
                                            <td scope="row">{{ \Illuminate\Support\Str::limit($question->answer, 8, '...') }}</td>
                                            <td>
                                                <a class="bi bi-eye-fill text-info fs-6 px-1"
                                                   href="{{ route('quizzes.showQuestion', $question->id) }}" title="مشاهده"></a>
                                                <a class="bi bi-pencil-square text-success fs-6 px-1"
                                                   href="{{ route('quizzes.showEditQuestionForm', $question->id) }}" title="ویرایش"></a>
                                                <a data-route="{{ route('quizzes.destroyQuestion', $question->id) }}"
                                                   class="deleteItem bi bi-trash text-danger fs-6 px-1" href="javascript:;"
                                                   title="حذف"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-4 mt-3 mt-lg-0">

            @include('Quiz::Admin.questions.create')

        </div>

    </div>
@endsection
