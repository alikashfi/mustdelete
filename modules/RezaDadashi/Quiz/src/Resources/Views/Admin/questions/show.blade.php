@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('quizzes.index') }}">@lang('quizzes_title')</a></li>
    <li class="breadcrumb-item"><a href="{{ route('quizzes.showQuestions') }}">@lang('quizzes_questions_title')</a></li>
    <li class="breadcrumb-item"><a href="#">@lang('questions_question_view')</a></li>
@endsection

@section('content')
<div class="p-3 card">
    <div class="card border-success">
        <div class="card-header bg-success text-white">
            @lang('questions_question_view')
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 d-flex align-items-center">
                    <i class="bi bi-person-circle fs-3"></i>
                    <span class="mx-2 text-muted">@lang('questions_question_maker')</span>
                    <span>{{ $question->user->name }}</span>
                </div>
                <div class="col-lg-6">
                    <i class="bi bi-question-circle-fill fs-3"></i>
                    <span class="mx-2 text-muted">@lang('questions_question_question')</span>
                    <div class="text-justify">{{ $question->question }}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 d-flex align-items-center">
                    <i class="bi bi-patch-question-fill fs-3"></i>
                    <span class="mx-2 text-muted">@lang('questions_question_type')</span>
                    <span>@lang($question->type)</span>
                </div>
                <div class="col-lg-6">
                    <i class="bi bi-pen-fill fs-3"></i>
                    <span class="mx-2 text-muted">@lang('questions_question_answer')</span>
                    <div class="text-justify">{{ $question->answer }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
