@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('quizzes.index') }}">@lang('quizzes_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('quizzes_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="d-flex justify-content-between flex-column flex-lg-row">
                            <div class="dropdown bg-white border border-2 col-lg-4 my-2">
                                <div class="dropdown-toggle d-flex align-items-center justify-content-between p-2" data-bs-toggle="dropdown">@lang('quizzes_search_title')</div>
                                <div class="dropdown-menu col-4 w-100 mt-1 p-3 shadow">
                                    <form method="GET">
                                        <input type="text" name="user" value="{{ request('user') }}" placeholder="@lang('quizzes_user')" class="form-control mb-3">

                                        <select name="status" class="form-select mb-3">
                                            <option value="">-- @lang('quizzes_status') --</option>
                                            @foreach(\RezaDadashi\Quiz\Models\Quiz::$statuses as $quizStatus)
                                                <option
                                                    @if(request('status') == $quizStatus) selected @endif
                                                value="{{ $quizStatus }}">
                                                    @lang('quiz_status_' . $quizStatus)
                                                </option>
                                            @endforeach
                                        </select>

                                        <select name="question" class="form-select mb-3">
                                            <option value="">-- @lang('quizzes_type') --</option>
                                            @foreach(\RezaDadashi\Quiz\Models\Question::$questions as $questionType)
                                                <option
                                                    @if(request('question') == $questionType) selected @endif
                                                value="{{ $questionType }}">
                                                    @lang($questionType)
                                                </option>
                                            @endforeach
                                        </select>

                                        <div class="text-end">
                                            <button class="btn btn-primary" type="submit">
                                                <span>@lang('common_search')</span>
                                                <i class="bi bi-search text-white"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="text-end"><a class="btn btn-success my-2" href="{{ route('quizzes.showQuestions') }}">@lang('quizzes_questions_title')</a></div>
                        </div>

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead>
                                <tr>
                                    <th scope="col">@lang('quizzes_user')</th>
                                    <th scope="col">@lang('quizzes_type')</th>
                                    <th scope="col">@lang('quizzes_created_at')</th>
                                    <th scope="col">@lang('quizzes_status')</th>
                                    <th scope="col">@lang('common_actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($quizzes as $quiz)
                                    <tr>
                                        <td>{{ $quiz->user->name }}</td>
                                        <td>
                                            <span class="{{ $quiz->question->getCssClass() }}">@lang($quiz->question->type)</span>
                                        </td>
                                        <td>{{ getJalali($quiz->created_at) }}</td>
                                        <td>
                                            <span class="{{ $quiz->getCssClass() }}">@lang('quiz_status_' . $quiz->status)</span>
                                        </td>
                                        <td>
                                            <a class="bi bi-eye-fill text-info fs-6 px-1"
                                               href="{{ route('quizzes.show', $quiz->id) }}" title="@lang('quizzes_view')"></a>

                                            <a data-route="{{ route('quizzes.destroy', $quiz->id) }}"
                                               class="deleteItem bi bi-trash text-danger fs-6 px-1" href="javascript:;"
                                               title="حذف"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center d-flex justify-content-center">
                                {{ $quizzes->links('pagination::bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
