<?php

use Illuminate\Support\Facades\Route;

/** Cooperate Links */
Route::group([
    'middleware' => ['web'],
    'prefix' => 'cooperate'
], function () {

    Route::get('/quiz', [
        'uses'  => 'RezaDadashi\Quiz\Http\Controllers\QuizController@showQuizForm',
        'as' => 'quizzes.showQuizForm',
    ]);

    Route::post('/quiz/type', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'getQuiz'])
        ->name('quizzes.getQuiz');

    Route::get('/quiz/start', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'quiz'])
        ->name('quizzes.quiz');

    Route::post('/quiz/check', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'check'])
        ->name('quizzes.check');

    Route::get('/quiz/introduction', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'introduction'])
        ->name('quizzes.introduction');

    Route::post('/quiz/introduction', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'storeIntroduction'])
        ->name('quizzes.storeIntroduction');
});


/** Quizzes & Questions Links */
Route::group([
    'middleware' => ['web'],
    'prefix' => 'quizzes'
], function () {


    Route::get('/', [
        'uses'  => 'RezaDadashi\Quiz\Http\Controllers\QuizController@index',
        'as' => 'quizzes.index',
        'middleware' => ['auth', 'verified']
    ]);


    Route::delete('/quiz/{quiz}', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'destroy'])
        ->name('quizzes.destroy');

    Route::get('/quiz/{quiz}/show', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'show'])
        ->name('quizzes.show');

    Route::post('/quiz/{quiz}/decision', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'decision'])
        ->name('quizzes.decision');



    Route::get('/questions', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'showQuestions'])
        ->middleware('auth', 'verified')
        ->name('quizzes.showQuestions');

    Route::get('/questions/{question}/show', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'showQuestion'])
        ->middleware('auth', 'verified')
        ->name('quizzes.showQuestion');

    Route::post('/questions', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'storeQuestion'])
        ->middleware('auth', 'verified')
        ->name('quizzes.storeQuestion');

    Route::get('/questions/{question}/edit', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'showEditQuestionForm'])
        ->middleware('auth', 'verified')
        ->name('quizzes.showEditQuestionForm');

    Route::patch('/questions/{question}/update', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'updateQuestion'])
        ->middleware('auth', 'verified')
        ->name('quizzes.updateQuestion');

    Route::delete('/questions/{question}/questions', [\RezaDadashi\Quiz\Http\Controllers\QuizController::class, 'destroyQuestion'])
        ->middleware('auth', 'verified')
        ->name('quizzes.destroyQuestion');
});
