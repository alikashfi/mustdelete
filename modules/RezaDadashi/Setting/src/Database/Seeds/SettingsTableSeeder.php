<?php

namespace RezaDadashi\Setting\Database\Seeds;

use Illuminate\Database\Seeder;
use RezaDadashi\Setting\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::query()->truncate();

        foreach (Setting::$settings as $setting) {
            Setting::query()->create($setting);
        }
    }
}
