<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth', 'verified'],
    'prefix' => 'settings'
], function () {

    Route::get('/', [
        'uses' => 'RezaDadashi\Setting\Http\Controllers\SettingController@index',
        'as' => 'settings.index'
    ]);

    Route::patch('/{setting}/updatePersianToEnglish', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updatePersianToEnglish'])
        ->name('settings.updatePersianToEnglish');

    Route::patch('/{setting}/updateEnglishToPersian', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updateEnglishToPersian'])
        ->name('settings.updateEnglishToPersian');

    Route::patch('/{setting}/updateEnglishEditing', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updateEnglishEditing'])
        ->name('settings.updateEnglishEditing');

    Route::patch('/{setting}/updateCardNumber', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updateCardNumber'])
        ->name('settings.updateCardNumber');

    Route::patch('/{setting}/updateDepositPercent', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updateDepositPercent'])
        ->name('settings.updateDepositPercent');

    Route::patch('/{setting}/updatePaymentFileReceipt', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updatePaymentFileReceipt'])
        ->name('settings.updatePaymentFileReceipt');


    Route::patch('/{setting}/updateTranslateWordPriceExpert', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updateTranslateWordPriceExpert'])
        ->name('settings.updateTranslateWordPriceExpert');

    Route::patch('/{setting}/updateEditingWordPriceExpert', [\RezaDadashi\Setting\Http\Controllers\SettingController::class, 'updateEditingWordPriceExpert'])
        ->name('settings.updateEditingWordPriceExpert');

});
