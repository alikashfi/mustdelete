@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('settings.index') }}">@lang('settings_title')</a></li>
@endsection

@section('content')

    <div class="row">


        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('price_each_word_persian_to_english')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH)->id }}">
                        <form action="{{ route('settings.updatePersianToEnglish', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div>
                                <label for="quiz-type" class="form-label">
                                    @lang('Toman')
                                </label>
                                <div class="input-group">
                                    <input name="value_persian_to_english" type="text" class="@error('value_persian_to_english') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH)->value }}">
                                    <x-validation-error field="value_persian_to_english" />
                                    <button class="btn btn-primary" type="submit" id="button-addon1">@lang('settings_save')</button>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('settings.updatePersianToEnglish', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH_DOLLAR)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="mt-2">
                                <label for="quiz-type" class="form-label">
                                    @lang('Dollar')
                                </label>
                                <div class="input-group">
                                    <input name="value_persian_to_english" type="text" class="@error('value_persian_to_english') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH_DOLLAR)->value }}">
                                    <x-validation-error field="value_persian_to_english" />
                                    <button class="btn btn-success" type="submit" id="button-addon1">@lang('settings_save')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('price_each_word_english_to_persian')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN)->id }}">
                        <form action="{{ route('settings.updateEnglishToPersian', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div>
                                <label for="quiz-type" class="form-label">
                                    @lang('Toman')
                                </label>
                                <div class="input-group">
                                    <input name="value_english_to_persian" type="text" class="@error('value_english_to_persian') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN)->value }}">
                                    <x-validation-error field="value_english_to_persian" />
                                    <button class="btn btn-primary" type="submit" id="button-addon1">@lang('settings_save')</button>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('settings.updateEnglishToPersian', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN_DOLLAR)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="mt-2">
                                <label for="quiz-type" class="form-label">
                                    @lang('Dollar')
                                </label>
                                <div class="input-group">
                                    <input name="value_english_to_persian" type="text" class="@error('value_english_to_persian') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN_DOLLAR)->value }}">
                                    <x-validation-error field="value_english_to_persian" />
                                    <button class="btn btn-success" type="submit" id="button-addon1">@lang('settings_save')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('price_each_english_editing')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_ENGLISH_EDITING)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_ENGLISH_EDITING)->id }}">
                        <form action="{{ route('settings.updateEnglishEditing', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_ENGLISH_EDITING)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div>
                                <label for="quiz-type" class="form-label">
                                    @lang('Toman')
                                </label>
                                <div class="input-group">
                                    <input name="value_english_editing" type="text" class="@error('value_english_editing') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_ENGLISH_EDITING)->value }}">
                                    <x-validation-error field="value_english_editing" />
                                    <button class="btn btn-primary" type="submit" id="button-addon1">@lang('settings_save')</button>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('settings.updateEnglishEditing', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_ENGLISH_EDITING_DOLLAR)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="mt-2">
                                <label for="quiz-type" class="form-label">
                                    @lang('Dollar')
                                </label>
                                <div class="input-group">
                                    <input name="value_english_editing" type="text" class="@error('value_english_editing') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_ENGLISH_EDITING_DOLLAR)->value }}">
                                    <x-validation-error field="value_english_editing" />
                                    <button class="btn btn-success" type="submit" id="button-addon1">@lang('settings_save')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('card_number_for_payment_outside_software')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_CARD_NUMBER)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_CARD_NUMBER)->id }}">
                        <form action="{{ route('settings.updateCardNumber', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_CARD_NUMBER)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="input-group">
                                <input name="value_card_number" type="number" class="@error('value_card_number') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_CARD_NUMBER)->value }}">
                                <x-validation-error field="value_card_number" />
                                <button class="btn btn-primary" type="submit" id="button-addon1">@lang('settings_save')</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('deposit_payment_percentage')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->id }}">
                        <form action="{{ route('settings.updateDepositPercent', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="input-group">
                                <input name="value_deposit_percent" type="number" class="@error('value_deposit_percent') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->value }}">
                                <x-validation-error field="value_deposit_percent" />
                                <button class="btn btn-primary" type="submit" id="button-addon1">@lang('settings_save')</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('send_payment_receipt')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->id }}">
                        <form action="{{ route('settings.updatePaymentFileReceipt', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->id) }}" method="post">
                            @csrf
                            @method('PATCH')

                            <div class="text-center">
                                @if($getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value == 0)
                                    <input name="value" onchange="this.form.submit()" value="1" type="checkbox" class="btn-check" id="btn-payment-file-receipt-true" autocomplete="off">
                                    <label class="btn btn-success" for="btn-payment-file-receipt-true">@lang('settings_enable')</label>
                                @endif

                                @if($getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value == 1)
                                    <input name="value" onchange="this.form.submit()" value="0" type="checkbox" class="btn-check" id="btn-payment-file-receipt-false" autocomplete="off">
                                    <label class="btn btn-danger" for="btn-payment-file-receipt-false">@lang('settings_disable')</label>
                                @endif
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('translate_word_price_for_expert')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_WORD_PRICE_EXPERT)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_WORD_PRICE_EXPERT)->id }}">
                        <form action="{{ route('settings.updateTranslateWordPriceExpert', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_WORD_PRICE_EXPERT)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="input-group">
                                <input name="value" type="number" class="@error('value_deposit_percent') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_TRANSLATE_WORD_PRICE_EXPERT)->value }}">
                                <x-validation-error field="value" />
                                <button class="btn btn-primary" type="submit" id="button-addon1">@lang('settings_save')</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="p-3 my-2 card">
                <div class="card border">
                    <div class="card-header bg-gradient bg-light">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span class="bi bi-gear"></span>
                                <span>@lang('editing_word_price_for_expert')</span>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_EDITING_WORD_PRICE_EXPERT)->id }}" aria-expanded="false"></i>
                        </div>
                    </div>
                    <div class="card-body show collapse" id="panel-toggle-{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_EDITING_WORD_PRICE_EXPERT)->id }}">
                        <form action="{{ route('settings.updateEditingWordPriceExpert', $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_EDITING_WORD_PRICE_EXPERT)->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="input-group">
                                <input name="value" type="number" class="@error('value_deposit_percent') is-invalid @enderror form-control" value="{{ $getSettingByKey->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_EDITING_WORD_PRICE_EXPERT)->value }}">
                                <x-validation-error field="value" />
                                <button class="btn btn-primary" type="submit" id="button-addon1">@lang('settings_save')</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection
