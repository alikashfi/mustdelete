<?php

namespace RezaDadashi\Setting\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded = [];

    const SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH = 'persian to english';
    const SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH_DOLLAR = 'persian to english dollar';
    const SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN = 'english to persian';
    const SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN_DOLLAR = 'english to persian dollar';
    const SETTING_KEY_ENGLISH_EDITING = 'english editing';
    const SETTING_KEY_ENGLISH_EDITING_DOLLAR = 'english editing dollar';
    const SETTING_KEY_CARD_NUMBER = 'card number';
    const SETTING_KEY_PERCENT_DEPOSIT_PAYMENT = 'percent deposit payment';
    const SETTING_KEY_PAYMENT_FILE_RECEIPT = 'payment file receipt';
    const SETTING_KEY_TRANSLATE_WORD_PRICE_EXPERT = 'translate word price expert';
    const SETTING_KEY_EDITING_WORD_PRICE_EXPERT = 'editing word price expert';

    public static $settings = [
        [
            'key' => self::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH,
            'value' => '300',
            'title' => 'قیمت هرکلمه ترجمه فارسی به انگلیسی(تومان)'
        ],
        [
            'key' => self::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH_DOLLAR,
            'value' => '1',
            'title' => 'قیمت هرکلمه ترجمه فارسی به انگلیسی(دلار)'
        ],
        [
            'key' => self::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN,
            'value' => '500',
            'title' => 'قیمت هرکلمه ترجمه انگلیسی به فارسی(تومان)'
        ],
        [
            'key' => self::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN_DOLLAR,
            'value' => '1',
            'title' => 'قیمت هرکلمه ترجمه انگلیسی به فارسی(دلار)'
        ],
        [
            'key' => self::SETTING_KEY_ENGLISH_EDITING,
            'value' => '200',
            'title' => 'قیمت هرکلمه ویراستاری انگلیسی(تومان)'
        ],
        [
            'key' => self::SETTING_KEY_ENGLISH_EDITING_DOLLAR,
            'value' => '1',
            'title' => 'قیمت هرکلمه ویراستاری انگلیسی(دلار)'
        ],
        [
            'key' => self::SETTING_KEY_CARD_NUMBER,
            'value' => '1111111111111111',
            'title' => 'شماره کارت جهت پرداخت خارج از نرم افزار'
        ],
        [
            'key' => self::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT,
            'value' => '25',
            'title' => 'درصد پرداخت بيعانه'
        ],
        [
            'key' => self::SETTING_KEY_PAYMENT_FILE_RECEIPT,
            'value' => '1',
            'title' => 'ارسال رسید فایل پرداخت'
        ],
        [
            'key' => self::SETTING_KEY_TRANSLATE_WORD_PRICE_EXPERT,
            'value' => '200',
            'title' => 'قیمت هرکلمه ترجمه (تومان)'
        ],
        [
            'key' => self::SETTING_KEY_EDITING_WORD_PRICE_EXPERT,
            'value' => '150',
            'title' => 'قیمت هرکلمه ویراستاری (تومان)'
        ],
    ];
}
