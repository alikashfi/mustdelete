<?php

namespace RezaDadashi\Setting\Providers;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\Setting\Database\Seeds\SettingsTableSeeder;
use RezaDadashi\Setting\Models\Setting;
use RezaDadashi\Setting\Policies\SettingPolicy;

class SettingServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/setting_routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Setting');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');

        Gate::policy(Setting::class, SettingPolicy::class);

        DatabaseSeeder::$seeders[] = SettingsTableSeeder::class;
    }

    public function boot()
    {
        config()->set('sidebar.items.settings', [
            'icon' => 'bi bi-gear-fill',
            'title' => ['fa' => 'تنظیمات', 'en' => 'Settings'],
            'url' => route('settings.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_SETTINGS
            ]
        ]);
    }
}
