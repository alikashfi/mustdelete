<?php

namespace RezaDadashi\Setting\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Setting\Models\Setting;

class SettingRepository extends Repository
{
    public function model()
    {
        return Setting::class;
    }

    public function getByKey($key)
    {
        return Setting::query()->where('key', $key)->first();
    }

    public function updateSetting($settingId, $value)
    {
        $this->update(
            ['id' => $settingId],
            ['value' => $value]
        );
    }
}
