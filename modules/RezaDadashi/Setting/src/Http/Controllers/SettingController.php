<?php

namespace RezaDadashi\Setting\Http\Controllers;

use App\Http\Controllers\Controller;
use RezaDadashi\Setting\Http\Requests\UpdateCardNumberRequest;
use RezaDadashi\Setting\Http\Requests\UpdateDepositPercentRequest;
use RezaDadashi\Setting\Http\Requests\UpdateEnglishEditingRequest;
use RezaDadashi\Setting\Http\Requests\UpdateEnglishToPersianRequest;
use RezaDadashi\Setting\Http\Requests\UpdatePaymentFileReceiptRequest;
use RezaDadashi\Setting\Http\Requests\UpdatePersianToEnglishRequest;
use RezaDadashi\Setting\Http\Requests\UpdateSettingRequest;
use RezaDadashi\Setting\Http\Requests\UpdateTranslateOrEditorsPriceForExpertsRequest;
use RezaDadashi\Setting\Models\Setting;
use RezaDadashi\Setting\Repositories\SettingRepository;

class SettingController extends Controller
{
    private $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    public function index()
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $getSettingByKey = $this->settingRepository;
        return view('Setting::index', compact('getSettingByKey'));
    }

    public function updatePersianToEnglish($settingId, UpdatePersianToEnglishRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value_persian_to_english);

        newFeedback();
        return redirect()->route('settings.index');
    }

    public function updateEnglishToPersian($settingId, UpdateEnglishToPersianRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value_english_to_persian);

        newFeedback();
        return redirect()->route('settings.index');
    }

    public function updateEnglishEditing($settingId, UpdateEnglishEditingRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value_english_editing);

        newFeedback();
        return redirect()->route('settings.index');
    }

    public function updateCardNumber($settingId, UpdateCardNumberRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value_card_number);

        newFeedback();
        return redirect()->route('settings.index');
    }

    public function updateDepositPercent($settingId, UpdateDepositPercentRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value_deposit_percent);

        newFeedback();
        return redirect()->route('settings.index');
    }

    public function updatePaymentFileReceipt($settingId, UpdatePaymentFileReceiptRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value);

        newFeedback();
        return redirect()->route('settings.index');
    }

    public function updateTranslateWordPriceExpert($settingId, UpdateTranslateOrEditorsPriceForExpertsRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value);

        newFeedback();
        return redirect()->route('settings.index');
    }

    public function updateEditingWordPriceExpert($settingId, UpdateTranslateOrEditorsPriceForExpertsRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Setting::class);

        $this->settingRepository->findOrFail($settingId);

        $this->settingRepository->updateSetting($settingId, $request->value);

        newFeedback();
        return redirect()->route('settings.index');
    }
}
