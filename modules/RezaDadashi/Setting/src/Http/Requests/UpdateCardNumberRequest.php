<?php

namespace RezaDadashi\Setting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use RezaDadashi\User\Rules\ValidCardNumber;

class UpdateCardNumberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value_card_number' => ['required', 'string', 'size:16', new ValidCardNumber()]
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'value_card_number' => 'شماره کارت',
        ] : [];
    }
}
