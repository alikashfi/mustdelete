<?php

namespace RezaDadashi\Setting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class UpdatePaymentFileReceiptRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => ['required', 'boolean']
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'value' => 'رسید فایل پرداخت',
        ] : [];
    }
}
