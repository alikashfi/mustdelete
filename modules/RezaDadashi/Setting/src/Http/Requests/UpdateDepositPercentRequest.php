<?php

namespace RezaDadashi\Setting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class UpdateDepositPercentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value_deposit_percent' => ['required', 'numeric', 'min:1', 'max:100']
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'value_deposit_percent' => 'درصد پرداخت بیعانه',
        ] : [];
    }
}
