<?php

namespace RezaDadashi\Setting\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Models\User;

class SettingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage($user)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_SETTINGS)) return true;
    }
}
