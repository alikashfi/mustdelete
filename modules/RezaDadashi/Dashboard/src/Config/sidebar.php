<?php
return [
    'items' => [
        'dashboard' => [],
        'requests' => [],
        'reports' => [],
        'my-requests' => [],
        'cooperation' => [],
        'discount' => [],
        'quizzes' => [],
        'payments' => [],
        'users' => [],
        'partners' => [],
        'my-settlement' => [],
        'role-permissions' => [],
        'settings' => [],
        'profile' => [],
    ]
];
