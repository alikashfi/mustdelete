<!doctype html>
<html dir="{{ App::getLocale() == 'fa' ? 'rtl' : 'ltr' }}">
    {{--  Header  --}}
    @include('Dashboard::layouts.head')
<body data-dir="{{ App::getLocale() == 'fa' ? 'fa' : 'en' }}" class="">

    @include('Core::layouts.feedbacks')

<section class="">

    {{--  Header  --}}
    @include('Dashboard::layouts.header')

    <div class="text-start">

        {{--   Breadcrumb     --}}
        @include('Dashboard::layouts.breadcrumb')


        <section class="p-3 pt-4">
            {{--    Content    --}}
            @yield('content')
        </section>

    </div>
</section>




    {{-- Sidebar --}}
    @include('Dashboard::layouts.sidebar')
    @yield('js')
</body>
</html>
