@extends('Dashboard::master')

@section('content')
    @if(auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPER_ADMIN))
        <div id="dashboard-report-items" class="row">
            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_users_count')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($usersCount) }}</span>
                    </div>
                    <i class="bi bi-people-fill bg-danger rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>

            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_earn_today')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($todayBenefit) }} @lang('Toman')</span>
                    </div>
                    <i class="bi bi-graph-up-arrow bg-success rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>

            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_earnings_last_7_days')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($last7DayBenefit) }} @lang('Toman')</span>
                    </div>
                    <i class="bi bi-coin bg-info rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>

            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_earnings_last_30_days')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($last30DaysBenefit) }} @lang('Toman')</span>
                    </div>
                    <i class="bi bi-graph-up-arrow bg-primary rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>

            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_requests_count')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($allRequestsCount) }}</span>
                    </div>
                    <i class="bi bi-briefcase bg-info rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>

            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_ongoing_requests_count')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($doingRequestsCount) }}</span>
                    </div>
                    <i class="bi bi-briefcase bg-warning rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>

            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_supervisors_count')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($supervisorsCount) }}</span>
                    </div>
                    <i class="bi bi-person-check-fill bg-secondary rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>

            <div class="col-lg-3 mb-4">
                <div class="bg-white rounded-pill px-4 py-3 shadow d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-column">
                        <span class="fs-13">@lang('dashboard_experts_count')</span>
                        <span class="text-start text-muted fs-5 fw-500">{{ number_format($expertsCount) }}</span>
                    </div>
                    <i class="bi bi-person-check bg-danger rounded-circle d-flex justify-content-center align-items-center fs-5 text-white"></i>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="bg-white p-4">
                    <div id="chart" class="w-100 overflow-auto"></div>
                </div>
            </div>
        </div>
    @endif

    @if(auth()->user()->hasAnyPermission([
        \RezaDadashi\RolePermissions\Models\Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH,
        \RezaDadashi\RolePermissions\Models\Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN,
        \RezaDadashi\RolePermissions\Models\Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR,
    ]))
        <div class="row mb-4">
            <div class="col-12">
                <div class="card p-3">
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <span>@lang('your_doing_requests')</span>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped align-middle">
                            <thead>
                            <tr>
                                <th scope="col">@lang('request_id')</th>
                                <th scope="col">@lang('request_created_at')</th>
                                <th scope="col">@lang('request_type')</th>
                                <th scope="col">@lang('request_status')</th>
                                <th scope="col">@lang('common_actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($myDoingRequests as $doingRequest)
                                    <tr class="">
                                        <td>{{ $doingRequest->id }}</td>
                                        <td>{{ getJalali($doingRequest->created_at) }}</td>
                                        <td>
                                            <span class="{{ $doingRequest->tableColor() }}">
                                                @lang($doingRequest->type)
                                            </span>
                                        </td>
                                        <td>
                                            <span class="{{ $doingRequest->getStatus() }}">
                                                @lang($doingRequest->status)
                                            </span>
                                        </td>
                                        <td>
                                            <a class="text-decoration-none"
                                               href="{{ route('requests.show', $doingRequest->id) }}">
                                                @lang('request_view')
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card p-3">
                <div class="d-flex justify-content-between align-items-center mb-2">
                    <span>@lang('your_transactions')</span>

                    <a class="btn btn-success my-2" href="{{ route('requests.newRequest') }}">
                        @lang('my_request_new_request')
                    </a>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped align-middle">
                        <thead>
                        <tr>
                            <th scope="col">@lang('common_id')</th>
                            <th scope="col">@lang('common_mobile_number')</th>
                            <th scope="col">@lang('payments_price')</th>
                            <th scope="col">@lang('payments_type')</th>
                            <th scope="col">@lang('payments_date')</th>
                            <th scope="col">@lang('payments_status')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($myPayments as $payment)
                                <tr>
                                    <th>{{ $payment->request_id }}</th>
                                    <td>{{ $payment->user->mobile }}</td>
                                    <td>{{ number_format($payment->amount) }} @lang('Toman')</td>
                                    <td>@lang('payment_type_' . $payment->type)</td>
                                    <td>{{ getJalali($payment->created_at) }}</td>
                                    <td>
                                        @if($payment->status == \RezaDadashi\Payment\Models\Payment::STATUS_SUCCESS)
                                            <span class="text-success">@lang("payment_status_" . $payment->status)</span>
                                        @elseif($payment->status == \RezaDadashi\Payment\Models\Payment::STATUS_PENDING)
                                            <form name="{{ "payForm{$payment->id}" }}" action="{{ route('requests.deposit', ['request' => $payment->request_id, 'existing_payment' => 1]) }}" method="post">
                                                @csrf
                                                <span onclick="{{ "payForm{$payment->id}" }}.submit()" class="text-warning">@lang("payment_status_" . $payment->status)</span>
                                            </form>
                                        @elseif($payment->status == \RezaDadashi\Payment\Models\Payment::STATUS_FAIL)
                                            <span class="text-danger">@lang("payment_status_" . $payment->status)</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="text-center d-flex justify-content-center">
                        {{ $myPayments->links('pagination::bootstrap-4') }}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@include('Payment::chart')
