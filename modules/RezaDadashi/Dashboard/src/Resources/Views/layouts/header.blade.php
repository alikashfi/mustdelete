<section id="header" class="px-3 py-2 d-flex justify-content-between align-items-center">
    <div>
        <a href="#" id="btn-reza-sidebar" class="bi bi-text-right fs-4" data-bs-toggle="offcanvas"
           data-bs-target="#reza-sidebar"></a>
    </div>
    <div class="d-flex align-items-center">

        <div class="d-flex align-items-center px-2">
            @if (\Illuminate\Support\Facades\App::getLocale() == 'fa')
                <a href="{{ route('changeLang') . '?lang=en' }}" class="text-decoration-none d-flex align-items-center">
                    <img class="rounded-circle mx-1" width="20" src="{{ asset('/assets/img/lang/en.jpg') }}"
                         alt="{{ config()->get("translationLang.languages.en") }}">
                    <span class="text-black-50 fs-12">{{ config()->get("translationLang.languages.en") }}</span>
                </a>
            @else
                <a href="{{ route('changeLang') . '?lang=fa' }}" class="text-decoration-none d-flex align-items-center">
                    <img class="rounded-circle mx-1" width="20" src="{{ asset('/assets/img/lang/fa.jpg') }}"
                         alt="{{ config()->get("translationLang.languages.fa") }}">
                    <span class="text-black-50 fs-12">{{ config()->get("translationLang.languages.fa") }}</span>
                </a>
            @endif
        </div>


        <div class="d-flex align-items-center px-2">
            <div class="dropdown position-relative">

                @if($allUnreadComments = (new \RezaDadashi\Comment\Repositories\CommentRepository())->getUnreadCommentByReceiverIdAndRequestId(auth()->id())->groupBy(['request_id', 'step']))
                    @if($allUnreadComments->count())
                        <span id="unreadCommentCount" class="position-absolute bg-danger p-1 text-white border-2 border-white border rounded-circle">
                             {{ $allUnreadComments->count() <= 99 ? $allUnreadComments->count() : '+99' }}
                         </span>

                        <a id="unreadComment" class="dropdown-toggle text-decoration-none text-muted" href=":javascript;" data-bs-toggle="dropdown">
                            <i class="bi bi-envelope-fill fs-4 d-flex"></i>
                        </a>
                    @endif
                @endif

                <ul id="unreadCommentDropdown" class="dropdown-menu shadow">
                    @php $flag = null; @endphp
                    @foreach((new \RezaDadashi\Comment\Repositories\CommentRepository())->getUnreadCommentByReceiverIdAndRequestId(auth()->id()) as $unreadComment)
                        @if ($flag != $unreadComment->request_id . '_' . $unreadComment->step)
                        <li>
                            <a class="dropdown-item" href="{{ route('comments.link', [$unreadComment->request_id, $unreadComment->step]) }}">
                                <div class="d-flex align-items-center">
                                    <img width="30" height="30" class="rounded-circle border border-secondary border-2" src="{{ $unreadComment->user->thumb }}">
                                    <div class="d-flex flex-column ps-2">
                                        <div class="d-flex align-items-center">
                                            <span class="fs-12 text-muted pe-1">{{ \Illuminate\Support\Str::limit($unreadComment->user->name, 5,  '...') }} / </span>
                                            <span class="fs-12">{{ \Illuminate\Support\Str::limit($unreadComment->body, 12, '...') }}</span>
                                        </div>
                                        <div>
                                           <i class="text-muted fs-12 bi bi-clock-fill"></i>
                                            <span class="text-muted fs-12">{{ $unreadComment->created_at->diffForHumans() }}</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li><hr class="dropdown-divider"></li>
                        @endif
                        @php $flag = $unreadComment->request_id . '_' . $unreadComment->step; @endphp
                    @endforeach
                </ul>
            </div>
        </div>



        <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle ps-2" data-bs-toggle="dropdown">
            <img src="{{ auth()->user()->thumb }}" alt="{{ auth()->user()->name }}" class="rounded-circle" width="32"
                 height="32">
            <span>{{ auth()->user()->name }}</span>
        </a>
        <ul class="dropdown-menu text-small shadow">
            <li>
                <a class="dropdown-item d-flex align-items-center" href="{{ route('users.profile') }}">
                    <i class="bi bi-person-lines-fill pe-2"></i>
                    <span>@lang('common_header_profile')</span>
                </a>
            </li>
            @can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_SETTINGS)
                <li>
                    <a class="dropdown-item d-flex align-items-center" href="{{ route('settings.index') }}">
                        <i class="bi bi-gear pe-2"></i>
                        <span>@lang('common_header_settings')</span>
                    </a>
                </li>
            @endcan
            <li>
                <hr class="dropdown-divider">
            </li>
            <li>
                <a class="dropdown-item d-flex align-items-center" href="javascript:;"
                   onclick="document.getElementById('logout-form').submit();">
                    <i class="bi bi-box-arrow-in-left pe-2"></i>
                    <span>@lang('common_header_logout')</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="post">@csrf</form>
            </li>
        </ul>
    </div>
</section>
