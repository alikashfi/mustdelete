<nav class="bg-white border-bottom breadcrumb-shadow">
    <ol class="breadcrumb m-0 p-2">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('dashboard')</a></li>
        @yield('breadcrumb')
    </ol>
</nav>
