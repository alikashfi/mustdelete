<div id="reza-sidebar" class="is-show offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false">
    <div class="offcanvas-header text-white d-flex justify-content-center align-items-center">
        <div class="d-flex justify-content-center align-items-center text-center">
            <a class="text-decoration-none text-white px-1 fs-13" href="{{ route('home') }}">
                <img width="40" src="{{ asset('/assets/img/logo-white.png') }}" alt="">
                @lang('app_name')
            </a>
        </div>
    </div>
    <div class="offcanvas-body">

        <div class="d-flex flex-column justify-content-center align-items-center">
            <div>
                <img class="rounded-circle border border-2" src="{{ auth()->user()->thumb }}" width="80" height="80">
            </div>
            <div class="px-2 text-center pt-2">
                <span class="text-white">{{ auth()->user()->name }}</span>
                <span class="text-white-50 d-block mt-1">@lang('sidebar_welcome')</span>
            </div>
        </div>


        <div id="sidebar-links" class="mt-4">


            <ul class="list-unstyled">

                @foreach(config('sidebar.items') as $sidebarItem)
                    @if(! array_key_exists('permission', $sidebarItem) ||
                    auth()->user()->hasAnyPermission($sidebarItem['permission']) ||
                    auth()->user()->hasPermissionTo(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPER_ADMIN)
                    )
                        <li class="my-2">
                            <a href="{{ $sidebarItem['url'] }}"
                               class="@if(str_starts_with(request()->url(), $sidebarItem['url'])) active @endif link-item rounded-pill py-1 px-2 d-flex align-items-center">
                                <i class="{{ $sidebarItem['icon'] }} fs-5 mt-1"></i>
                                <span
                                    class="px-2">
                                    {{ \Illuminate\Support\Facades\App::getLocale()
                                        == 'fa' ? $sidebarItem['title']['fa'] :
                                         $sidebarItem['title']['en']
                                     }}
                                </span>
                            </a>
                        </li>
                    @endif
                @endforeach

                {{--                <li>--}}
                {{--                    <a href="/" class="link-item rounded-pill py-1 px-2 d-flex align-items-center">--}}
                {{--                        <i class="bi bi-grid-fill fs-5 mt-1"></i>--}}
                {{--                        <span class="px-2">پیشخوان</span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}

            </ul>

        </div>

    </div>
</div>
