<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth', 'verified']
], function ($router) {
    $router->get('/home', [
        'uses' => 'RezaDadashi\Dashboard\Http\Controllers\DashboardController@home',
        'as' => 'home'
    ]);
});
