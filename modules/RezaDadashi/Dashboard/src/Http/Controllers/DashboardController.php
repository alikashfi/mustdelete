<?php

namespace RezaDadashi\Dashboard\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use RezaDadashi\Core\Services\SMSService;
use RezaDadashi\Payment\Repositories\PaymentRepository;
use RezaDadashi\Request\Repositories\RequestRepository;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Repositories\UserRepository;

class DashboardController extends Controller
{
    private $paymentRepository;
    private $userRepository;
    private $requestRepository;

    public function __construct(PaymentRepository $paymentRepository, UserRepository $userRepository, RequestRepository $requestRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->userRepository = $userRepository;
        $this->requestRepository = $requestRepository;
    }

    public function home()
    {
        $usersCount = $this->userRepository->getUsersCount();
        $todayBenefit = $this->paymentRepository->getPaymentsByPeriod(Carbon::now()->startOfDay(), Carbon::now()->endOfDay());
        $last7DayBenefit = $this->paymentRepository->getPaymentsByPeriod(Carbon::now()->startOfDay(), Carbon::now()->subDays(7)->endOfDay());
        $last30DaysBenefit = $this->paymentRepository->getNDaysPayment(-30);
        $allRequestsCount = $this->requestRepository->getRequestsCountByStatus();
        $doingRequestsCount = $this->requestRepository->getRequestsCountByStatus(\RezaDadashi\Request\Models\Request::STATUS_TAKE);
        $supervisorsCount = $this->userRepository->getUsersCountByPermission(Permission::PERMISSION_SUPERVISOR);
        $expertsCount = $this->userRepository->getUsersCountByPermission([Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH, Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN, Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR]);
        $myPayments = $this->paymentRepository->getUserRecentPayments(auth()->id());
        $myDoingRequests = $this->requestRepository->getMyDoingRequestsByUserId(auth()->id());


        $dates = collect();
        foreach (range(-30, 0) as $i) {
            $dates->put(now()->addDays($i)->format('Y-m-d'), 0);
        }

        $summery = $this->paymentRepository->getDailySummery($dates);
        $requestsCount = $this->requestRepository->getRequestsCount();
        $persianToEnglishCount = isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_PERSIAN_TO_ENGLISH]) ? isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_PERSIAN_TO_ENGLISH]) : 0;
        $englishToPersianCount = isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_TO_PERSIAN]) ? isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_TO_PERSIAN]) : 0;
        $englishEditorCount = isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_EDITING]) ? isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_EDITING]) : 0;

        return view('Dashboard::index', compact(
            'usersCount',
            'supervisorsCount',
            'expertsCount',
            'allRequestsCount',
            'doingRequestsCount',
            'todayBenefit',
            'last7DayBenefit',
            'last30DaysBenefit',
            'myPayments',
            'persianToEnglishCount',
            'englishToPersianCount',
            'englishEditorCount',
            'dates',
            'summery',
            'myDoingRequests'
        ));
    }
}
