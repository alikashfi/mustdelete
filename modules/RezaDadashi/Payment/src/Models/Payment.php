<?php

namespace RezaDadashi\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use RezaDadashi\Discount\Models\Discount;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\User\Models\User;

class Payment extends Model
{
    protected $guarded = [];

    const TYPE_DEPOSIT = 'deposit'; // بيعانه
    const TYPE_SETTLEMENT = 'settlement'; // تسويه
    const TYPE_FILE = 'file'; // فايل
    public static $types = [
        self::TYPE_DEPOSIT,
        self::TYPE_SETTLEMENT,
        self::TYPE_FILE
    ];

    const STATUS_PENDING = 'pending';
    const STATUS_CANCELED = 'canceled';
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';
    public static $statuses = [
        self::STATUS_PENDING,
        self::STATUS_CANCELED,
        self::STATUS_SUCCESS,
        self::STATUS_FAIL,
    ];

    public function getTableCss()
    {
        switch ($this->status) {
            case self::STATUS_PENDING:
                return "table-warning";
                break;
            case self::STATUS_CANCELED:
            case self::STATUS_FAIL:
                return "table-danger";
                break;
            case self::STATUS_SUCCESS:
                return "table-success";
                break;
            default:
                return "";
        }
    }

    public function request()
    {
        return $this->belongsTo(Request::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }
}
