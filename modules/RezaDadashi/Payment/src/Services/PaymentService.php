<?php

namespace RezaDadashi\Payment\Services;

use RezaDadashi\Discount\Repositories\DiscountRepository;
use RezaDadashi\Payment\Gateways\Gateway;
use RezaDadashi\Payment\Models\Payment;
use RezaDadashi\Payment\Repositories\PaymentRepository;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\User\Models\User;

class PaymentService
{
    public static function generate($amount, $type, Request $request, User $user, $discountCode = null)
    {
        if ($amount <= 0 || is_null($request->id) || is_null($user->id)) return false;

        $gateway = resolve(Gateway::class);
        $invoiceId = $gateway->request($amount, $request->type);


        $discountRepo = new DiscountRepository();
        $discount = $discountRepo->getByDiscountCode($discountCode);

        return resolve(PaymentRepository::class)->store([
            'user_id' => $user->id,
            'discount_id' => $discount ? $discount->id : null,
            'request_id' => $request->id,
            'media_id' => null,
            'type' => $type,
            'amount' => $amount,
            'invoice_id' => $invoiceId,
            'gateway' => $gateway->getName(),
            'status' => Payment::STATUS_PENDING,
        ]);
    }
}
