<?php

namespace RezaDadashi\Payment\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RezaDadashi\Payment\Events\PaymentWasSuccessful;
use RezaDadashi\Payment\Gateways\Gateway;
use RezaDadashi\Payment\Models\Payment;
use RezaDadashi\Payment\Repositories\PaymentRepository;
use RezaDadashi\Request\Models\History;
use RezaDadashi\Request\Repositories\HistoryRepository;
use RezaDadashi\Request\Repositories\RequestRepository;

class PaymentController extends Controller
{
    private $paymentRepository;
    private $historyRepository;
    private $requestRepository;

    public function __construct(PaymentRepository $paymentRepository, HistoryRepository $historyRepository, RequestRepository $requestRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->historyRepository = $historyRepository;
        $this->requestRepository = $requestRepository;
    }

    public function index()
    {
        /** Check Permission */
        $this->authorize('manage', Payment::class);

        $dates = collect();
        foreach (range(-30, 0) as $i) {
            $dates->put(now()->addDays($i)->format('Y-m-d'), 0);
        }

        $summery = $this->paymentRepository->getDailySummery($dates);
        $requestsCount = $this->requestRepository->getRequestsCount();
        $persianToEnglishCount = isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_PERSIAN_TO_ENGLISH]) ? isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_PERSIAN_TO_ENGLISH]) : 0;
        $englishToPersianCount = isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_TO_PERSIAN]) ? isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_TO_PERSIAN]) : 0;
        $englishEditorCount = isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_EDITING]) ? isset($requestsCount[\RezaDadashi\Request\Models\Request::REQUEST_ENGLISH_EDITING]) : 0;

        $payments = $this->paymentRepository
            ->searchId(request('id'))
            ->searchName(request('name'))
            ->searchMobile(request('mobile'))
            ->paginateLatest();

        return view('Payment::index', compact('payments', 'dates', 'summery', 'persianToEnglishCount', 'englishToPersianCount', 'englishEditorCount'));
    }

    private function addHistory($requestId, $body)
    {
        $this->historyRepository->addHistory($requestId, $body);
    }

    public function callback(Request $request)
    {
        $gateway = resolve(Gateway::class);
        $paymentRepository = $this->paymentRepository;
        $payment = $paymentRepository->findByInvoiceId($gateway->getInvoiceIdFromRequest($request));

        if (!$payment) {
            newFeedback('notification_failed_payment', 'error');
            return redirect('/home');
        }

        $result = $gateway->verify($payment);

        if (is_array($result)) {
            newFeedback($result['message'], 'error');
            $paymentRepository->changeStatus($payment->id, Payment::STATUS_FAIL);

            /** Add Log History */
            $this->addHistory($payment->request_id, History::HISTORY_UNSUCCESSFUL_PAYMENT);

            return redirect()->to('/home');
        } else {
            event(new PaymentWasSuccessful($payment));
            newFeedback('notification_success_payment', 'success');
            $paymentRepository->changeStatus($payment->id, Payment::STATUS_SUCCESS);

            /** Add Log History */
            if ($payment->type == Payment::TYPE_DEPOSIT) {
                $this->addHistory($payment->request_id, History::HISTORY_SUCCESSFUL_DEPOSIT);
            }
            if ($payment->type == Payment::TYPE_SETTLEMENT) {
                $this->addHistory($payment->request_id, History::HISTORY_SUCCESSFUL_SETTLEMENT);
            }


            return redirect()->route('requests.show', $payment->request_id);
        }
    }
}
