<?php

namespace RezaDadashi\Payment\Gateways\Zarinpal;

use Illuminate\Http\Request;
use RezaDadashi\Payment\Contracts\GatewayContract;
use RezaDadashi\Payment\Models\Payment;
use RezaDadashi\Payment\Repositories\PaymentRepository;

class ZarinpalAdaptor implements GatewayContract
{
    private $url;
    private $client;
    //private $merchantId = "536c834b-7fbf-4c3b-a0f7-816998f856fb";
    private $merchantId = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
    private $sandBox = true;
    public function request($amount, $description)
    {
        $this->client = new Zarinpal();
        $callback = route('payments.callback');
        $result = $this->client->request($this->merchantId, $amount, $description, "", "", $callback, $this->sandBox);

        if (isset($result["Status"]) && $result["Status"] == 100)
        {
            $this->url = $result['StartPay'];
            return $result['Authority'];
        } else {
            return [
                "status" => $result["Status"],
                "message" => $result["Message"]
            ];
        }
    }

    public function verify(Payment $payment)
    {
        $this->client = new Zarinpal();
        $result = $this->client->verify($this->merchantId, $payment->amount, $this->sandBox);

        if (isset($result["Status"]) && $result["Status"] == 100)
        {
            return $result["RefID"];
        } else {
            return [
                "status" => $result["Status"],
                "message" => $result["Message"]
            ];
        }
    }

    public function redirect()
    {
        $this->client->redirect($this->url);
    }

    public function getName()
    {
        return 'zarinpal';
    }

    public function getInvoiceIdFromRequest(Request $request)
    {
        return $request->Authority;
    }
}
