<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth'],
    'prefix' => 'payments'
], function ($router) {

    $router->any('/callback', [\RezaDadashi\Payment\Http\Controllers\PaymentController::class, 'callback'])
        ->name('payments.callback');

    $router->get('/', [
        'uses' =>  'RezaDadashi\Payment\Http\Controllers\PaymentController@index',
        'as' => 'payments.index'
    ]);

});
