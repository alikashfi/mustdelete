<?php

namespace RezaDadashi\Payment\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Payment\Models\Payment;

class PaymentRepository extends Repository
{
    public function model()
    {
        return Payment::class;
    }

    public function store($data)
    {
        return Payment::query()->create([
            'user_id' => $data['user_id'],
            'discount_id' => $data['discount_id'],
            'request_id' => $data['request_id'],
            'media_id' => $data['media_id'],
            'type' => $data['type'],
            'amount' => $data['amount'],
            'invoice_id' => $data['invoice_id'],
            'gateway' => $data['gateway'],
            'status' => Payment::STATUS_PENDING,
        ]);
    }

    public function findByInvoiceId($invoiceId)
    {
        return Payment::query()
            ->where('invoice_id', $invoiceId)
            ->first();
    }

    public function changeStatus($id, $status)
    {
        return Payment::query()
            ->where('id', $id)
            ->update([
                'status' => $status
            ]);
    }

    private function getPaymentByStatus($status)
    {
        return Payment::query()
            ->where("status", $status);
    }

    public function getPaymentsByPeriod($startDate, $endDate)
    {
        return $this->getPaymentByStatus(Payment::STATUS_SUCCESS)
            ->whereDate("created_at", "<= ", $startDate)
            ->whereDate("created_at", ">=", $endDate)
            ->sum("amount");
    }

    public function getNDaysPayment($days = null)
    {
        $query = $this->getPaymentByStatus(Payment::STATUS_SUCCESS);
        if ($days) {
            $query = $query->whereDate('created_at', '>=', now()->addDays($days));
        }
        $query = $query->latest()
            ->sum('amount');
        return $query;
    }

    public function getUserRecentPayments($userId, $number = null)
    {
        $query = Payment::query()
            ->where('user_id', $userId)
            ->with('user');

        if ($number) {
            $query->limit($number);
        }

        return $query->paginate();
    }

    public function getDailySummery(Collection $dates)
    {
        return Payment::query()
            ->where('status', Payment::STATUS_SUCCESS)
            ->where('created_at', '>=', $dates->keys()->first())
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                DB::raw('DATE(created_at) as date'),
                DB::raw('SUM(amount) as totalAmount')
            ]);
    }

    public function searchId($id = null)
    {
        if ($id)
            $this->query->where('id', $id);
        return $this;
    }

    public function searchName($name = null)
    {
        if ($name)
            $this->query->whereHas('user', function ($query) use($name) {
                return $query->where('name', 'like', '%'. $name .'%');
            });
        return $this;
    }

    public function searchMobile($mobile = null)
    {
        if ($mobile)
            $this->query->whereHas('user', function ($query) use($mobile) {
                return $query->where('mobile', 'like', '%'. $mobile .'%');
            });
        return $this;
    }
}
