@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('payments.index') }}">@lang('payments_title')</a></li>
@endsection

@section('content')
	
    <div class="row">

        <div class="col-lg-12 mb-4">
            <div class="bg-white p-4">
                <div id="chart" class="w-100 overflow-auto"></div>
            </div>
        </div>

        <div class="col-md-12 col-lg-12">

            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('payments_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="dropdown bg-white border border-2 col-lg-4 my-2">
                            <div class="dropdown-toggle d-flex align-items-center justify-content-between p-2" data-bs-toggle="dropdown">@lang('payments_search')</div>
                            <div class="dropdown-menu col-4 w-100 mt-1 p-3 shadow">
                                <form method="GET">
                                    <input type="number" name="id" value="{{ request('id') }}" placeholder="@lang('common_id')" class="form-control mb-3">
                                    <input type="text" name="name" value="{{ request('name') }}" placeholder="@lang('common_name')" class="form-control mb-3">
                                    <input type="number" name="mobile" value="{{ request('mobile') }}" placeholder="@lang('common_mobile_number')" class="form-control mb-3">

                                    <div class="text-end">
                                        <button class="btn btn-primary" type="submit">
                                            <span>@lang('common_search')</span>
                                            <i class="bi bi-search text-white"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive pt-2">
                            <table class="table align-middle">
                                <thead>
                                    <tr>
                                        <th scope="col">@lang('common_id')</th>
                                        <th scope="col">@lang('common_name')</th>
                                        <th scope="col">@lang('common_mobile_number')</th>
                                        <th scope="col">@lang('common_email')</th>
                                        <th scope="col">@lang('payments_price')</th>
                                        <th scope="col">@lang('payments_type')</th>
                                        <th scope="col">@lang('payments_to_pay')</th>
                                        <th scope="col">@lang('payments_date')</th>
                                        <th scope="col">@lang('register_step_3_promo_code')</th>
                                        <th scope="col">@lang('payments_status')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr class="{{ $payment->getTableCss() }}">
                                        <th scope="row">{{ $payment->id }}</th>
                                        <td>
											@if($payment->user)
												<a class="text-decoration-none" href="{{ route('users.show', $payment->user->id) }}">{{ $payment->user->name }}</a>
											@endif
										</td>
                                        <td>
											@if($payment->user)
												<a class="text-decoration-none" href="tel:{{ $payment->user->mobile }}">{{ $payment->user->mobile }}</a>
											@endif
										</td>
                                        <td>
											@if($payment->user)
												<a class="text-decoration-none" href="mailto:{{ $payment->user->email }}">{{ $payment->user->email }}</a>
											@endif
										</td>
                                        <td>{{ number_format($payment->amount) }}</td>
                                        <td>@lang("payment_type_" . $payment->type)</td>
                                        <td>@if($payment->request)
                                          		@lang($payment->request->type)
                                          	@endif
                                        </td>
                                        <td>{{ getJalali($payment->created_at) }}</td>
                                        <td>
                                            @if($payment->discount)
                                                <span class="badge bg-primary rounded-pill">{{ $payment->discount->code }}</span>
                                                <span class="badge bg-danger rounded-pill">{{ $payment->discount->percent }} %</span>
                                            @endif
                                        </td>
                                        <td>@lang("payment_status_" . $payment->status)</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center d-flex justify-content-center"> {{ $payments->links('pagination::bootstrap-4') }} </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection

@include('Payment::chart')
