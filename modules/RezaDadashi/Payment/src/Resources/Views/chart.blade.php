@section('js')
    <script src="{{ asset('/assets/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('/assets/highcharts/modules/series-label.js') }}"></script>
    <script src="{{ asset('/assets/highcharts/modules/exporting.js') }}"></script>
    <script src="{{ asset('/assets/highcharts/modules/export-data.js') }}"></script>
    <script src="{{ asset('/assets/highcharts/modules/accessibility.js') }}"></script>

    <script>
        Highcharts.chart('chart', {
            title: {
                text: '@lang('chart_last_30_days_benefit')'
            },
            tooltip: {
                useHTML: true,
                style: {
                    fontSize: '16px',
                    fontFamily: 'tahoma',
                    direction: 'rtl',
                },
                formatter: function () {
                    return (this.x ? "تاریخ: " +  this.x + "<br>" : "")  + "مقدار: " + this.y
                }
            },
            xAxis: {
                categories: [@foreach($dates as $date => $value) '{{ getJalaliFromFormat($date) }}', @endforeach]
            },
            yAxis: {
                title: {
                    text: '@lang('chart_price')'
                },
                labels: {
                    formatter: function () {
                        return this.value + " @lang('Toman')"
                    }
                },
            },
            labels: {
                items: [{
                    html: '@lang('chart_requests_title')',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            series: [
                {
                    type: 'column',
                    name: '@lang('chart_success_payment')',
                    data: [@foreach($dates as $date => $value) @if($day = $summery->where("date", $date)->first()) {{ $day->totalAmount }}, @else 0, @endif @endforeach],
                },
                {
                    type: 'spline',
                    name: '@lang('chart_benefit')',
                    data: [@foreach($dates as $date => $value) @if($day = $summery->where("date", $date)->first()) {{ $day->totalAmount }}, @else 0, @endif @endforeach],
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                },
                {
                    type: 'pie',
                    name: 'تعداد',
                    title: {
                        text: 'sdf'
                    },
                    labels: {
                        formatter: function () {
                            return this.value + " تومان"
                        }
                    },
                    data: [{
                        name: '@lang('persian to english')',
                        y: {{ $persianToEnglishCount }},
                        color: Highcharts.getOptions().colors[0] // Jane's color
                    }, {
                        name: '@lang('english to persian')',
                        y: {{ $englishToPersianCount }},
                        color: Highcharts.getOptions().colors[1] // John's color
                    }, {
                        name: '@lang('english editing')',
                        y: {{ $englishEditorCount }},
                        color: Highcharts.getOptions().colors[2] // John's color
                    }],
                    center: [150, 80],
                    size: 100,
                    showInLegend: false,
                    dataLabels: {
                        enabled: true
                    }
                }
            ]
        });

    </script>
@endsection
