<?php

namespace RezaDadashi\Payment\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use RezaDadashi\Payment\Events\PaymentWasSuccessful;
use RezaDadashi\Request\Listeners\RequestUpdateStatus;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        PaymentWasSuccessful::class => [
            RequestUpdateStatus::class,
        ],
    ];

    public function boot()
    {
        parent::boot();
    }
}
