<?php

namespace RezaDadashi\Payment\Providers;

use Illuminate\Support\ServiceProvider;
use RezaDadashi\Payment\Gateways\Gateway;
use RezaDadashi\Payment\Gateways\Zarinpal\ZarinpalAdaptor;
use RezaDadashi\RolePermissions\Models\Permission;

class PaymentServiceProvider extends ServiceProvider
{
    public function register()
    {
        //Register Events
        $this->app->register(EventServiceProvider::class);

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadRoutesFrom(__DIR__ . '/../Routes/payment_routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Payment');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');
    }

    public function boot()
    {
        $this->app->singleton(Gateway::class,function ($app) {
            return  new ZarinpalAdaptor();
        });

        config()->set('sidebar.items.payments', [
            'icon' => 'bi bi-bar-chart-line-fill',
            'title' => ['fa' => 'تراکنش ها', 'en' => 'Transactions'],
            'url' => route('payments.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_PAYMENTS
            ]
        ]);
    }
}
