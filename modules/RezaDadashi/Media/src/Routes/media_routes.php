<?php
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth']
], function ($router) {
    $router->get('/media/{media}/download', [\RezaDadashi\Media\Http\Controllers\MediaController::class, 'download'])->name('media.download');
});
