<?php

use RezaDadashi\Media\Services\ImageFileService;
use RezaDadashi\Media\Services\DocFileService;

return [
     'MediaTypeServices' => [
         'image' => [
             'extensions' => [
                 'png', 'jpg', 'jpeg'
             ],
             'handler' => ImageFileService::class
         ],
         'doc' => [
             'extensions' => [
                 'doc', 'docx'
             ],
             'handler' => DocFileService::class
         ],
     ]
 ];
