<?php

namespace RezaDadashi\Media\Services;

use RezaDadashi\Media\Contracts\FileServiceContract;
use Illuminate\Support\Facades\Storage;
use RezaDadashi\Media\Models\Media;

class DocFileService extends DefaultFileService implements FileServiceContract
{
    public static function upload($file, $filename, $dir) : array
    {
        $filename = uniqid();
        $extension = $file->getClientOriginalExtension();
        $dir = 'private\\';
        Storage::putFileAs( $dir , $file, $filename . '.' . $extension);
        return ["doc" => $filename .  '.' . $extension];
    }

    public static function thumb(Media $media)
    {
        return url("/img/video-thumb.png");
    }

    public static function getFilename()
    {
        return (static::$media->is_private ? 'private/' : 'public/') . static::$media->files['doc'];
    }
}
