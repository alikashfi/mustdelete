<?php

namespace RezaDadashi\Media\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RezaDadashi\Media\Models\Media;
use RezaDadashi\Media\Services\MediaFileService;

class MediaController extends Controller
{
    public function download(Media $media, Request $request)
    {
        if (!$request->hasValidSignature()) {
            abort(401);
        }

        return MediaFileService::stream($media);
    }
}
