<?php

namespace RezaDadashi\Media\Models;

use RezaDadashi\Media\Services\MediaFileService;
use Illuminate\Database\Eloquent\Model;
use RezaDadashi\User\Models\User;

class Media extends Model
{
    protected $guarded = [];

    protected $casts = [
        'files' => 'json'
    ];

    protected static function booted()
    {
        static::deleting(function ($media) {
            MediaFileService::delete($media);
        });
    }

    public function getThumbAttribute()
    {
        return MediaFileService::thumb($this);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
