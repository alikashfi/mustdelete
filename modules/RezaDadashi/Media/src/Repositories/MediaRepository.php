<?php

namespace RezaDadashi\Media\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Media\Models\Media;

class MediaRepository extends Repository
{
    public function model()
    {
        return Media::class;
    }
}
