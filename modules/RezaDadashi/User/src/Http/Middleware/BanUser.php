<?php

namespace RezaDadashi\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BanUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // if user is ban show unauthorized page
        if (auth()->check() && !auth()->user()->isActive()) {
            return abort(403, trans('common_ban_account'));
        }
        return $next($request);
    }
}
