<?php

namespace RezaDadashi\User\Http\Requests;

use Illuminate\Support\Facades\App;
use RezaDadashi\User\Rules\ValidMobile;
use RezaDadashi\User\Services\VerifyCodeService;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordVerifyCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'verify_code' => VerifyCodeService::getRule(),
            'mobile' => ['required', new ValidMobile()]
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'verify_code' => 'کد',
            'mobile' => 'موبایل'
        ] : [];
    }
}
