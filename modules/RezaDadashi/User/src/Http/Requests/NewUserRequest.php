<?php

namespace RezaDadashi\User\Http\Requests;

use Illuminate\Support\Facades\App;
use RezaDadashi\User\Models\User;
use RezaDadashi\User\Rules\ValidMobile;
use RezaDadashi\User\Rules\ValidPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use RezaDadashi\User\Rules\ValidUsername;

class NewUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:190'],
            'mobile' => ['required', 'string', 'max:14', 'unique:users', new ValidMobile()],
            'password' => ['required', 'confirmed'],
            'role' => ['nullable']
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'name' => 'نام و نام خانوادگی',
            'mobile' => 'شماره موبايل',
            'password' => 'رمزعبور',
            'role' => 'سطح دسترسي',
        ] : [];
    }
}
