<?php

namespace RezaDadashi\User\Http\Requests;

use Illuminate\Support\Facades\App;
use RezaDadashi\User\Models\User;
use RezaDadashi\User\Rules\ValidPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:190',
            'email' => 'nullable|email|min:3|max:190|unique:users,email,' . request()->route('user'),
            'username' => 'nullable|min:3|max:190|unique:users,username,' . request()->route('user'),
            'translate_word_price' => 'nullable|integer|min:1',
            'editing_word_price' => 'nullable|integer|min:1',
            'mobile' => 'required|min:3|max:190|unique:users,mobile,' . request()->route('user'),
            'status' => ['required', Rule::in(User::$statuses)],
            'image' => 'nullable|mimes:jpg,jpeg,png',
            'password' => ['nullable', 'confirmed', new ValidPassword()],
            'role' => 'nullable'
        ];
    }

    public function attributes()
    {

        return App::getLocale() == 'fa' ? [
            'name' => 'نام و نام خانوادگی',
            'email' => 'ایمیل',
            'username' => 'نام کاربری',
            'translate_word_price' => 'قیمت هرکلمه ترجمه',
            'editing_word_price' => 'قیمت هرکلمه ویراستاری',
            'mobile' => 'موبایل',
            'status' => 'وضعیت حساب',
            'image' => 'تصویر پروفایل',
        ] : [];
    }
}
