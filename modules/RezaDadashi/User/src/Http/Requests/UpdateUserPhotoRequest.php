<?php

namespace RezaDadashi\User\Http\Requests;

use Illuminate\Support\Facades\App;
use RezaDadashi\User\Models\User;
use RezaDadashi\User\Rules\ValidPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserPhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userPhoto' => 'required|mimes:jpg,jpeg,png',
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'userPhoto' => 'تصویر پروفایل',
        ] : [];
    }
}
