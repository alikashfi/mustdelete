<?php

namespace RezaDadashi\User\Http\Requests;

use Illuminate\Support\Facades\App;
use RezaDadashi\User\Models\User;
use RezaDadashi\User\Rules\ValidMobile;
use RezaDadashi\User\Rules\ValidPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use RezaDadashi\User\Rules\ValidUsername;

class UpdateProfileInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:190',
            'email' => 'nullable|email|min:3|max:190|unique:users,email,' . auth()->id(),
            'username' => ['nullable', 'min:3', 'max:190', 'unique:users,username,' . auth()->id(), new ValidUsername()],
            'telegram' => ['nullable', 'string'],
            'password' => ['nullable', 'confirmed', new ValidPassword()],
            'mobile' => ['required', 'string', 'max:14', 'unique:users', new ValidMobile()]
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'name' => 'نام و نام خانوادگی',
            'email' => 'ایمیل',
            'username' => 'نام کاربری',
            'telegram' => 'تلگرام',
            'password' => 'رمزعبور',
        ] : [];
    }
}
