<?php

namespace RezaDadashi\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use RezaDadashi\Core\Responses\AjaxResponses;
use RezaDadashi\Media\Models\Media;
use RezaDadashi\Media\Services\MediaFileService;
use RezaDadashi\Request\Notifications\ExpertPaymentNotification;
use RezaDadashi\Request\Notifications\RequestSettlementCustomerNotification;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\RolePermissions\Repositories\RoleRepository;
use RezaDadashi\User\Http\Requests\NewUserRequest;
use RezaDadashi\User\Http\Requests\UpdateProfileInformationRequest;
use RezaDadashi\User\Http\Requests\UpdateUserPhotoRequest;
use RezaDadashi\User\Http\Requests\UpdateUserRequest;
use RezaDadashi\User\Models\User;
use RezaDadashi\User\Repositories\ExpertPaymentRepository;
use RezaDadashi\User\Repositories\UserRepository;

class UserController extends Controller
{
    private $userRepository;
    private $roleRepository;
    private $expertPaymentRepository;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository, ExpertPaymentRepository $expertPaymentRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->expertPaymentRepository = $expertPaymentRepository;
    }

    private function upload($fieldName)
    {
        $mediaId = 0;
        if (\request()->hasFile($fieldName)) {  //check the file present or not
            $image = \request()->file($fieldName); //get the file
            $name = md5(time() . time()).'.'.$image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/uploads/profile'); //public path folder dir
            $image->move($destinationPath, $name);  //mve to destination you mentioned

            $media = Media::create([
                'user_id' => auth()->id(),
                'files' => ['original' => $name],
                'type' => 'image',
                'filename' => $image->getClientOriginalName(),
                'is_private' => 0,
            ]);
            $mediaId = $media->id;
        }
        return $mediaId;
    }

    public function index()
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $users = $this->userRepository
            ->searchName(request('name'))
            ->searchMobile(request('mobile'))
            ->searchEmail(request('email'))
            ->searchStatus(request('status'))
            ->paginateLatest();
        return view('User::Admin.index', compact('users'));
    }

    public function newUserForm()
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $roles = $this->roleRepository->all();
        return view('User::Admin.create', compact('roles'));
    }

    public function newUser(NewUserRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $mobile = $request->mobile;
        if (str_starts_with($mobile, '09')) {
            $prefix = '09';
            $str = $mobile;
            if (substr($str, 0, strlen($prefix)) == $prefix) {
                $mobile = '989' . substr($str, strlen($prefix));
            }
        }

        $this->userRepository->create([
            'name' => $request->name,
            'mobile' => $mobile,
            'password' => bcrypt($request->password),
            'token' => generateToken()
        ])->assignRole($request->role)->markEmailAsVerified();

        newFeedback();

        return redirect()->route('users.index');
    }

    public function show($userId)
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $user = $this->userRepository->findOrFail($userId);
        return view('User::Admin.show', compact('user'));
    }

    public function edit($userId)
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $user = $this->userRepository->findOrFail($userId);
        $roles = $this->roleRepository->all();
        return view('User::Admin.edit', compact('user', 'roles'));
    }

    public function update(UpdateUserRequest $request, $userId)
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $user = $this->userRepository->findOrFail($userId);

        if ($request->hasFile('image')) {
            $request->request->add([
                'image_id' => $this->upload('image')
            ]);
            if ($user->image) {
                $user->image->delete();
            }
        } else {
            $request->request->add(['image_id' => $user->image_id]);
        }

        $update = [
            'name' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'username' => $request->username,
            'translate_word_price' => $request->translate_word_price,
            'editing_word_price' => $request->editing_word_price,
            'bio' => $request->bio,
            'status' => $request->status,
            'telegram' => $request->telegram,
            'image_id' => $request->image_id
        ];

        if (!is_null($request->password)) {
            $update['password'] = bcrypt($request->password);
        }



        $user->syncRoles([]);
        if ($request->role) {
            foreach ($request->role as $roleItem) {
                $user->assignRole($roleItem);
            }
        }


        $this->userRepository->update(['id' => $userId], $update);
        newFeedback();

        return redirect()->back();
    }

    public function destroy($userId)
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $user = $this->userRepository->findOrFail($userId);
        $this->userRepository->delete($user);

        return AjaxResponses::successResponse();
    }

    public function manualVerify($userId)
    {
        /** Check Permission */
        $this->authorize('manage', User::class);

        $user = $this->userRepository->findOrFail($userId);

        if (! $user->hasVerifiedEmail())
            $user->markEmailAsVerified();

        return AjaxResponses::successResponse();
    }

    public function updatePhoto(UpdateUserPhotoRequest $request)
    {
        $mediaId = $this->upload('userPhoto');

        auth()->user()->image_id = $mediaId;
        auth()->user()->save();

        newFeedback();
        return back();
    }

    public function profile()
    {
        return view('User::Admin.profile');
    }

    public function updateProfile(UpdateProfileInformationRequest $request)
    {
        $this->userRepository->updateProfile($request);
        newFeedback();
        return back();
    }

    public function partners()
    {
        /** Check Permission */
        $this->authorize('partners', User::class);

        $partners = $this->userRepository->getByPermission([
            Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH,
            Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN,
            Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR
        ]);

        return view('User::Admin.Partners.index', compact('partners'));
    }

    public function showPartnersPayment($userId)
    {
        /** Check Permission */
        $this->authorize('partners', User::class);

        $user = $this->userRepository->findOrFail($userId);
        $payments = $user->expertPayment()->latest()->get();

        $totalNotPaid = $this->expertPaymentRepository->getExpertPaymentNotPaidSum($userId);

        return view('User::Admin.Partners.payments', compact('user','payments', 'totalNotPaid'));
    }

    public function partnerPaymentsPaid($userId)
    {
        /** Check Permission */
        $this->authorize('partners', User::class);

        $user = $this->userRepository->findOrFail($userId);

        /** Send Notification To Customer */
        $totalNotPaidCount = $this->expertPaymentRepository->getExpertPaymentNotPaidCount($userId);
        $totalNotPaidAmount = $this->expertPaymentRepository->getExpertPaymentNotPaidSum($userId);
        Notification::send($user, (new ExpertPaymentNotification($totalNotPaidCount, $totalNotPaidAmount)));

        $this->expertPaymentRepository->paidExpertPayment($userId);

        newFeedback();
        return redirect()->route('partners.index');
    }

    public function settlement()
    {
        /** Check Permission */
        $this->authorize('mySettlement', auth()->user());

        $settlements = $this->expertPaymentRepository->getExpertPaymentPaginate(auth()->id());

        return view('User::Admin.Settlement.index', compact('settlements'));
    }
}
