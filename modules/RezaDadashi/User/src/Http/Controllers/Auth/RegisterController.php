<?php

namespace RezaDadashi\User\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use RezaDadashi\User\Rules\ValidMobile;
use RezaDadashi\User\Rules\ValidPassword;
use App\Providers\RouteServiceProvider;
use RezaDadashi\User\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/requests/new';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $attributes = [];

        // if language was Persian replace this attributes.
        if (App::getLocale() == 'fa') {
            $attributes = [
                'name' => 'نام',
                'email' => 'ایمیل',
                'mobile' => 'شماره موبایل',
                'password' => 'رمزعبور',
            ];
        }

        if (isset($data['mobile'])) {
            $data['mobile'] = str_replace('00', '', $data['mobile']);
        }

        if (isset($data['mobile']) && str_starts_with($data['mobile'], '09')) {
            $prefix = '09';
            $str = $data['mobile'];
            if (substr($str, 0, strlen($prefix)) == $prefix) {
                $data['mobile'] = '989' . substr($str, strlen($prefix));
            }
        }

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'string', 'integer', 'digits_between:11,14', 'unique:users', new ValidMobile()],
        ], [
            'mobile.unique' => App::getLocale() == 'fa' ?
                'شما قبلا ثبت نام کرده اید، لطفا از بخش ' . __('common_goto_login') . ' کاربری، وارد سامانه شوید.' : null
        ])->setAttributeNames($attributes);

        return $validation;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \RezaDadashi\User\Models\User
     */
    protected function create(array $data)
    {
        $mobile = $data['mobile'];
        if (str_starts_with($mobile, '09')) {
            $prefix = '09';
            $str = $data['mobile'];
            if (substr($str, 0, strlen($prefix)) == $prefix) {
                $mobile = '989' . substr($str, strlen($prefix));
            }
        }

        return User::create([
            'name' => $data['name'],
            'mobile' => $mobile,
            'token' => generateToken(),
        ]);
    }

    public function showRegistrationForm()
    {
        return view('User::Front.register');
    }
}
