<?php

namespace RezaDadashi\User\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        $trans = [];

        //login translation field
        if (App::getLocale() == 'fa') {
            $trans = [
                'email.required' => 'نام کاربری را وارد نمایید',
                'password.required' => 'رمزعبور را وارد نمایید',
            ];
        }

        $request->validate([
            recaptchaFieldName() => recaptchaRuleName(),
            $this->username() => 'required|string',
            'password' => 'required|string',
        ], $trans);
    }

    protected function credentials(Request $request)
    {
        // email or mobile
        $username = $request->get($this->username());

        // check the it field is email or mobile
        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';

        if (str_starts_with($username, '09')) {
            $prefix = '09';
            $str = $username;
            if (substr($str, 0, strlen($prefix)) == $prefix) {
                $username = '989' . substr($str, strlen($prefix));
            }
        }

        return [
            $field => $username,
            'password' => $request->password
        ];
    }

    public function showLoginForm()
    {
        return view('User::Front.login');
    }
}
