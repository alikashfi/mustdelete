<?php

namespace RezaDadashi\User\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use RezaDadashi\User\Http\Requests\ResetPasswordVerifyCodeRequest;
use RezaDadashi\User\Http\Requests\SendResetPasswordVerifyCodeRequest;
use RezaDadashi\User\Http\Requests\VerifyCodeRequest;
use RezaDadashi\User\Models\User;
use RezaDadashi\User\Repositories\UserRepository;
use RezaDadashi\User\Services\VerifyCodeService;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showVerifyCodeRequestForm()
    {
        return view('User::Front.passwords.email');
    }

    public function sendVerifyCode(SendResetPasswordVerifyCodeRequest $request)
    {
        $mobile = $request->mobile;
        if (str_starts_with($mobile, '09')) {
            $prefix = '09';
            $str = $mobile;
            if (substr($str, 0, strlen($prefix)) == $prefix) {
                $mobile = '989' . substr($str, strlen($prefix));
            }
        }

        $user = resolve(UserRepository::class)->findByMobile($mobile);

        if ($user && !VerifyCodeService::has($user->id)) {
            $user->sendResetPasswordRequestNotification();

            return view('User::Front.passwords.enter-verify-code-form');
        }

        // error message translated
        $transMessage = App::getLocale() == 'fa' ? 'کاربری با موبایل وارد شده یافت نشد' : 'Could not find user with imported mobile';
        return back()->withErrors([
            'mobile' => $transMessage
        ]);
    }

    public function checkVerifyCode(ResetPasswordVerifyCodeRequest $request)
    {
        $mobile = $request->mobile;
        if (str_starts_with($mobile, '09')) {
            $prefix = '09';
            $str = $mobile;
            if (substr($str, 0, strlen($prefix)) == $prefix) {
                $mobile = '989' . substr($str, strlen($prefix));
            }
        }

        $user = resolve(UserRepository::class)->findByMobile($mobile);

        // error message translated
        $transMessage = App::getLocale() == 'fa' ? 'کد وارد شده معتبر نمی باشد!' : 'The entered code is not valid!';

        if (! VerifyCodeService::check($user->id, $request->verify_code)) {
            return back()->withErrors([
                'verify_code' => $transMessage
            ]);
        }

        auth()->loginUsingId($user->id);

        return redirect()->route('password.showResetForm');
    }
}
