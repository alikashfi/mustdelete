<?php

namespace RezaDadashi\User\Services;

use RezaDadashi\User\Models\User;

class UserService
{
    public static function changePassword(User $user, $newPassword)
    {
        $user->password = bcrypt($newPassword);
        $user->save();
    }
}
