<?php

namespace RezaDadashi\User\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;

class ValidMobile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (str_starts_with($value, '09')) {
            if (strlen($value) > 11) {
                return false;
            }
            return preg_match('/^09\d{9}$/', $value);
        }
        if (preg_match('/^(?!00.*$)[0-9]/', $value)) {
            if (strlen($value) > 14) {
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $faMessage = 'فرمت موبایل نامعتبر است.';
        $enMessage = 'Invalid mobile format.';

        return App::getLocale() == 'fa' ? $faMessage : $enMessage;
    }
}
