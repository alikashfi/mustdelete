<?php

namespace RezaDadashi\User\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;

class ValidUsername implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('~^[a-z-A-Z_][a-z-A-Z_0-9\-_]{3,254}$~', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $faMessage = 'فرمت نام کاربری نامعتبر است.';
        $enMessage = 'Channel Name Must Contain Only Chars [a-z, A-Z, 0-9, _ or -] with min length of 4';

        return App::getLocale() == 'fa' ? $faMessage : $enMessage;
    }
}
