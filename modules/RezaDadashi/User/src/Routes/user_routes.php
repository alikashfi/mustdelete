<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth']
], function () {

    //upload photo profile
    Route::post('/users/photo', [\RezaDadashi\User\Http\Controllers\UserController::class, 'updatePhoto'])->name('users.photo');

    //profile
    Route::get('/profile', [
        'uses' => 'RezaDadashi\User\Http\Controllers\UserController@profile',
        'as' => 'users.profile'
    ]);

    //post profile
    Route::post('/profile', [
        'uses' => 'RezaDadashi\User\Http\Controllers\UserController@updateProfile',
        'as' => 'users.profile'
    ]);
});


Route::group([
    'middleware' => ['web', 'auth', 'verified']
], function ($router) {

    //settlement list
    Route::get('/settlement', [
        'uses' => 'RezaDadashi\User\Http\Controllers\UserController@settlement',
        'as' => 'settlement.index'
    ]);

    //partners list
    Route::get('/mySettlement', [
        'uses' => 'RezaDadashi\User\Http\Controllers\UserController@partners',
        'as' => 'partners.index'
    ]);

    //show expert payments
    Route::get('/partners/{user}/payments', [\RezaDadashi\User\Http\Controllers\UserController::class, 'showPartnersPayment'])
        ->name('partners.showPartnersPayment');

    //paid expert payment
    Route::get('/partners/{user}/paid', [\RezaDadashi\User\Http\Controllers\UserController::class, 'partnerPaymentsPaid'])
        ->name('partners.partnerPaymentsPaid');


    //manual verify account
    Route::patch('/users/{user}/manual-verify', [\RezaDadashi\User\Http\Controllers\UserController::class, 'manualVerify'])->name('manualVerify');


    //newUserForm
    Route::get('/new', [\RezaDadashi\User\Http\Controllers\UserController::class, 'newUserForm'])
        ->name('users.showFormNewUser');

    //newUser
    Route::post('/new', [\RezaDadashi\User\Http\Controllers\UserController::class, 'newUser'])
        ->name('users.newUser');

    //user resource
    Route::resource('/users', \RezaDadashi\User\Http\Controllers\UserController::class);
});


Route::group([
    'middleware' => 'web'
], function ($router) {
    Route::post('/mobile/verify', [\RezaDadashi\User\Http\Controllers\Auth\VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('/mobile/resend', [\RezaDadashi\User\Http\Controllers\Auth\VerificationController::class, 'resend'])->name('verification.resend');
    Route::get('/mobile/verify', [\RezaDadashi\User\Http\Controllers\Auth\VerificationController::class, 'show'])->name('verification.notice');

    //Login
    Route::get('/login', [\RezaDadashi\User\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');

    Route::post('/login', [\RezaDadashi\User\Http\Controllers\Auth\LoginController::class, 'login'])
        //->middleware('throttle:3,1')
        ->name('login');

    //Logout
    Route::post('/logout', [\RezaDadashi\User\Http\Controllers\Auth\LoginController::class, 'logout'])
        ->middleware('auth')
        ->name('logout');


    //Reset Password
    Route::get('/password/reset', [\RezaDadashi\User\Http\Controllers\Auth\ForgotPasswordController::class, 'showVerifyCodeRequestForm'])->name('password.request');
    Route::get('/password/reset/send', [\RezaDadashi\User\Http\Controllers\Auth\ForgotPasswordController::class, 'sendVerifyCode'])->name('password.sendVerifyCode');
    Route::post('/password/reset/check-verify-code', [\RezaDadashi\User\Http\Controllers\Auth\ForgotPasswordController::class, 'checkVerifyCode'])
        ->middleware('throttle:5,1')
        ->name('password.checkVerifyCode');
    //Throttle => perTime:forMinutes

    //Change Password
    Route::get('/password/change', [\RezaDadashi\User\Http\Controllers\Auth\ResetPasswordController::class, 'showResetForm'])
        ->middleware('auth')
        ->name('password.showResetForm');

    Route::post('/password/change', [\RezaDadashi\User\Http\Controllers\Auth\ResetPasswordController::class, 'reset'])->name('password.update');


    //Register
    Route::get('/register', [\RezaDadashi\User\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('/register', [\RezaDadashi\User\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register');

});
