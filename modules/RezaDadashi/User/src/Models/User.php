<?php

namespace RezaDadashi\User\Models;

use Illuminate\Support\Facades\URL;
use RezaDadashi\Comment\Models\Comment;
use RezaDadashi\Discount\Models\Discount;
use RezaDadashi\Media\Models\Media;
use RezaDadashi\Payment\Models\Payment;
use RezaDadashi\Quiz\Models\Question;
use RezaDadashi\Quiz\Models\Quiz;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\RolePermissions\Models\Role;
use RezaDadashi\User\Notifications\ResetPasswordRequestNotification;
use RezaDadashi\User\Notifications\VerifySMSNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    const STATUS_ACTIVE = 'active';
    const STATUS_BAN = 'ban';
    public static $statuses = [
        self::STATUS_ACTIVE,
        self::STATUS_BAN
    ];

    public static $defaultUsers = [
        [
            'mobile' => '989190495293',
            'email' => 'reza@gmail.com',
            'password' => '123',
            'name' => 'رضا داداشی',
            'bio' => 'برنامه نویس',
            'role' => [Role::ROLE_SUPER_ADMIN],
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'password',
        'token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendEmailVerificationNotification()
    {
        return true;
        //$this->notify(new VerifySMSNotification());
    }

    public function sendResetPasswordRequestNotification()
    {
        $this->notify(new ResetPasswordRequestNotification());
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function image()
    {
        return $this->belongsTo(Media::class, 'image_id');
    }

    public function getThumbAttribute()
    {
        if($this->image)
            return '/uploads/profile/' . $this->image->files['original'];

        return '/assets/img/profile.jpg';
    }

    public function requests()
    {
        return $this->hasMany(Request::class, 'user_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function isExpert()
    {
        return $this->hasAnyPermission([
            Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH,
            Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN,
            Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR
        ]);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id');
    }

    public function discounts()
    {
        return $this->morphToMany(Discount::class, 'discountable');
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }

    public function expertPayment()
    {
        return $this->hasMany(ExpertPayment::class);
    }
}
