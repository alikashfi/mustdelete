<?php

namespace RezaDadashi\User\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\User\Models\ExpertPayment;

class ExpertPaymentRepository extends Repository
{
    public function model()
    {
        return ExpertPayment::class;
    }

    private function getExpertPayment($userId)
    {
        return ExpertPayment::query()
            ->where('user_id', $userId)
            ->whereNull('paid_at');
    }

    public function getExpertPaymentPaginate($userId)
    {
        return ExpertPayment::query()
            ->where('user_id', $userId)
            ->orderByRaw('ISNULL(paid_at) desc')
            ->paginate();
    }

    public function getExpertPaymentNotPaidCount($userId)
    {
        return $this->getExpertPayment($userId)->count();
    }

    public function getExpertPaymentNotPaidSum($userId)
    {
        return $this->getExpertPayment($userId)->sum('amount');
    }

    public function paidExpertPayment($userId)
    {
        return ExpertPayment::query()
            ->where('user_id', $userId)
            ->update([
                'paid_at' => now()
            ]);
    }
}
