<?php

namespace RezaDadashi\User\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Repositories\RequestRepository;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\Setting\Models\Setting;
use RezaDadashi\Setting\Repositories\SettingRepository;
use RezaDadashi\User\Models\User;

class UserRepository extends Repository
{
    public function model()
    {
        return User::class;
    }

    public function findByMobile($mobile)
    {
        return User::query()->where('mobile', $mobile)->first();
    }

    public function findByToken($token)
    {
        return User::query()->where('token', $token)->firstOrFail();
    }

    public function getByPermission($permission)
    {
        return User::query()->permission($permission)->get();
    }

    public function getAllTranslators()
    {
        return User::query()->permission(
            Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH,
            Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN
            )
            ->get();
    }

    public function getAllEditors()
    {
        return User::query()->permission(Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR)->get();
    }

    public function getAllSupervisors()
    {
        return User::query()->permission(Permission::PERMISSION_SUPERVISOR)->get();
    }

    public function updateProfile($values)
    {
        auth()->user()->name = $values->name;
        auth()->user()->email = $values->email;

        if (auth()->user()->mobile != $values->mobile) {

            $mobile = $values->mobile;
            if (str_starts_with($mobile, '09')) {
                $prefix = '09';
                $str = $mobile;
                if (substr($str, 0, strlen($prefix)) == $prefix) {
                    $mobile = '989' . substr($str, strlen($prefix));
                }
            }

            auth()->user()->mobile = $mobile;
            auth()->user()->email_verified_at = null;
        }

        auth()->user()->bio = $values->bio;
        auth()->user()->username = $values->username;
        auth()->user()->telegram = $values->telegram;

        if ($values->password) {
            auth()->user()->password = bcrypt($values->password);
        }

        auth()->user()->save();
    }

    public function getUsersCount()
    {
        return User::query()->count();
    }

    public function getUsersCountByPermission($permission)
    {
        return $this->getByPermission($permission)->count();
    }

    public function searchName($name = null)
    {
        if ($name)
            $this->query->where('name', 'like', '%'. $name .'%');
        return $this;
    }

    public function searchMobile($mobile = null)
    {
        if ($mobile)
            $this->query->where('mobile', 'like', '%'. $mobile .'%');
        return $this;
    }

    public function searchEmail($email = null)
    {
        if ($email)
            $this->query->where('email', 'like', '%'. $email .'%');
        return $this;
    }

    public function searchStatus($status = null)
    {
        if ($status)
            $this->query->where('status', $status);
        return $this;
    }

    public function calculateWage($userId, $requestId)
    {
        $requestRepository = (new RequestRepository());
        $settingRepository = (new SettingRepository());

        $user = $this->findOrFail($userId);
        $translateUserWage = $user->translate_word_price;
        $editorUserWage = $user->editing_word_price;

        if (!$translateUserWage) {
            $translateUserWage = $settingRepository->getByKey(Setting::SETTING_KEY_TRANSLATE_WORD_PRICE_EXPERT)->value;
        }

        if (!$editorUserWage) {
            $editorUserWage = $settingRepository->getByKey(Setting::SETTING_KEY_EDITING_WORD_PRICE_EXPERT)->value;
        }

        $request = $requestRepository->findOrFail($requestId);

        if ($request->type == Request::REQUEST_PERSIAN_TO_ENGLISH || $request->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
            return $translateUserWage * $request->word_count['supervisor'];
        } else {
            return $editorUserWage * $request->word_count['supervisor'];
        }
    }
}
