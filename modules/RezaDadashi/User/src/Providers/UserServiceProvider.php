<?php

namespace RezaDadashi\User\Providers;

use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Database\Seeds\UsersTableSeeder;
use RezaDadashi\User\Http\Middleware\BanUser;
use RezaDadashi\User\Http\Middleware\StoreUserIp;
use RezaDadashi\User\Models\User;
use RezaDadashi\User\Policies\UserPolicy;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    public function register()
    {
        config()->set('auth.providers.users.model', User::class);

        $this->loadRoutesFrom(__DIR__ . '/../Routes/user_routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        // $this->loadFactoriesFrom(__DIR__ . '/../Database/Factories');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'User');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');

        Gate::policy(User::class, UserPolicy::class);

        DatabaseSeeder::$seeders[] = UsersTableSeeder::class;
    }

    public function boot()
    {
        /** Push Middleware */
        $this->app['router']->pushMiddlewareToGroup('web', StoreUserIp::class); // User Ip
        $this->app['router']->pushMiddlewareToGroup('web', BanUser::class); // Ban User

        config()->set('sidebar.items.users', [
            'icon' => 'bi bi-people-fill',
            'title' => ['fa' => 'کاربران', 'en' => 'Users'],
            'url' => route('users.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_USERS
            ]
        ]);

        config()->set('sidebar.items.partners', [
            'icon' => 'bi bi-person-lines-fill',
            'title' => ['fa' => 'همکاران', 'en' => 'Partners'],
            'url' => route('partners.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_SETTLEMENT_PARTNERS
            ]
        ]);

        config()->set('sidebar.items.my-settlement', [
            'icon' => 'bi bi-coin',
            'title' => ['fa' => 'لیست تسویه', 'en' => 'Settlement list'],
            'url' => route('settlement.index'),
            'permission' => [
                Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH,
                Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN,
                Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR
            ]
        ]);

        config()->set('sidebar.items.profile', [
            'icon'  => 'bi bi-person-fill',
            'title' => ['fa' => 'اطلاعات کاربری', 'en' => 'Profile Information'],
            'url'   => route('users.profile')
        ]);
    }
}
