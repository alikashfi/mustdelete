<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expert_payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('request_id');
            $table->integer('amount');
            $table->timestamp('paid_at')->nullable();
            $table->timestamps();

            // اگر کارشناس انجام د هنده از جدول کاربران حذف شد، این رکورد هم حذف می شود.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            // اگر درخواست از جدول درخواست ها حذف شد، این رکورد هم حذف می شود.
            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expert_payments');
    }
}
