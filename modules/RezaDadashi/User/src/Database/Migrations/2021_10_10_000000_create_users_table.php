<?php

use RezaDadashi\User\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('mobile', 14)->unique();
            $table->string('username', 50)->nullable();
            $table->integer('translate_word_price')->nullable();
            $table->integer('editing_word_price')->nullable();
            $table->string('bio')->nullable();
            $table->string('ip')->nullable();
            $table->string('telegram')->nullable();
            $table->unsignedBigInteger('image_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('token');
            $table->enum('status', User::$statuses)
                ->default(User::STATUS_ACTIVE);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('image_id')->references('id')->on('media')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
