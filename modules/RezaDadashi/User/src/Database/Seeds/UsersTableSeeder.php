<?php

namespace RezaDadashi\User\Database\Seeds;

use RezaDadashi\User\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::$defaultUsers as $user) {
            User::query()->firstOrCreate(
                ['email' => $user['email']],
                [
                    'mobile' => $user['mobile'],
                    'email' => $user['email'],
                    'name' => $user['name'],
                    'bio' => $user['bio'],
                    'password' => bcrypt($user['password']),
                    'token' => generateToken(),
                ]
            )->assignRole($user['role'])->markEmailAsVerified();
        }
    }
}
