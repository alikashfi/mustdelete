<!doctype html>
<html dir="{{ App::getLocale() == 'fa' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="{{ asset('/assets/img/favicon.ico') }}">

    <title>{{ __('app_name') }}</title>
    @if (App::getLocale() == 'fa')
        <link rel="stylesheet" href="{{ asset("/assets/css/bootstrap.rtl.min.css") }}">
    @else
        <link rel="stylesheet" href="{{ asset("/assets/css/bootstrap.min.css") }}">
    @endif
    <link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/animation.min.css') }}"/>

    <script src="{{ asset('/assets/js/jquery.3.6.0.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/js/js.js') }}"></script>
    <script src="{{ asset('/assets/js/toast.js') }}"></script>
</head>
<body>


<section class="accounts min-vh-100 d-flex align-items-center justify-content-center">

    @include('User::Front.layouts.nav')

    <div id="content" class="container">

        <div class="row d-flex justify-content-center">

            <div
                class="my-5 col-md-8 col-sm-12 bg-light py-5 px-4
                 d-flex -justify-content-center -align-items-center flex-column rounded shadow">

                <div class="text-start fs-6 mb-3 mt-sm-0">@lang('orders_top_step_title')</div>

                {{-- Step 1 --}}
                <div class="p-3 card w-100">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_1_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_1_description') }}</strong>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                               data-bs-target="#step1" aria-expanded="false"></i>
                        </div>
                        <div class="collapse show border-top border-danger mt-3" id="step1">

                            <form action="{{ route('register') }}" method="post">
                                @csrf
                                <div class="row pt-3">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="mb-3">
                                            <label for="mobile" class="form-label">
                                                <i class="bi bi-telephone px-1"></i>
                                                    {{ __('common_mobile_number') }}
                                                    <span class="required">*</span>
                                            </label>
                                            <input name="mobile" value="{{ old('mobile') }}" type="text"
                                                   class="form-control @error('mobile') is-invalid @enderror"
                                                   id="mobile" autocomplete="mobile" required>
{{--                                            <div--}}
{{--                                                class="form-text">{{ __('register_step_1_mobile_number_description') }}</div>--}}
                                            @error('mobile')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                <a class="text-decoration-none fs-13 btn btn-danger" href="{{ route('login') }}">@lang('common_goto_login')</a>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <div class="mb-3">
                                            <label for="name" class="form-label">
                                                <i class="bi bi-person px-1"></i>
                                                {{ __('common_name') }}
                                                <span class="required">*</span>
                                            </label>
                                            <input name="name" value="{{ old('name') }}" type="text"
                                                   class="form-control @error('name') is-invalid @enderror"
                                                   autocomplete="name" id="name" required>
                                            @error('name')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>



                                <div class="text-end">
                                    <button type="submit" class="btn btn-primary">{{ __('common_ban_continue') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- Step 2 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_2_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_2_description') }}</strong>
{{--                            </div>--}}
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step2" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step2" style="">
                        </div>
                    </div>
                </div>


            </div>


                {{-- Step 3 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_3_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_3_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step3" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step3">
                        </div>
                    </div>
                </div>

                {{-- Step 4 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_4_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_4_description') }}</strong>
                            </div>
                            {{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
                            {{--                               data-bs-target="#step4" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step4">
                        </div>
                    </div>
                </div>
            </div>

    </div>
</section>


</body>
</html>
