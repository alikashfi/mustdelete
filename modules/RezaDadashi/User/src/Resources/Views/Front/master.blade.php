<!doctype html>
<html dir="{{ App::getLocale() == 'fa' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="{{ asset('/assets/img/favicon.ico') }}">

    <title>{{ __('app_name') }}</title>


    @if (App::getLocale() == 'fa')
        <link rel="stylesheet" href="{{ asset("/assets/css/bootstrap.rtl.min.css") }}">
    @else
        <link rel="stylesheet" href="{{ asset("/assets/css/bootstrap.min.css") }}">
    @endif
    <link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/animation.min.css') }}"/>

    <script src="{{ asset('/assets/js/jquery.3.6.0.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/js/js.js') }}"></script>
    <script src="{{ asset('/assets/js/toast.js') }}"></script>

    {!! htmlScriptTagJsApi(['lang' => \Illuminate\Support\Facades\App::getLocale()]) !!}

</head>
<body data-dir="">


@include('Core::layouts.feedbacks')

<section class="accounts min-vh-100 d-flex align-items-center justify-content-center">

    @include('User::Front.layouts.nav')
	

    <div id="content" class="container">

        <div class="row d-flex justify-content-center">

            @yield('content')

        </div>

    </div>
</section>


@yield('js')
</body>
</html>
