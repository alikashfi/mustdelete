@extends('User::Front.master')

@section('content')

    <div
        class="col-md-4 col-sm-12 bg-light py-5 d-flex justify-content-center align-items-center flex-column order-lg-0 order-2">
        <form action="{{ route('login') }}" method="POST" class="w-100">
            @csrf
            <div class="w-100 my-2">
                <label for="username" class="form-label">{{ __('common_username') }}</label>
                <input name="email" value="{{ old('email') }}" type="text"
                       class="form-control @error('email') is-invalid @enderror" id="username" autofocus>
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-100 my-3">
                <label for="password" class="form-label">{{ __('common_password') }}</label>
                <input name="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       id="password" autocomplete="current-password">
                @error('password')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-100">
                <label for="name" class="form-label"><i
                        class="bi bi-robot px-1"></i>@lang('common_captcha')
                </label>
                {!! htmlFormSnippet() !!}
                <input class="@error('g-recaptcha-response') is-invalid @enderror" type="hidden" name="">
                @error('g-recaptcha-response')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-100 my-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>

            <a class="text-decoration-none" href="{{ route('password.request') }}">{{ __('login_forgot_password') }}</a>

            <div class="w-100 text-end my-2">
                <button type="submit" class="btn btn-dark">{{ __('login_btn_login') }}</button>
            </div>
        </form>
    </div>

    <div id="des"
         class="col-md-4 col-sm-12 text-center text-white d-flex justify-content-center align-items-center flex-column py-5 order-0">

        <img class="img-fluid my-4" width="90" src="{{ asset('/assets/img/logo.png') }}" alt="">
        <div class="fs-4 d-flex my-3">{{ __('app_name') }}</div>
        <div class="fs-5 my-2">{{ __('common_welcome') }}</div>
        <a href="{{ route('register') }}"
           class="text-white text-decoration-none my-3">{{ __('common_dont_have_account') }}</a>
    </div>

@endsection
