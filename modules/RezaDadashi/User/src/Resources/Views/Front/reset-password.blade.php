@extends('User::Front.master')

@section('content')


    <div class="col-md-4 col-sm-12 bg-light py-5 d-flex justify-content-center align-items-center flex-column order-lg-0 order-2">
        <form action="" method="POST" class="w-100">
            <div class="w-100 my-2">
                <label for="mobile" class="form-label">شماره موبایل</label>
                <input type="text" class="form-control" id="mobile" placeholder="" autofocus>
            </div>

            <div class="w-100 text-end my-2">
                <button type="submit" class="btn btn-outline-dark">ارسال کد</button>
            </div>
        </form>
    </div>

    <div id="des" class="col-md-4 col-sm-12 text-center text-white d-flex justify-content-center align-items-center flex-column py-5 order-0">

        <img class="img-fluid my-4" width="90" src="{{ asset('/assets/img/logo.png') }}" alt="">
        <div class="fs-4 d-flex my-3">{{ __('app_name') }}</div>
        <div class="fs-5 my-2">{{ __('account_welcome') }}</div>
        <a href="#" class="text-white text-decoration-none my-3">{{ __('account_done_have_account') }}</a>
    </div>



@endsection
