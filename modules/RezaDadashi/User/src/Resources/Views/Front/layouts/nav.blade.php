<nav class="navbar navbar-expand-md navbar-light fixed-top bg-white" style="border: solid 1px #8fb1b9">
    <div class="container-fluid">
        <a class="navbar-brand me-0 me-md-3" style="font-size:15px;" href="/">
            <img width="50" src="{{ asset('/assets/img/logo.png') }}" alt="">
            <span class="d-none d-sm-none d-md-inline-block ms-1">{{ __('app_name') }}</span>
        </a>
         <span class="d-md-none header-text fw-bold fst-italic">
             {{ __('app_name') }}
            <div class="d-sm-none d-block text-center w-100 fw-normal fst-normal mt-1">
                <a class="text-center text-decoration-none text-danger" href="{{ route('login') }}">{{ __('common_goto_login') }}</a>
            </div>
         </span>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav me-auto mb-2 mb-md-0">
                @if(! auth()->check())
                    <li class="nav-item">
                        <a class="nav-link {{ request()->segment(1) == 'login' ? 'active' : null }}" href="{{ route('login') }}">{{ __('common_goto_login') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->segment(1) == 'register' ? 'active' : null }}" href="{{ route('register') }}">{{ __('common_goto_register') }}</a>
                    </li>
                @endif
            </ul>
            <div class="d-flex align-items-center justify-content-start">
                <ul class="navbar-nav me-auto mb-2 mb-md-0 d-flex ">
                    <li class="nav-item">
                        <a class="nav-link p-0 me-3 pb-3 pb-sm-3 pb-xl-0 pb-lg-0 {{ request()->segment(1) == 'quizzes' ? 'active' : null }}" href="{{ route('quizzes.showQuizForm') }}">@lang('cooperation_request')</a>
                    </li>
                    @if (\Illuminate\Support\Facades\App::getLocale() == 'fa')
                        <li class="nav-item">
                            <a href="{{ route('changeLang') . '?lang=en' }}" class="text-decoration-none">
                                <img class="rounded-circle mx-1" width="20" src="{{ asset('/assets/img/lang/en.jpg') }}"
                                     alt="{{ config()->get("translationLang.languages.en") }}">
                                <span class="text-white">{{ config()->get("translationLang.languages.en") }}</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="{{ route('changeLang') . '?lang=fa' }}" class="text-decoration-none">
                                <img class="rounded-circle mx-1" width="20" src="{{ asset('/assets/img/lang/fa.jpg') }}"
                                     alt="{{ config()->get("translationLang.languages.fa") }}">
                                <span class="text-white">{{ config()->get("translationLang.languages.fa") }}</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</nav>

<style>
    @media screen and (max-width: 311px) {
        .navbar .container-fluid .header-text {
            flex-basis: 50%;
        }
    }
</style>