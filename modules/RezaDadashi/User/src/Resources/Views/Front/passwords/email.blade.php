@extends('User::Front.master')


@section('content')


    <div
        class="col-md-4 col-sm-12 bg-light py-5 d-flex justify-content-center align-items-center flex-column  order-lg-0 order-2">
        <form action="{{ route('password.sendVerifyCode') }}" method="get" class="w-100">

            <div class="w-100 my-2">

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <label for="mobile" class="form-label">{{ __('common_mobile_number') }}</label>
                <input name="mobile" value="{{ old('mobile') }}" type="text"
                       class="form-control @error('mobile') is-invalid @enderror" id="email" autocomplete="mobile"
                       autofocus>
                @error('mobile')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-100">
                <label for="name" class="form-label"><i
                        class="bi bi-robot px-1"></i>@lang('common_captcha')
                </label>
                {!! htmlFormSnippet() !!}
                <input class="@error('g-recaptcha-response') is-invalid @enderror" type="hidden" name="">
                @error('g-recaptcha-response')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-100 text-end my-2">
                <button type="submit" class="btn btn-outline-dark">{{ __('forget_password_btn_recovery') }}</button>
            </div>
        </form>
    </div>

    <div id="des"
         class="col-md-4 col-sm-12 text-center text-white d-flex justify-content-center align-items-center flex-column py-5 order-0">

        <img class="img-fluid my-4" width="80" src="{{ asset('/assets/img/logo.png') }}" alt="">
        <div class="fs-4 d-flex my-3">{{ __('app_name') }}</div>
        <div class="fs-5 my-2">{{ __('forget_password_title') }}</div>
    </div>



@endsection
