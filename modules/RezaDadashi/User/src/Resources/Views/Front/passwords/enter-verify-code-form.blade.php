@extends('User::Front.master')

@section('content')


    <div
        class="col-md-4 col-sm-12 bg-light py-5 d-flex justify-content-center align-items-center flex-column  order-lg-0 order-2">
        <form class="w-100" method="POST" action="{{ route('password.checkVerifyCode') }}">
            @csrf

            @if(request()->mobile)
                <div class="text-muted">
                    {{ __('recovery_password_verify_code_notice', ['mobile' => request()->mobile]) }}
                </div>
            @endif

            <input type="hidden" name="mobile" value="{{ request()->mobile }}">

            <div class="w-100 my-3">
                <input name="verify_code" type="text" class="form-control @error('verify_code') is-invalid @enderror"
                       id="verify_code">
                @error('verify_code')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-100 text-end my-2">
                <button type="submit" class="btn btn-outline-dark">{{ __('recovery_password_verify_btn') }}</button>
            </div>

        </form>

        <form id="resend-code" action="{{ route('verification.resend') }}" method="post">@csrf</form>
    </div>

    <div id="des"
         class="col-md-4 col-sm-12 text-center text-white d-flex justify-content-center align-items-center flex-column py-5 order-0">

        <img class="img-fluid my-4" width="80" src="{{ asset('/assets/img/logo.png') }}" alt="">
        <div class="fs-4 d-flex my-3">{{ __('app_name') }}</div>
        <div class="fs-5 my-2">{{ __('forget_password_title') }}</div>
        <a href="{{ route('login') }}" class="text-white text-decoration-none my-3">{{ __('common_goto_login') }}</a>
    </div>



@endsection
