@extends('User::Front.master')


@section('content')


    <div
        class="col-md-4 col-sm-12 bg-light py-5 d-flex justify-content-center align-items-center flex-column  order-lg-0 order-2">
        <form action="{{ route('password.update') }}" method="POST" class="w-100">
            @csrf

            <div class="w-100 my-2">
                <label for="password" class="form-label"><i class="bi bi-key px-1"></i>{{ __('common_password') }}</label>
                <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" id="password" autocomplete="new-password">
                @error('password')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-100 my-3">
                <div class="mb-3">
                    <label for="password-confirm" class="form-label"><i class="bi bi-key px-1"></i>{{ __('common_password_confirmation') }}</label>
                    <input name="password_confirmation" type="password" class="form-control"
                           id="password-confirm" autocomplete="new-password">
                </div>
            </div>

            <div class="w-100 text-end my-2">
                <button type="submit" class="btn btn-outline-dark">{{ __('change_password_new_password_btn') }}</button>
            </div>
        </form>
    </div>

    <div id="des"
         class="col-md-4 col-sm-12 text-center text-white d-flex justify-content-center align-items-center flex-column py-5 order-0">

        <img class="img-fluid my-4" width="80" src="{{ asset('/assets/img/logo.png') }}" alt="">
        <div class="fs-4 d-flex my-3">{{ __('app_name') }}</div>
        <div class="fs-5 my-2">{{ __('change_password_title') }}</div>
        <a href="{{ route('login') }}" class="text-white text-decoration-none my-3">{{ __('common_goto_login') }}</a>
    </div>



@endsection
