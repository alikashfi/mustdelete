@extends('User::Front.master')

@section('content')



    <div
        class="col-md-4 col-sm-12 bg-light py-5 d-flex justify-content-center align-items-center flex-column  order-lg-0 order-2">
        <form class="w-100" method="POST" action="{{ route('verification.verify') }}">
            @csrf

            <div class="w-100 my-3">
                <label for="verify_code" class="form-label">
                    @lang('common_code', ['mobile' => auth()->user()->mobile])
                    <span class="required">*</span>
                </label>

                <a href="{{ route('users.profile') }}" class="form-label d-block text-decoration-none">
                    @lang('change_mobile_number_to_verify')
                </a>

                <a href="javascript:;"
                   onclick="document.getElementById('logout-form').submit();"
                   class="form-label d-block text-decoration-none">
                    @lang('logout_account_in_verify_area')
                </a>


                <input name="verify_code" type="text" class="form-control @error('verify_code') is-invalid @enderror"
                       id="verify_code" required>
                @error('verify_code')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-100 text-end my-2">
                <button type="submit" class="btn btn-outline-dark">@lang('common_verify')</button>
            </div>

            <a class="text-decoration-none" href="#"
               onclick="event.preventDefault(); document.getElementById('resend-code').submit();"
            >@lang('common_resend_code')</a>

        </form>

        <form id="resend-code" action="{{ route('verification.resend') }}" method="post">@csrf</form>

        <form id="logout-form" action="{{ route('logout') }}" method="post">@csrf</form>
    </div>

    <div id="des"
         class="col-md-4 col-sm-12 text-center text-white d-flex justify-content-center align-items-center flex-column py-5 order-0">

        <img class="img-fluid my-4" width="90" src="{{ asset('/assets/img/logo.png') }}" alt="">
        <div class="fs-4 d-flex my-3">{{ __('app_name') }}</div>
        <div class="fs-5 my-2">@lang('common_verify_account')</div>
        <a href="{{ route('login') }}" class="text-white text-decoration-none my-3">@lang('common_goto_login')</a>
    </div>



@endsection
