@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('users.profile') }}">@lang('edit_profile_information')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-lg-3 d-flex flex-column align-items-center">
            <form action="{{ route('users.photo') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div id="profile-info" class="border rounded-circle cursor-pointer text-center" style="width: 200px; height: 200px;">
                    <div id="avatar-img" class="w-100 h-100 border border-4 position-relative overflow-hidden rounded-circle">
                        <img src="{{ auth()->user()->thumb }}">
                        <input name="userPhoto" type="file" accept="image/*" onchange="this.form.submit()">
                        <div id="box-camera"></div>
                    </div>
                </div>
            </form>
            <div class="w-100 mt-2">
                <div class="text-center my-2">{{ auth()->user()->bio }}</div>

                @if(auth()->user()->mobile)
                    <a href="tel:{{ auth()->user()->mobile }}" class="my-2 text-decoration-none bg-white d-flex justify-content-start align-items-center border border-start px-2">
                        <i class="bi bi-telephone-fill fs-4 text-primary mt-2"></i>
                        <span class="px-2">@lang('common_mobile_number')</span>
                    </a>
                @endif

                @if(auth()->user()->email)
                    <a href="mailto:{{ auth()->user()->email }}" class="my-2 text-decoration-none bg-white d-flex justify-content-start align-items-center border border-start px-2">
                        <i class="bi bi-envelope fs-4 text-primary mt-2"></i>
                        <span class="px-2">@lang('common_email')</span>
                    </a>
                @endif
            </div>
        </div>

        <div class="col-lg-9">
            <div class="card p-3">

                <form action="{{ route('users.profile') }}" method="post">
                    @csrf

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="title" class="form-label">
                                    @lang('common_name')
                                    <span class="required">*</span>
                                </label>
                                <x-input name="name" value="{{ auth()->user()->name }}" type="text" id="title" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="email" class="form-label">
                                    @lang('common_email')
                                </label>
                                <x-input name="email" value="{{ auth()->user()->email }}" type="email" id="email" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="mobile" class="form-label">
                                    @lang('common_mobile_number')
                                    <span class="required">*</span>
                                </label>
                                @if(auth()->user()->hasVerifiedEmail())
                                    <x-input name="mobile" value="{{ auth()->user()->mobile }}" disabled type="number" id="mobile" />
                                @else
                                    <x-input name="mobile" value="{{ auth()->user()->mobile }}" type="number" id="mobile" />
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="username" class="form-label">
                                    @lang('common_just_username')
                                </label>
                                <x-input name="username" value="{{ auth()->user()->username }}" class="text-end" type="text" id="username" />
                                <div
                                    class="form-text">{{ __('common_username_rule') }}</div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="password" class="form-label">@lang('common_password')</label>
                                <x-input name="password" value="" type="password" id="password" />
                                <div
                                    class="form-text">@lang('common_password_description')</div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="password-confirm" class="form-label">@lang('common_password_confirmation')</label>
                                <x-input name="password_confirmation" value="" type="password" id="password-confirm" />
                            </div>
                        </div>
                    </div>


                    <div class="text-end">
                        <button type="submit" class="btn btn-primary mt-2">@lang('common_update')</button>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
