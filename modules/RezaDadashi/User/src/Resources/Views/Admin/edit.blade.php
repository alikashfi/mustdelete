@extends('Dashboard::master')

@section('css')
<link rel="stylesheet" href="{{ asset('/assets/css/select2.min.css') }}">
@endsection

@section('js')
<script src="{{ asset('/assets/js/select2.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
</script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">@lang('users_title')</a></li>
    <li class="breadcrumb-item"><a href="#">@lang('users_edit')</a></li>
@endsection

@section('content')
    <div class="p-3 card">
        <div class="card border-primary">
            <div class="card-header bg-primary text-white">
                @lang('users_edit') ({{ $user->name }})
            </div>
            <div class="card-body">
                <form action="{{ route('users.update', $user->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="title" class="form-label">@lang('common_name')</label>
                                <x-input name="name" value="{{ $user->name }}" type="text" id="title" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="email" class="form-label">@lang('common_email')</label>
                                <x-input name="email" value="{{ $user->email }}" type="email" id="email" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="mobile" class="form-label">@lang('common_mobile_number')</label>
                                <x-input name="mobile" value="{{ $user->mobile }}" type="number" id="mobile" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="username" class="form-label">@lang('common_just_username')</label>
                                <x-input name="username" value="{{ $user->username }}" class="text-end" type="text" id="username" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="translate_word_price" class="form-label">@lang('translate_word_price')</label>
                                <x-input name="translate_word_price" value="{{ $user->translate_word_price }}" class="text-end" type="number" id="translate_word_price" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="editing_word_price" class="form-label">@lang('editing_word_price')</label>
                                <x-input name="editing_word_price" value="{{ $user->editing_word_price }}" class="text-end" type="number" id="editing_word_price" />
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="my-2">
                                <label for="role" class="form-label">@lang('common_user_role')</label>
                                <x-select name="role[]" id="role" class="js-example-basic-multiple" multiple="multiple">
                                    <option value="">-- @lang('common_user_role') --</option>
                                    @foreach($roles as $role)
                                        <option {{ $user->hasRole($role->name) ? 'selected' : null }} value="{{ $role->name }}">@lang($role->name)</option>
                                    @endforeach
                                </x-select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="status" class="form-label">@lang('users_account_status')</label>
                                <x-select name="status" id="status">
                                    @foreach(\RezaDadashi\User\Models\User::$statuses as $status)
                                        <option @if ($user->status == $status) selected @endif value="{{ $status }}">@lang($status)</option>
                                    @endforeach
                                </x-select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="image" class="form-label">@lang('users_avatar')</label>
                                <x-input name="image" value="" type="file" id="image" accept=".jpg,.jpeg,.png" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="password" class="form-label">@lang('common_password')</label>
                                <x-input name="password" value="" type="password" id="password" />
                                <div
                                    class="form-text">@lang('common_password_description')</div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="my-2">
                                <label for="password-confirm" class="form-label">@lang('common_password_confirmation')</label>
                                <x-input name="password_confirmation" value="" type="password" id="password-confirm" />
                            </div>
                        </div>
                    </div>


                    <div class="text-end">
                        <button type="submit" class="btn btn-outline-primary mt-2">@lang('common_update')</button>
                        <a href="{{ route('users.index') }}" class="btn btn-outline-danger mt-2">@lang('common_cancel')</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
