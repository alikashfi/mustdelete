@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('partners.index') }}">@lang('partners_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('partners_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="row">
                            <div class="col-md-4 p-1">
                                <div class="bg-white border p-2">
                                    @lang('partners_total_not_paid'): {{ number_format($totalNotPaid) }} @lang('Toman')
                                    @if($totalNotPaid)
                                        <a class="btn btn-success ms-4" href="{{ route('partners.partnerPaymentsPaid', $user->id) }}">@lang('partners_paid')</a>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead>
                                <tr>
                                    <th scope="col">@lang('common_id')</th>
                                    <th scope="col">@lang('common_name')</th>
                                    <th scope="col">@lang('partners_request_id')</th>
                                    <th>@lang('user_settlement_word_count')</th>
                                    <th scope="col">@lang('partners_amount')</th>
                                    <th scope="col">@lang('partners_date')</th>
                                    <th scope="col">@lang('partners_status')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <th scope="row">{{ $payment->id }}</th>
                                        <td><a class="text-decoration-none" href="{{ route('users.show', $payment->id) }}">{{ $payment->user->name }}</a></td>
                                        <td><a href="{{ route('requests.show', $payment->request_id) }}">{{ $payment->request_id }}</a></td>
                                        <td>{{ number_format($payment->request->word_count['supervisor']) }}</td>
                                        <td>{{ number_format($payment->amount) }} @lang('Toman')</td>
                                        <td>{{ getJalali($payment->created_at) }}</td>
                                        <td>
                                            @if($payment->paid_at)
                                                <span class="badge bg-success">@lang('partners_expert_payment_status_paid')</span>
                                                <br><span class="badge text-muted bg-light border">{{ getJalali($payment->paid_at) }}</span>
                                            @else
                                                <span class="badge bg-danger">@lang('partners_expert_payment_status_waiting')</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
