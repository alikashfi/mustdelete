@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('partners.index') }}">@lang('partners_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('partners_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead>
                                <tr>
                                    <th scope="col">@lang('common_id')</th>
                                    <th scope="col">@lang('common_name')</th>
                                    <th scope="col">@lang('users_avatar')</th>
                                    <th scope="col">@lang('common_balance')</th>
                                    <th scope="col">@lang('users_role')</th>
                                    <th scope="col">@lang('common_actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($partners as $partner)
                                    <tr>
                                        <th scope="row">{{ $partner->id }}</th>
                                        <td><a class="text-decoration-none" href="{{ route('users.show', $partner->id) }}">{{ $partner->name }}</a></td>
                                        <td>
                                            <a href="{{ route('users.show', $partner->id) }}">
                                                <img width="40" height="40" class="rounded-circle border border-3"
                                                     src="{{ $partner->thumb }}" alt="">
                                            </a>
                                        </td>
                                        <td>{{ number_format((new \RezaDadashi\User\Repositories\ExpertPaymentRepository())->getExpertPaymentNotPaidSum($partner->id)) }} @lang('Toman')</td>
                                        <td>
                                            @if(count($partner->roles) > 0)
                                                <ul class="list-unstyled">
                                                    @foreach($partner->roles as $userRole)
                                                        <li>
                                                            <span class="badge bg-danger">@lang($userRole->name)</span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @else
                                                <span class="badge bg-success">کاربر عادی</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="text-decoration-none" href="{{ route('partners.showPartnersPayment', $partner->id) }}">@lang('partners_view_link')</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
