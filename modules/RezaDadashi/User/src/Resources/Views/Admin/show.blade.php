@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">@lang('users_title')</a></li>
    <li class="breadcrumb-item"><a href="{{ route('users.profile') }}">@lang('user_profile')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-lg-12 col-lg-8">

            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('user_profile') {{ $user->name }}</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="border-top collapse show" id="groups-toggle">

                        <div class="row">
                            <div class="col-lg-4 d-flex justify-content-center align-items-center mt-3">
                                <img width="200" height="200" src="{{ $user->thumb }}" alt="{{ $user->name }}" class="rounded-circle shadow">
                            </div>

                            <div class="col-lg-4 d-flex align-items-centers justify-content-center flex-column mt-3 mt-lg-0">
                                <div class="my-2">
                                    <i class="bi bi-person-bounding-box fs-4"></i>
                                    <span class="px-2">{{ $user->name }}</span>
                                </div>
                                <div class="my-2">
                                    <i class="bi bi-person-check-fill fs-4"></i>
                                    <span class="px-2">
                                        @lang('users_account_status'):
                                        @if($user->hasVerifiedEmail())
                                            <i class="bi bi-patch-check-fill fs-5 text-primary"></i>
                                        @else
                                            <span class="text-danger">-</span>
                                        @endif
                                    </span>
                                </div>
                                <div class="my-2">
                                    <i class="bi bi-person-lines-fill fs-4"></i>
                                    <span class="px-2">{{ $user->bio }}</span>
                                </div>
                            </div>

                            <div class="col-lg-4 d-flex align-items-centers justify-content-center flex-column mt-3 mt-lg-0">

                                @if($user->mobile)
                                    <div class="my-2">
                                        <a href="https://wa.me/{{ $user->mobile }}" class="text-decoration-none bg-white d-flex justify-content-start align-items-center border border-start px-2">
                                            <i class="bi bi-whatsapp fs-4 text-success mt-2"></i>
                                            <span class="px-2">@lang('common_whatsapp')</span>
                                        </a>
                                    </div>
                                @endif

                                @if($user->telegram)
                                    <div class="my-2">
                                        <a href="https://t.me/{{ $user->telegram }}" class="text-decoration-none bg-white d-flex justify-content-start align-items-center border border-start px-2">
                                            <i class="bi bi-telegram fs-4 text-primary mt-2"></i>
                                            <span class="px-2">@lang('common_telegram')</span>
                                        </a>
                                    </div>
                                @endif

                                @if($user->mobile)
                                    <div class="my-2">
                                        <a href="tel:{{ $user->mobile }}" class="text-decoration-none bg-white d-flex justify-content-start align-items-center border border-start px-2">
                                            <i class="bi bi-telephone-fill fs-4 text-primary mt-2"></i>
                                            <span class="px-2">@lang('common_mobile_number')</span>
                                        </a>
                                    </div>
                                @endif

                                @if($user->email)
                                    <div class="my-2">
                                        <a href="mailto:{{ $user->email }}" class="text-decoration-none bg-white d-flex justify-content-start align-items-center border border-start px-2">
                                            <i class="bi bi-envelope fs-4 text-primary mt-2"></i>
                                            <span class="px-2">@lang('common_email')</span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection
