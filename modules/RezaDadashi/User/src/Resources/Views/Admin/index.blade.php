@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">@lang('users_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('users_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="d-flex justify-content-between flex-column flex-lg-row">
                            <div class="dropdown bg-white border border-2 col-lg-4 my-2">
                                <div class="dropdown-toggle d-flex align-items-center justify-content-between p-2" data-bs-toggle="dropdown">@lang('users_search_title')</div>
                                <div class="dropdown-menu col-4 w-100 mt-1 p-3 shadow">
                                    <form method="GET" autocomplete="off">
                                        <input type="text" name="name" value="{{ request('name') }}" placeholder="@lang('common_name')" class="form-control mb-3">
                                        <input type="number" name="mobile" value="{{ request('mobile') }}" placeholder="@lang('common_mobile_number')" class="form-control mb-3">
                                        <input type="email" name="email" value="{{ request('email') }}" placeholder="@lang('common_email')" class="form-control mb-3">
                                        <x-select name="status" class="mb-3">
                                            @foreach(\RezaDadashi\User\Models\User::$statuses as $status)
                                                <option @if (request('status') == $status) selected @endif value="{{ $status }}">@lang($status)</option>
                                            @endforeach
                                        </x-select>
                                        <div class="text-end">
                                            <button class="btn btn-primary" type="submit">
                                                <span>@lang('common_search')</span>
                                                <i class="bi bi-search text-white"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="text-end"><a class="btn btn-success my-2" href="{{ route('users.showFormNewUser') }}">@lang('create_user')</a></div>
                        </div>

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead>
                                <tr>
                                    <th scope="col">@lang('common_id')</th>
                                    <th scope="col">@lang('common_name')</th>
                                    <th scope="col">@lang('users_avatar')</th>
                                    <th scope="col">@lang('common_email')</th>
                                    <th scope="col">@lang('common_mobile_number')</th>
                                    <th scope="col">@lang('users_account_status')</th>
                                    <th scope="col">@lang('users_role')</th>
                                    <th scope="col">@lang('users_create_account')</th>
                                    <th scope="col">@lang('users_ip')</th>
                                    <th scope="col">@lang('common_actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>
                                            <a href="{{ route('users.show', $user->id) }}">
                                                <img width="40" height="40" class="rounded-circle border border-3"
                                                     src="{{ $user->thumb }}" alt="">
                                            </a>
                                        </td>
                                        <td><a class="text-decoration-none"
                                               href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                        <td><a class="text-decoration-none"
                                               href="tel:{{ $user->mobile }}">{{ $user->mobile }}</a></td>
                                        <td id="account-verify">
                                            @if($user->hasVerifiedEmail())
                                                <span class="text-success">@lang('users_account_status_accepted')</span>
                                            @else
                                                <span class="text-danger">@lang('users_account_status_awaiting_accepted')</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if(count($user->roles) > 0)
                                                <ul class="list-unstyled">
                                                    @foreach($user->roles as $userRole)
                                                        <li>
                                                            <span class="badge bg-danger">@lang($userRole->name)</span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @else
                                                <span class="badge bg-success">@lang('normal user')</span>
                                            @endif
                                        </td>
                                        <td>{{ getJalali($user->created_at) }}</td>
                                        <td>{{ $user->ip }}</td>
                                        <td>
                                            <a class="bi bi-eye-fill text-info fs-6 px-1"
                                               href="{{ route('users.show', $user->id) }}" title="مشاهده"></a>
                                            <a class="bi bi-pencil-square text-success fs-6 px-1"
                                               href="{{ route('users.edit', $user->id) }}" title="ویرایش"></a>
                                            <a data-route="{{ route('users.destroy', $user->id) }}"
                                               class="deleteItem bi bi-trash text-danger fs-6 px-1" href="javascript:;"
                                               title="حذف"></a>
                                            @if(! $user->hasVerifiedEmail())
                                                <a data-route="{{ route('manualVerify', $user->id) }}"
                                                   data-field="account-verify"
                                                   data-status="<span class='text-success'>تایید شده</span>"
                                                   class="updateStatus bi bi-check-lg text-black-50 fs-5 px-1"
                                                   href="javascript:;" title="تایید حساب کاربری"></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="text-center d-flex justify-content-center">
                                {{ $users->links('pagination::bootstrap-4') }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
