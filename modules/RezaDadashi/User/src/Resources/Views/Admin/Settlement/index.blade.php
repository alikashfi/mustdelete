@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('settlement.index') }}">@lang('user_settlement_title')</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">

            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('user_settlement_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead>
                                <tr>
                                    <th>@lang('user_settlement_id')</th>
                                    <th>@lang('user_settlement_request_id')</th>
                                    <th>@lang('user_settlement_word_count')</th>
                                    <th>@lang('user_settlement_amount')</th>
                                    <th>@lang('user_settlement_status')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($settlements as $settlement)
                                    <tr>
                                        <td>{{ $settlement->id }}</td>
                                        <td>
                                            <a href="{{ route('requests.show', $settlement->request_id) }}">{{ $settlement->request_id }}</a>
                                        </td>
                                        <td>{{ number_format($settlement->request->word_count['supervisor']) }}</td>
                                        <td>{{ number_format($settlement->amount) }} @lang('Toman')</td>
                                        <td>
                                            @if($settlement->paid_at)
                                                <span class="badge bg-success">@lang('partners_expert_payment_status_paid')</span>
                                                <br><span class="badge text-muted bg-light border">{{ getJalali($settlement->paid_at) }}</span>
                                            @else
                                                <span class="badge bg-danger">@lang('partners_expert_payment_status_waiting')</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="text-center d-flex justify-content-center">
{{--                                {{ $users->links('pagination::bootstrap-4') }}--}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
