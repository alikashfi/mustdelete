@extends('Dashboard::master')

@section('css')
    <link rel="stylesheet" href="{{ asset('/assets/css/select2.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('/assets/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">@lang('users_title')</a></li>
    <li class="breadcrumb-item"><a href="#">@lang('create_user')</a></li>
@endsection

@section('content')
<div class="p-3 card">
    <div class="card border-secondary">
        <div class="card-header bg-secondary text-white">
            @lang('create_user')
        </div>
        <div class="card-body">
            <form action="{{ route('users.newUser') }}" method="post" autocomplete="off">
                @csrf
                <div class="row">
                    <div class="col-lg-4 my-2">
                        <label for="name" class="form-label">
                            @lang('common_name')
                            <span class="required">*</span>
                        </label>
                        <x-input id="name" type="text" value="{{ old('name') }}" name="name" class="" />
                    </div>
                    <div class="col-lg-4 my-2">
                        <label for="mobile" class="form-label">
                            @lang('common_mobile_number')
                            <span class="required">*</span>
                        </label>
                        <x-input id="mobile" type="number" value="{{ old('mobile') }}" name="mobile" class="text-end" />
                    </div>
                    <div class="col-lg-4 my-2">
                        <label for="role" class="form-label">
                            @lang('common_user_role')
                        </label>
                        <x-select name="role[]" id="role" class="js-example-basic-multiple" multiple="multiple">
                            @foreach($roles as $role)
                                <option @if(!is_null(old('role')) && in_array($role->name, old('role'))) selected @endif value="{{ $role->name }}">@lang($role->name)</option>
                            @endforeach
                        </x-select>
                    </div>
                    <div class="col-lg-4 my-2">
                        <label for="password" class="form-label">
                            @lang('common_password')
                            <span class="required">*</span>
                        </label>
                        <x-input name="password" value="" type="password" id="password" />
                    </div>
                    <div class="col-lg-4 my-2">
                        <label for="password-confirm" class="form-label">
                            @lang('common_password_confirmation')
                            <span class="required">*</span>
                        </label>
                        <x-input name="password_confirmation" value="" type="password" id="password-confirm" />
                    </div>
                </div>
                <div class="text-end">
                    <button type="submit" class="btn btn-outline-dark mt-2">@lang('common_add')</button>
                    <a href="{{ route('users.index') }}" class="btn btn-outline-danger mt-2">@lang('common_cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
