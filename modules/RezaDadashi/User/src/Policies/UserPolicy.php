<?php

namespace RezaDadashi\User\Policies;

use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage($user)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_USERS)) return true;
    }

    public function partners($user)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_SETTLEMENT_PARTNERS)) return true;
    }

    public function mySettlement($user)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH)) return true;
        if ($user->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN)) return true;
        if ($user->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR)) return true;
    }
}
