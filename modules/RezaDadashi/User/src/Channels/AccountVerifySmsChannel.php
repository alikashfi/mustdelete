<?php

namespace RezaDadashi\User\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Core\Services\SMSService;

class AccountVerifySmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $password = passwordGenerator();
        $notifiable->password = bcrypt($password);
        $notifiable->save();

        $pattern = iranianMobile($notifiable->mobile) ? 'ztk0oodqyd' : 'ztk0oodqyd';

        SMSService::send($notifiable->mobile, $pattern, [
            'verification_code' => $notification->toSms($notifiable),
            'password' => $password,
        ]);
    }
}
