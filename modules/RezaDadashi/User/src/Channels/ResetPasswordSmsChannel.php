<?php

namespace RezaDadashi\User\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Core\Services\SMSService;

class ResetPasswordSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        Log::info($notifiable->mobile . ' : sms reset password code is: ' . $notification->toSms($notifiable));

        $pattern = iranianMobile($notifiable->mobile) ? '4kw6pt3cy3' : '4kw6pt3cy3';

        SMSService::send($notifiable->mobile, $pattern, [
            'code' => $notification->toSms($notifiable),
        ]);
    }
}
