<?php

namespace RezaDadashi\Request\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use RezaDadashi\Comment\Models\Comment;
use RezaDadashi\Core\Responses\AjaxResponses;
use RezaDadashi\Discount\Repositories\DiscountRepository;
use RezaDadashi\Media\Models\Media;
use RezaDadashi\Media\Services\MediaFileService;
use RezaDadashi\Payment\Gateways\Gateway;
use RezaDadashi\Payment\Models\Payment;
use RezaDadashi\Payment\Repositories\PaymentRepository;
use RezaDadashi\Payment\Services\PaymentService;
use RezaDadashi\Request\Classes\DocumentWordCount;
use RezaDadashi\Request\Http\Requests\NewRequest;
use RezaDadashi\Request\Http\Requests\RequestUpdateRequest;
use RezaDadashi\Request\Http\Requests\SatisfactionRequest;
use RezaDadashi\Request\Http\Requests\SetSupervisorRequest;
use RezaDadashi\Request\Http\Requests\SupervisorRejectDescriptionRequest;
use RezaDadashi\Request\Http\Requests\UpdateDeliveryFileRequest;
use RezaDadashi\Request\Http\Requests\UpdateUserEnteredWordCountRequest;
use RezaDadashi\Request\Http\Requests\UploadPaymentReceiptRequest;
use RezaDadashi\Request\Models\History;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Notifications\RejectSatisfactionNotification;
use RezaDadashi\Request\Notifications\RequestCheckAgainExpertNotification;
use RezaDadashi\Request\Notifications\RequestCheckBySupervisorNotification;
use RezaDadashi\Request\Notifications\RequestCreatedNotification;
use RezaDadashi\Request\Notifications\RequestSettlementCustomerNotification;
use RezaDadashi\Request\Notifications\RequestTakedNotification;
use RezaDadashi\Request\Repositories\HistoryRepository;
use RezaDadashi\Request\Repositories\RequestRepository;
use RezaDadashi\Request\Services\RequestService;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\Setting\Models\Setting;
use RezaDadashi\Setting\Repositories\SettingRepository;
use RezaDadashi\User\Repositories\ExpertPaymentRepository;
use RezaDadashi\User\Repositories\UserRepository;

class RequestController extends Controller
{
    private $requestRepository;
    private $paymentRepository;
    private $historyRepository;
    private $userRepository;
    private $expertPaymentRepository;
    private $discountRepository;

    public function __construct(RequestRepository $requestRepository, PaymentRepository $paymentRepository, HistoryRepository $historyRepository, UserRepository $userRepository, ExpertPaymentRepository $expertPaymentRepository, DiscountRepository $discountRepository)
    {
        $this->requestRepository = $requestRepository;
        $this->paymentRepository = $paymentRepository;
        $this->historyRepository = $historyRepository;
        $this->userRepository = $userRepository;
        $this->expertPaymentRepository = $expertPaymentRepository;
        $this->discountRepository = $discountRepository;
    }

    private function upload($fieldName)
    {
        if (\request()->hasFile($fieldName)) {  //check the file present or not
            $image = \request()->file($fieldName); //get the file
            $name = md5(time() . time()).'.'.$image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/uploads/word_file'); //public path folder dir
            $image->move($destinationPath, $name);  //mve to destination you mentioned

            $media = Media::create([
                'user_id' => auth()->id(),
                'files' => ['doc' => $name],
                'type' => 'doc',
                'filename' => $image->getClientOriginalName(),
                'is_private' => 0,
            ]);
            return $media;
        }
    }

    private function uploadReceiptFile($fieldName)
    {
        if (\request()->hasFile($fieldName)) {  //check the file present or not
            $image = \request()->file($fieldName); //get the file
            $name = md5(time() . time()).'.'.$image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/uploads/receipt'); //public path folder dir
            $image->move($destinationPath, $name);  //mve to destination you mentioned

            $media = Media::create([
                'user_id' => auth()->id(),
                'files' => ['original' => $name],
                'type' => 'image',
                'filename' => $image->getClientOriginalName(),
                'is_private' => 0,
            ]);
            return $media;
        }
    }

    private function addHistory($requestId, $body)
    {
        $this->historyRepository->addHistory($requestId, $body);
    }

    public function index()
    {
        /** Check Permission */
        $this->authorize('index', Request::class);

        if (auth()->user()->hasAnyPermission([Permission::PERMISSION_MANAGE_REQUESTS, Permission::PERMISSION_SUPER_ADMIN])) {
            $requests = $this->requestRepository
                ->searchId(request('id'))
                ->searchEmail(request('email'))
                ->searchMobile(request('mobile'))
                ->searchSupervisor(request('supervisor'))
                ->searchExpert(request('expert'))
                ->searchType(request('type'))
                ->searchStatus(request('status'))
                ->paginateLatest();
        } else {
            $requests = $this->requestRepository;
            if (auth()->user()->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH)) {
                $requests->persianToEnglish();
            }
            if (auth()->user()->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN)) {
                $requests->englishToPersian();
            }
            if (auth()->user()->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR)) {
                $requests->englishEditing();
            }
            $requests = $requests->paginateLatest();
        }
        return view('Request::Admin.index', compact('requests'));
    }

    public function my()
    {
        $requests = $this->requestRepository
            ->searchId(request('id'))
            ->searchSupervisor(request('supervisor'))
            ->searchExpert(request('expert'))
            ->searchType(request('type'))
            ->getByUserId(auth()->id())
            ->paginateLatest();
        return view('Request::Admin.my', compact('requests'));
    }

    public function showNewRequestForm()
    {
        return view('Request::Front.newRequest');
    }

    public function store(NewRequest $request)
    {
        $user = auth()->user();

        $mediaFilename = null;
        $mediaFileWordCount = 0;
        if ($request->hasFile('file')) {
            $media = $this->upload('file');
            $request->request->add([
                'media_id' => $media->id
            ]);
        }

        $filename =  public_path('/uploads/word_file/') . $media->files['doc'];
        $wordCount =  new DocumentWordCount($filename);
        $mediaFileWordCount = wordCount($wordCount->convertToText());

        $prices = RequestService::calculationPrice($request->type, $mediaFileWordCount);


        $newRequest = $this->requestRepository->create([
            'type' => $request->type,
            'user_id' => $user->id,
            'media_id' => $request->media_id,
            'word_count' => [
                'user' => $request->word_count ? $request->word_count : 0,
                'system' => $mediaFileWordCount,
                'supervisor' => $mediaFileWordCount
            ],
            'price' => $prices['toman'],
            'price_dollar' => $prices['dollar'],
        ]);

        /** Add Log History */
        $this->addHistory($newRequest->id, History::HISTORY_CREATE);

        newFeedback();
        return redirect()->route('requests.deposit', $newRequest->id);
    }

    public function update($requestId, RequestUpdateRequest $request)
    {
        $oldRequest = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('update', $oldRequest);

        $this->requestRepository->updateRequest($requestId, $request);
        $newRequest = $oldRequest->fresh();

        $changes = getDiff($oldRequest, $newRequest, ['word_count']);

        //فقط یکبار پیامک ارسال شود؛ زمانی که وضعیت بررسی مجدد نباشد، اما به بررسی مجدد آپدیت شود
        if ($oldRequest->status !== Request::STATUS_SUPERVISOR_REJECT) {
            if ($newRequest->status == Request::STATUS_SUPERVISOR_REJECT) {
                if ($oldRequest->supervisor) {
                    /** Send Notification To Expert */
                    Notification::send($newRequest->supervisor, (new RequestCheckAgainExpertNotification($newRequest)));
                }
            }
        }


        if ($oldRequest->word_count['supervisor'] != $newRequest->word_count['supervisor']) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_WORD_COUNT . $request->word_count);
        }

        //increase expert balance
        if  ($request->status == Request::STATUS_CLOSED) {
            $expertPaymentForOwner = $oldRequest->expertPayment;
            if (!$expertPaymentForOwner) {
                $expertWage = $this->userRepository->calculateWage($oldRequest->owner_id, $oldRequest->id);
                $this->expertPaymentRepository->create([
                    'user_id' => $oldRequest->owner_id,
                    'request_id' => $oldRequest->id,
                    'amount' => $expertWage,
                ]);
                $this->addHistory($requestId,History::HISTORY_EXPERT_PAYMENT);
            }
        }

        if (!is_null($changes) && array_key_exists('price', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_PRICE . $changes['price']);
        }
        if (!is_null($changes) && array_key_exists('price_dollar', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_PRICE_DOLLAR . $changes['price_dollar']);
        }
        if (!is_null($changes) && array_key_exists('is_dollar', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_IS_DOLLAR);
        }
        if (!is_null($changes) && array_key_exists('deposit_payment', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_DEPOSIT_PAYMENT . $changes['deposit_payment']);
        }
        if (!is_null($changes) && array_key_exists('final_payment', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_FINAL_PAYMENT . $changes['final_payment']);
        }
        if (!is_null($changes) && array_key_exists('supervisor_id', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_SUPERVISOR . $changes['supervisor_id']);
        }
        if (!is_null($changes) && array_key_exists('owner_id', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_OWNER . $changes['owner_id']);
        }
        if (!is_null($changes) && array_key_exists('status', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_STATUS . $changes['status']);
        }
        if (!is_null($changes) && array_key_exists('type', $changes)) {
            $this->addHistory($requestId, History::HISTORY_CHANGE_TYPE . $changes['type']);
        }

        newFeedback();
        return back();
    }

    public function show($requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('show', $request);

        $supervisors = $this->userRepository->getByPermission(Permission::PERMISSION_SUPERVISOR);

        $requestOwnerPermission = $this->requestRepository->getPermissionByRequestType($request);
        $owners = $this->userRepository->getByPermission($requestOwnerPermission);

        return view('Request::Admin.show', compact('request', 'supervisors', 'owners'));
    }

    public function supervisorNote(SupervisorRejectDescriptionRequest $request)
    {
        $findRequest = $this->requestRepository->findOrFail($request->request_id);

        /** Check Permission */
        $this->authorize('supervisorNote', $findRequest);

        if ($request->hasFile('file')) {

            //قبلا فایل بارگذاری شده است
            if ($findRequest->supervisorFile) {
                $findRequest->supervisorFile->delete();
            }

            $media = $this->upload('file');
            $request->request->add([
                'supervisor_media_id' => $media->id
            ]);
        }

        $findRequest->update([
            'supervisor_id' => auth()->id(),
            'status' => Request::STATUS_SUPERVISOR_REJECT,
            'supervisor_description' => $request->supervisor_description,
            'supervisor_media_id' => $request->supervisor_media_id ?
                $request->supervisor_media_id : $findRequest->supervisor_media_id
        ]);

        /** Add Log History */
        $this->addHistory($findRequest->id, History::HISTORY_REJECT_SUPERVISOR);

        /** Send Notification To Expert */
        $notifiableUsers = $this->userRepository->findOrFail($findRequest->owner_id);
        Notification::send($notifiableUsers, (new RequestCheckAgainExpertNotification($findRequest)));

        newFeedback();
        return back();
    }

    public function showDepositForm($requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('deposit', $request);

        return view('Request::Front.deposit', compact('request'));
    }

    public function deposit($requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('deposit', $request);

        $amount = $request->getFinalDepositAmount(\request()->code);
        $type = Payment::TYPE_DEPOSIT;

        if ($amount <= 0) {

            if ($code = \request()->code) {
                $discountExist = $this->discountRepository->getByDiscountCode($code);
                $this->discountRepository->used($discountExist);
            }


            $this->requestRepository->update(
                ['id'    => $requestId],
                [
                    'status' => Request::STATUS_DEPOSIT
                ]
            );
            $this->addHistory($requestId, History::HISTORY_FULL_DISCOUNT);


            /** Send Notification To Experts */
            $notifiableUsers = collect();
            if ($request->type == Request::REQUEST_PERSIAN_TO_ENGLISH || $request->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
                $notifiableUsers = $this->userRepository->getAllTranslators();
            } else {
                $notifiableUsers = $this->userRepository->getAllEditors();
            }
            Notification::send($notifiableUsers, (new RequestCreatedNotification($request)));


            newFeedback();
            return redirect()->route('requests.show', $requestId);
        }

        // if wanna to pay an existing_payment. find that and request a gateway for it and update invoice.
        if (\request()->existing_payment && ! ($amount <= 0 || is_null($request->id) || ! auth()->check())) {
            $invoiceId = resolve(Gateway::class)->request($amount, $request->type);
            $payment = tap($request->payments()->first(), function ($payment) use ($invoiceId) {$payment->update(['invoice_id' => $invoiceId]);});
        } else {
            $payment = PaymentService::generate($amount, $type, $request, auth()->user(), \request()->code);
        }

        resolve(Gateway::class)->redirect($payment->invoice_id);
    }

    public function uploadReceipt($requestId, UploadPaymentReceiptRequest $request)
    {
        $findRequest = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('paymentFileReceipt', $findRequest);

        $media = $this->uploadReceiptFile('file');

        // file receipt uploaded is dollar
        if ($media && $request->is_dollar == 1) {
            $findRequest->is_dollar = 1;
            $findRequest->save();
        }

        if ($findRequest->status == Request::STATUS_PENDING) {
            if ($findRequest->depositFile) {
                $findRequest->depositFile->delete();
            }

            $findRequest->deposit_media_id = $media->id;
            $findRequest->save();

            /** Add Log History */
            $this->addHistory($requestId, History::HISTORY_UPLOAD_DEPOSIT_FILE);

            $this->requestRepository->changeStatus($requestId, Request::STATUS_DEPOSIT);

            /** Send Notification To Experts */
            $notifiableUsers = collect();
            if ($findRequest->type == Request::REQUEST_PERSIAN_TO_ENGLISH || $findRequest->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
                $notifiableUsers = $this->userRepository->getAllTranslators();
            } else {
                $notifiableUsers = $this->userRepository->getAllEditors();
            }
            Notification::send($notifiableUsers, (new RequestCreatedNotification($findRequest)));

        }
        elseif ($findRequest->status == Request::STATUS_SUPERVISOR_ACCEPT) {
            if ($findRequest->settlementFile) {
                $findRequest->settlementFile->delete();
            }
            $findRequest->settlement_media_id = $media->id;
            $findRequest->save();

            /** Add Log History */
            $this->addHistory($requestId, History::HISTORY_UPLOAD_SETTLEMENT_FILE);

            $this->requestRepository->changeStatus($requestId, Request::STATUS_SETTLEMENT);
        }

        newFeedback();
        return redirect(route('requests.show', $requestId));
    }

    public function setOwner($requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('setOwner', $request);

        $request->owner_id = auth()->id();
        $request->status = Request::STATUS_TAKE;
        $request->save();

        /** Add Log History */
        $this->addHistory($request->id, History::HISTORY_ACCEPT_OWNER);


        /** Send Notification Request Taked To Experts */
        $notifiableUsers = collect();
        if ($request->type == Request::REQUEST_PERSIAN_TO_ENGLISH || $request->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
            $notifiableUsers = $this->userRepository->getAllTranslators();
        } else {
            $notifiableUsers = $this->userRepository->getAllEditors();
        }
        Notification::send($notifiableUsers, (new RequestTakedNotification($request)));

        newFeedback();
        return back();
    }

    public function setSupervisor($requestId, SetSupervisorRequest $request)
    {
        $findRequest = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('setSupervisor', $findRequest);

        $findRequest->supervisor_id = auth()->id();
        $findRequest->status = Request::STATUS_SUPERVISOR_ACCEPT;
        $findRequest->save();

        /** Add Log History */
        $this->addHistory($findRequest->id, History::HISTORY_ACCEPT_SUPERVISOR);

        /** Send Notification To Customer */
        $notifiableUsers = $this->userRepository->findOrFail($findRequest->user_id);
        Notification::send($notifiableUsers, (new RequestSettlementCustomerNotification($findRequest)));

        newFeedback();
        return back();
    }

    public function updateDeliveryFile($requestId, UpdateDeliveryFileRequest $request)
    {
        $findRequest = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('deliveryFile', $findRequest);

        $media = $this->upload('file');
        $mediaFilename = $media->files['doc'];
        $filename = public_path('/uploads/word_file/') . $mediaFilename;
        $wordCount =  new DocumentWordCount($filename);
        $mediaFileWordCount = wordCount($wordCount->convertToText());

        if ($findRequest->deliveryFile) {
            $findRequest->deliveryFile->delete();
        }

        //calculate amount with new file uploaded by expert
        $totalAmount = RequestService::calculationPrice($findRequest->type, $mediaFileWordCount);
        $findRequest->update([
            'delivery_media_id' => $media->id,
            'word_count->supervisor' => $mediaFileWordCount,
            'price' => $totalAmount['toman'],
            'price_dollar' => $totalAmount['dollar'],
        ]);

        /** Add Log History */
        $this->addHistory($requestId, History::HISTORY_DONE);



        $this->requestRepository->changeStatus($requestId, Request::STATUS_DONE);


        /** Send Notification Request Taked To Experts */
        $notifiableUsers = $this->userRepository->getAllSupervisors();
        Notification::send($notifiableUsers, (new RequestCheckBySupervisorNotification($findRequest)));

        newFeedback();
        return back();
    }

    public function showSettlementForm($requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('settlement', $request);

        return view('Request::Front.settlement', compact('request'));
    }

    public function settlement($requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('settlement', $request);

        $amount = $request->getSettlementPayable();
        $type = Payment::TYPE_SETTLEMENT;
        $payment = PaymentService::generate($amount, $type, $request, auth()->user());

        if ($amount <= 0) {
            $this->addHistory($requestId, History::HISTORY_AMOUNT_LESS_ZERO);
            $request->status = Request::STATUS_SETTLEMENT;
            $request->save();
            return redirect()->route('requests.show', $requestId);
        }

        resolve(Gateway::class)->redirect($payment->invoice_id);
    }

    public function satisfaction($requestId, SatisfactionRequest $request)
    {
        $findRequest = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('satisfaction', $findRequest);

        $status = null;
        if (!$request->satisfaction) {

            /** Send Notification To The Expert */
            if ($findRequest->supervisor) {
                Notification::send($findRequest->supervisor, (new RejectSatisfactionNotification($findRequest)));
            }

            $this->addHistory($requestId,History::HISTORY_DISSATISFACTION);
            $status = Request::STATUS_COMEBACK;
        } else {

            //increase expert balance
            $expertPaymentForOwner = $findRequest->expertPayment;
            if (!$expertPaymentForOwner) {
                $expertWage = $this->userRepository->calculateWage($findRequest->owner_id, $findRequest->id);
                $this->expertPaymentRepository->create([
                    'user_id' => $findRequest->owner_id,
                    'request_id' => $findRequest->id,
                    'amount' => $expertWage,
                ]);
                $this->addHistory($requestId,History::HISTORY_EXPERT_PAYMENT);
            }

            $this->addHistory($requestId,History::HISTORY_SATISFACTION);
            $status = Request::STATUS_CLOSED;
        }



        $findRequest->status = $status;
        $findRequest->save();

        newFeedback();
        return back();
    }

    public function factor($requestId)
    {
        $request = $this->requestRepository->findOrFail($requestId);

        /** Check Permission */
        $this->authorize('factor', $request);

        return view('Request::Admin.layouts.factor', compact('request'));
    }

    public function changeUserEnteredWordCount(UpdateUserEnteredWordCountRequest $request)
    {
        $findRequest = $this->requestRepository->findOrFail($request->request_id);

        /** Check Permission */
        $this->authorize('changeWordCount', $findRequest);

        $findRequest->update([
            'word_count->user' => $request->number
        ]);

        return AjaxResponses::successResponse();
    }

    public function directLink(\Illuminate\Http\Request $request)
    {
        $request_id = $request->route('request_id');
        $userToken = $request->route('user_token');
        $type = $request->route('type');
        $key = $request->key;



        //check the parameters is valid
        $userRole = checkRequestDirectLink($request_id, $userToken, $type, $key);

        if (! $userRole) abort(404);

        //find user
        $user = $this->userRepository->findByToken($userToken);

        //login this user
        auth()->loginUsingId($user->id);

        //find request
        $targetRequest = $this->requestRepository->findOrFail($request_id);

        if ($userRole == \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_SUPERVISOR) {

            if ($targetRequest->supervisor_id) abort(403);
            $targetRequest->supervisor_id = $user->id;
            $targetRequest->status = Request::STATUS_SUPERVISOR_ACCEPT;
            $targetRequest->save();

            /** Send Notification To Customer */
            $notifiableUsers = $this->userRepository->findOrFail($targetRequest->user_id);
            Notification::send($notifiableUsers, (new RequestSettlementCustomerNotification($targetRequest)));

            newFeedback();
            return redirect()->route('requests.show', $request_id);
        }
        else if ($userRole == \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_OWNER) {

            if ($targetRequest->owner_id) abort(403);
            $targetRequest->owner_id = $user->id;
            $targetRequest->status = Request::STATUS_TAKE;
            $targetRequest->save();

            /** Send Notification Request Taked To Experts */
            $notifiableUsers = collect();
            if ($targetRequest->type == Request::REQUEST_PERSIAN_TO_ENGLISH || $targetRequest->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
                $notifiableUsers = $this->userRepository->getAllTranslators();
            } else {
                $notifiableUsers = $this->userRepository->getAllEditors();
            }
            Notification::send($notifiableUsers, (new RequestTakedNotification($targetRequest)));

            newFeedback();
            return redirect()->route('requests.show', $request_id);
        }
        else if ($userRole == \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_USER_LOGIN) {
            return redirect()->route('requests.show', $request_id);
        }
        else if ($userRole == \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_DOWNLOAD_CUSTOMER_FILE) {
            if (! $targetRequest->file) abort(404);
            return redirect($targetRequest->downloadLink());
        }
        else if ($userRole == \RezaDadashi\Request\Models\Request::DIRECT_LINK_ACTION_DOWNLOAD_FINAL_FILE) {
            if (! $targetRequest->deliveryFile) abort(404);
            return redirect($targetRequest->downloadLinkDelivery());
        }

        return abort(404);
    }
}
