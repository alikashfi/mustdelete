<?php

namespace RezaDadashi\Request\Http\Controllers;

use App\Http\Controllers\Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use RezaDadashi\Request\Http\Requests\ExportExcelRequest;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Repositories\RequestRepository;

class ReportController extends Controller
{
    private $requestRepository;

    public function __construct(RequestRepository $requestRepository)
    {
        $this->requestRepository = $requestRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function requestsReport()
    {
        /** Check Permission */
        $this->authorize('report', Request::class);

        return view('Request::Admin.reports.requests');
    }


    private function exportSheet($data)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '4000M');

        try {
            $spreadSheet = new Spreadsheet();

            //set column with
            $spreadSheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
            $spreadSheet->getActiveSheet()->fromArray($data);

            // right to left
            $spreadSheet->getActiveSheet()->setRightToLeft(true);
            $excelWriter = new Xls($spreadSheet);

            $outputFilename = 'خروجی سفارشات الف پلاس' . time() . '.xls';

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $outputFilename . '"');
            header('Cache-Control: max-age=0');
            ob_end_clean();
            $excelWriter->save('php://output');
            return back();
        } catch (Exception $e) {
            return abort(402);
        }
    }

    public function exportExcel(ExportExcelRequest $request)
    {
        /** Check Permission */
        $this->authorize('report', Request::class);

        $searchRequest = $request->all();
        $searchRequest['start_date'] = $request['start_date'] ? dateFromJalali($request['start_date']) : null;
        $searchRequest['end_date'] = $request['end_date'] ? dateFromJalali($request['end_date']) : null;

        $data = $this->requestRepository->exportExcel($searchRequest);
        $data_array [] = array('شناسه سفارش', 'تاریخ ثبت سفارش', 'نوع درخواست', 'وضعیت','تعداد کلمات(سیستم)',
            'تعداد کلمات(ناظر)',  'مبلغ دریافتی در بیعانه', 'مبلغ دریافتی در تسویه', 'نام سفارش دهنده(مشتری)',
            'نام مترجم/ویراستار', 'نام ناظر', 'درآمد حاصل شده');
        foreach($data as $data_item)
        {
            $targetRequest = $this->requestRepository->find($data_item->id);

            $data_array[] = array(
                'id' => $targetRequest->id,
                'created_at' => getJalali($targetRequest->created_at),
                'type' => trans($targetRequest->type),
                'status' => trans($targetRequest->status),
                'system_word_count' => $targetRequest->word_count['system'],
                'supervisor_word_count' => $targetRequest->word_count['supervisor'],
                'deposit_payed' => $targetRequest->deposit_payment,
                'finale_payed' => $targetRequest->final_payment,
                'user_id' => $targetRequest->user ? $targetRequest->user->name : null,
                'owner' => $targetRequest->owner ? $targetRequest->owner->name : null,
                'supervisor' => $targetRequest->supervisor ? $targetRequest->supervisor->name : null,
                'price' => $targetRequest->deposit_payment + $targetRequest->final_payment,
            );
        }
        $this->exportSheet($data_array);
    }
}
