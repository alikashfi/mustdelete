<?php

namespace RezaDadashi\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class ExportExcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => ['nullable'],
            'end_date' => ['nullable']
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'start_date' => 'تاریخ شروع',
            'end_date' => 'تاریخ پایان',
        ] : [];
    }
}
