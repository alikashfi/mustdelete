<?php

namespace RezaDadashi\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use RezaDadashi\Request\Models\Request;

class NewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // min file size:10 KB
            // max file size:10 MB
            'file' => ['required', 'mimes:doc,docx'],
            'type' => ['required', Rule::in(Request::$requests)],
            'word_count' => ['nullable', 'numeric', 'max:30000']
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'file' => 'فایل',
            'type' => 'نوع درخواست',
        ] : [];
    }
}
