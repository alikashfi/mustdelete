<?php

namespace RezaDadashi\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Rules\ValidOwner;
use RezaDadashi\Request\Rules\ValidOwnerByRequest;
use RezaDadashi\Request\Rules\ValidSupervisor;

class RequestUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'word_count' => 'required|numeric',
            'type' => ['required', Rule::in(Request::$requests)],
            'is_dollar' => ['nullable', 'boolean'],
            'deposit_payment' => ['required', 'numeric'],
            'status' => ['required', Rule::in(Request::$statuses)],
            'supervisor_id' => ['nullable', 'numeric', new ValidSupervisor()],
            'owner_id' => ['nullable', 'numeric', new ValidOwner(), new ValidOwnerByRequest(\request()->route('request'))],
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'word_count' => 'تعداد کلمات',
            'type' => 'نوع درخواست',
            'price' => 'مبلغ',
            'deposit_payment' => 'بیعانه پرداخت شده',
            'final_payment' => 'تسویه نهایی',
            'status' => 'وضعیت',
            'supervisor_id' => 'ناظر',
            'owner_id' => 'کارشناس',
        ] : [];
    }
}
