<?php

namespace RezaDadashi\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use RezaDadashi\Request\Models\Request;

class UpdateUserEnteredWordCountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'request_id' => ['required', 'numeric', 'exists:requests,id'],
            'number' => ['required', 'numeric', 'min:0', 'max:1000000'],
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'request_id' => 'درخواست',
            'number' => 'تعداد کلمات',
        ] : [];
    }
}
