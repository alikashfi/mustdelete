<?php

namespace RezaDadashi\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use RezaDadashi\Request\Models\Request;

class SupervisorRejectDescriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'request_id' => ['required', 'numeric', 'exists:requests,id'],
            'file' => ['nullable', 'mimes:doc,docx'],
            'supervisor_description' => ['required', 'max:1000']
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'file' => 'فایل',
            'supervisor_description' => 'توضیحات',
        ] : [];
    }
}
