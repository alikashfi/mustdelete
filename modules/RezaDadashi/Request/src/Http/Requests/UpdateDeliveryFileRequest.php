<?php

namespace RezaDadashi\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class UpdateDeliveryFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required', 'mimes:doc,docx', 'max:10240'],
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'file' => 'فایل نهایی',
        ] : [];
    }
}
