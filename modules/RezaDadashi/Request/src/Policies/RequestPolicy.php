<?php

namespace RezaDadashi\Request\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\Setting\Models\Setting;
use RezaDadashi\Setting\Repositories\SettingRepository;

class RequestPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function index($user)
    {
        if (
            ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_REQUESTS)) ||
            ($user->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH)) ||
            ($user->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN)) ||
            ($user->hasPermissionTo(Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR)) ||
            ($user->hasPermissionTo(Permission::PERMISSION_SUPERVISOR))
        ) return true;
    }

    public function canShowRequest($user, $request)
    {
        if ($request->type == Request::REQUEST_PERSIAN_TO_ENGLISH) {
            if ($user->can(Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH)) {
                return true;
            }
        }

        if ($request->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
            if ($user->can(Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN)) {
                return true;
            }
        }

        if ($request->type == Request::REQUEST_ENGLISH_EDITING) {
            if ($user->can(Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR)) {
                return true;
            }
        }
    }

    public function show($user, $request)
    {
        if (
            ($user->id == $request->user_id) ||
            ($request->supervisor_id == $user->id || is_null($request->supervisor_id)) ||
            ($this->canShowRequest($user, $request)) ||
            ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_REQUESTS))
        ) return true;
    }

    public function supervisorNote($user, $request)
    {
        if (
            $request->status == Request::STATUS_DONE &&
            (
                $user->id == $request->supervisor_id ||
                $user->hasPermissionTo(Permission::PERMISSION_SUPERVISOR)
            )
        )
            return true;
    }

    public function update($user, $request)
    {
        if ($user->can(Permission::PERMISSION_MANAGE_REQUESTS) && $request->status != Request::STATUS_CLOSED) return true;

        if ($user->id == $request->supervisor_id && $request->status != Request::STATUS_CLOSED) return true;
    }

    public function deposit($user, $request)
    {
        if ($user->id == $request->user_id && $request->status == Request::STATUS_PENDING) return true;
    }

    public function paymentFileReceipt($user, $request)
    {
        if (
            (new SettingRepository())->getByKey(Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value &&
            $user->id == $request->user_id &&
            $request->status == Request::STATUS_PENDING ||
            $request->status == Request::STATUS_DONE
        ) return true;
    }

    public function setOwner($user, $request)
    {
        if ($request->status == Request::STATUS_DEPOSIT && is_null($request->owner_id) && $request->type == Request::REQUEST_PERSIAN_TO_ENGLISH) {
            if ($user->can(Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH)) {
                return true;
            }
        }

        if ($request->status == Request::STATUS_DEPOSIT && is_null($request->owner_id) && $request->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
            if ($user->can(Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN)) {
                return true;
            }
        }

        if ($request->status == Request::STATUS_DEPOSIT && is_null($request->owner_id) && $request->type == Request::REQUEST_ENGLISH_EDITING) {
            if ($user->can(Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR)) {
                return true;
            }
        }
    }

    public function setSupervisor($user, $request)
    {
        if ($user->can(Permission::PERMISSION_SUPERVISOR) &&
            ($request->status == Request::STATUS_DONE)
        ) return true;
    }

    public function deliveryFile($user, $request)
    {
        if ($user->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_REQUESTS)) {
            return true;
        }

        if (
            ($request->status == Request::STATUS_TAKE || $request->status == Request::STATUS_SUPERVISOR_REJECT) &&
            $user->id == $request->owner_id) {
            return true;
        }
    }

    public function settlement($user, $request)
    {
        if ($user->id == $request->user_id &&
            $request->status == Request::STATUS_SUPERVISOR_ACCEPT
        ) return true;
    }

    public function satisfaction($user, $request)
    {
        if ($user->id == $request->user_id && $request->status == Request::STATUS_SETTLEMENT) return true;
    }

    public function factor($user, $request)
    {
        if (
            ($request->status == Request::STATUS_CLOSED) &&
            $user->id == $request->user_id || $user->can(Permission::PERMISSION_SUPERVISOR)
        ) return true;
    }

    public function changeWordCount($user, $request)
    {
        if ($user->id == $request->user_id) return true;
    }

    public function report($user)
    {
        if ($user->can(Permission::PERMISSION_SUPERVISOR)) return true;
    }
}
