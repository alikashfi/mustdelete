<?php

namespace RezaDadashi\Request\Services;

use RezaDadashi\Request\Models\Request;
use RezaDadashi\Setting\Models\Setting;
use RezaDadashi\Setting\Repositories\SettingRepository;

class RequestService
{
    public static function calculationPrice($requestType, $wordCount)
    {
        $prices = [
            'toman' => 0,
            'dollar' => 0,
        ];
        if ($requestType == Request::REQUEST_PERSIAN_TO_ENGLISH) {
            $pricePerWord = (new SettingRepository())->getByKey(Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH)->value;
            $dollarPricePerWord = (new SettingRepository())->getByKey(Setting::SETTING_KEY_TRANSLATE_PERSIAN_TO_ENGLISH_DOLLAR)->value;
            $prices['toman'] = $wordCount * $pricePerWord;
            $prices['dollar'] = $wordCount * $dollarPricePerWord;
        }

        else if ($requestType == Request::REQUEST_ENGLISH_TO_PERSIAN) {
            $pricePerWord = (new SettingRepository())->getByKey(Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN)->value;
            $dollarPricePerWord = (new SettingRepository())->getByKey(Setting::SETTING_KEY_TRANSLATE_ENGLISH_TO_PERSIAN_DOLLAR)->value;
            $prices['toman'] = $wordCount * $pricePerWord;
            $prices['dollar'] = $wordCount * $dollarPricePerWord;
        }

        else if ($requestType == Request::REQUEST_ENGLISH_EDITING) {
            $pricePerWord = (new SettingRepository())->getByKey(Setting::SETTING_KEY_ENGLISH_EDITING)->value;
            $dollarPricePerWord = (new SettingRepository())->getByKey(Setting::SETTING_KEY_ENGLISH_EDITING_DOLLAR)->value;
            $prices['toman'] = $wordCount * $pricePerWord;
            $prices['dollar'] = $wordCount * $dollarPricePerWord;
        }
        return $prices;
    }

    public static function calculationPercentAmount($total, $percent)
    {
        return round(($total / 100) * $percent);
    }
}
