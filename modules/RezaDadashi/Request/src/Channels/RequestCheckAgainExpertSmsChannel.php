<?php

namespace RezaDadashi\Request\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Core\Services\SMSService;
use RezaDadashi\Request\Models\Request;

class RequestCheckAgainExpertSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $requestDetail = $notification->toSms($notifiable);
        $requestId = $requestDetail['request_id'];
        $type = $requestDetail['type'];
        $wordCount = $requestDetail['word_count'];
        $showRequestDirectLink = createRequestDirectLink($requestId, $notifiable->token, Request::DIRECT_LINK_ACTION_USER_LOGIN);

        $pattern = iranianMobile($notifiable->mobile) ? 'iscg8t1p0v' : 'iscg8t1p0v';
        SMSService::send($notifiable->mobile, $pattern, [
            'title' => $type,
            'id' => $requestId,
            'link' => $showRequestDirectLink,
        ]);
    }
}
