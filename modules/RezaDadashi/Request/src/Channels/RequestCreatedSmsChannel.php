<?php

namespace RezaDadashi\Request\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Core\Services\SMSService;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Repositories\RequestRepository;

class RequestCreatedSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $requestDetail = $notification->toSms($notifiable);
        $requestId = $requestDetail['request_id'];
        $type = $requestDetail['type'];
        $wordCount = $requestDetail['word_count'];
        $request = (new RequestRepository())->find($requestId);
        $customerFileLink = createRequestDirectLink($requestId, $notifiable->token, Request::DIRECT_LINK_ACTION_DOWNLOAD_CUSTOMER_FILE);
        $directLink = createRequestDirectLink($requestId, $notifiable->token, Request::DIRECT_LINK_ACTION_OWNER);

        $pattern = iranianMobile($notifiable->mobile) ? '7upisljdcx' : '7upisljdcx';
        SMSService::send($notifiable->mobile, $pattern, [
            'title' => $type,
            'word_count' => $wordCount,
            'accept_link' => $directLink,
        ]);
    }
}
