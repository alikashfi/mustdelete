<?php

namespace RezaDadashi\Request\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Core\Services\SMSService;
use RezaDadashi\Request\Models\Request;

class ExpertPaymentSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $notificationDetail = $notification->toSms($notifiable);
        $count = $notificationDetail['count'];
        $amount = number_format($notificationDetail['amount']);
        $link = route('settlement.index');

        $pattern = iranianMobile($notifiable->mobile) ? 'te84a30bnu' : 'te84a30bnu';
        SMSService::send($notifiable->mobile, $pattern, [
            'amount' => $amount,
            'count' => $count,
            'link' => $link,
        ]);
    }
}
