<?php

namespace RezaDadashi\Request\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Core\Services\SMSService;
use RezaDadashi\Request\Models\Request;

class RequestTakedSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $requestDetail = $notification->toSms($notifiable);
        $requestId = $requestDetail['request_id'];
        $type = $requestDetail['type'];
        $wordCount = $requestDetail['word_count'];

        $pattern = iranianMobile($notifiable->mobile) ? 'v7b1v9s4ev' : 'v7b1v9s4ev';
        SMSService::send($notifiable->mobile, $pattern, [
            'id' => $requestId,
            'word_count' => $wordCount,
        ]);
    }
}
