<?php

namespace RezaDadashi\Request\Channels;

use Illuminate\Notifications\Notification;
use RezaDadashi\Core\Services\SMSService;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Repositories\RequestRepository;

class RejectSatisfactionSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $requestDetail = $notification->toSms($notifiable);
        $requestId = $requestDetail['request_id'];
        $type = $requestDetail['type'];
        $wordCount = $requestDetail['word_count'];
        $request = (new RequestRepository())->find($requestId);

        $pattern = iranianMobile($notifiable->mobile) ? 'ba6e0jpr5zzd3r6' : 'ba6e0jpr5zzd3r6';
        SMSService::send($notifiable->mobile, $pattern, [
            'id' => $requestId,
        ]);
    }
}
