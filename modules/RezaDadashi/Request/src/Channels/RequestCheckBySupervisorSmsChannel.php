<?php

namespace RezaDadashi\Request\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use RezaDadashi\Core\Services\SMSService;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Repositories\RequestRepository;

class RequestCheckBySupervisorSmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $requestDetail = $notification->toSms($notifiable);
        $requestId = $requestDetail['request_id'];
        $type = $requestDetail['type'];
        $wordCount = $requestDetail['word_count'];
        $request = (new RequestRepository())->find($requestId);
        $directLink = createRequestDirectLink($requestId, $notifiable->token, Request::DIRECT_LINK_ACTION_USER_LOGIN);

        $pattern = iranianMobile($notifiable->mobile) ? 'svr2y24zyn' : 'svr2y24zyn';
        SMSService::send($notifiable->mobile, $pattern, [
            'id' => $requestId,
            'link' => $directLink,
        ]);
    }
}
