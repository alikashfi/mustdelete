<?php

namespace RezaDadashi\Request\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use RezaDadashi\User\Models\User;

class History extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'request_histories';

    const HISTORY_CREATE = 'Request created';
    const HISTORY_UPLOAD_DEPOSIT_FILE = 'The deposit payment receipt was uploaded';
    const HISTORY_UNSUCCESSFUL_PAYMENT = 'Deposit payment failed';
    const HISTORY_SUCCESSFUL_DEPOSIT = 'The deposit was paid successfully';
    const HISTORY_ACCEPT_SUPERVISOR = 'The request was assigned as an supervisor and the request was approved';
    const HISTORY_REJECT_SUPERVISOR = 'The request was assigned as an supervisor and the request was rejected';
    const HISTORY_ACCEPT_OWNER = 'The request was assigned as an expert and the request was approved';
    const HISTORY_DONE = 'The final file was uploaded';
    const HISTORY_UPLOAD_SETTLEMENT_FILE = 'The settlement payment receipt was uploaded';
    const HISTORY_SUCCESSFUL_SETTLEMENT = 'The final payment was successful';
    const HISTORY_FULL_DISCOUNT = 'No transaction was made due to the use of the 100% discount code';
    const HISTORY_AMOUNT_LESS_ZERO = 'The settling amount is less than or equal to zero, no transaction was made';
    const HISTORY_SATISFACTION = 'Customer satisfaction was recorded';
    const HISTORY_DISSATISFACTION = 'Declaration of dissatisfaction';
    const HISTORY_EXPERT_PAYMENT = 'Expert fee was allocated';

    const HISTORY_CHANGE_PRICE = 'Change request amount:';
    const HISTORY_CHANGE_PRICE_DOLLAR = 'Change dollar request amount:';
    const HISTORY_CHANGE_IS_DOLLAR = 'Change the payment system to dollars';
    const HISTORY_CHANGE_DEPOSIT_PAYMENT = 'Change in the amount of the deposit paid:';
    const HISTORY_CHANGE_FINAL_PAYMENT = 'Change the final amount paid:';
    const HISTORY_CHANGE_WORD_COUNT = 'Change the number of request words:';
    const HISTORY_CHANGE_SUPERVISOR = 'Change supervisor to ID:';
    const HISTORY_CHANGE_OWNER = 'Change expert to ID:';
    const HISTORY_CHANGE_STATUS = 'Change status to:';
    const HISTORY_CHANGE_TYPE = 'Change request type to:';

    public function request()
    {
        return $this->belongsTo(Request::class, 'request_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
