<?php

namespace RezaDadashi\Request\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use RezaDadashi\Discount\Http\Requests\DiscountRequest;
use RezaDadashi\Discount\Repositories\DiscountRepository;
use RezaDadashi\Discount\Services\DiscountService;
use RezaDadashi\Media\Models\Media;
use RezaDadashi\Payment\Models\Payment;
use RezaDadashi\Request\Services\RequestService;
use RezaDadashi\Setting\Models\Setting;
use RezaDadashi\Setting\Repositories\SettingRepository;
use RezaDadashi\User\Models\ExpertPayment;
use RezaDadashi\User\Models\User;

class Request extends Model
{
    use HasFactory;

    protected $guarded = [];

    const REQUEST_PERSIAN_TO_ENGLISH = 'persian to english';
    const REQUEST_ENGLISH_TO_PERSIAN = 'english to persian';
    const REQUEST_ENGLISH_EDITING = 'english editing';
    public static $requests = [
        self::REQUEST_PERSIAN_TO_ENGLISH,
        self::REQUEST_ENGLISH_TO_PERSIAN,
        self::REQUEST_ENGLISH_EDITING,
    ];


    const DIRECT_LINK_ACTION_SUPERVISOR = 'supervisor';
    const DIRECT_LINK_ACTION_OWNER = 'owner';
    const DIRECT_LINK_ACTION_USER_LOGIN = 'user-login';
    const DIRECT_LINK_ACTION_DOWNLOAD_CUSTOMER_FILE = 'download-customer-file';
    const DIRECT_LINK_ACTION_DOWNLOAD_FINAL_FILE = 'download-final-file';
    public static $directLinkActions = [
        self::DIRECT_LINK_ACTION_SUPERVISOR,
        self::DIRECT_LINK_ACTION_OWNER,
        self::DIRECT_LINK_ACTION_USER_LOGIN,
        self::DIRECT_LINK_ACTION_DOWNLOAD_CUSTOMER_FILE,
        self::DIRECT_LINK_ACTION_DOWNLOAD_FINAL_FILE,
    ];

    const STATUS_PENDING = 'pending'; // قبل از پرداخت توسط کاربر
    const STATUS_DEPOSIT = 'deposit'; // بعد از پرداخت بیعانه به صورت فایل یا آنلاین
    const STATUS_TAKE  = 'take'; // کارشناسان درخواست را take می کنند
    const STATUS_DONE  = 'done'; // کارشناس مربوطه درخواست را تکمیل کرده و فایل را بارگزاری می نماید
    const STATUS_SUPERVISOR_REJECT  = 'supervisor_reject'; // ناظر درخواست ترجمه را قبول نمیکند
    const STATUS_SUPERVISOR_ACCEPT  = 'supervisor_accept'; // ناظر درخواست ترجمه را قبول میکند
    const STATUS_SETTLEMENT  = 'settlement';  // کاربر مبلغ باقیمانده را (فایل رسید یا آنلاین) تسویه می کند
    const STATUS_COMEBACK  = 'comeback'; // درخواست از سمت کاربر به ناظر بازگردانی می شود
    const STATUS_CLOSED  = 'closed'; // درخواست پس از تکمیل پرداخت نهایی بسته می شود
    public static $statuses = [
        self::STATUS_PENDING,
        self::STATUS_DEPOSIT,
        self::STATUS_TAKE,
        self::STATUS_DONE,
        self::STATUS_SUPERVISOR_REJECT,
        self::STATUS_SUPERVISOR_ACCEPT,
        self::STATUS_SETTLEMENT,
        self::STATUS_COMEBACK,
        self::STATUS_CLOSED,
    ];

    protected $casts = [
        'word_count' => 'array'
    ];

    public function tableColor()
    {
        if ($this->type == self::REQUEST_PERSIAN_TO_ENGLISH) return 'badge py-2 bg-success';
        else if ($this->type == self::REQUEST_ENGLISH_TO_PERSIAN) return 'badge py-2 bg-primary';
        else if ($this->type == self::REQUEST_ENGLISH_EDITING) return 'badge text-dark py-2 bg-warning';
    }

    public function getStatus()
    {
        switch ($this->status) {
            case self::STATUS_PENDING:
                return "badge rounded-pill bg-warning text-dark";
                break;
            case self::STATUS_DEPOSIT:
                return "badge rounded-pill bg-warning";
                break;
            case self::STATUS_TAKE:
                return "badge rounded-pill bg-secondary";
                break;
            case self::STATUS_DONE:
                return "badge rounded-pill bg-primary";
                break;
            case self::STATUS_SUPERVISOR_ACCEPT:
                return "badge rounded-pill bg-success";
                break;
            case self::STATUS_SUPERVISOR_REJECT:
                return "badge rounded-pill bg-danger";
                break;
            case self::STATUS_SETTLEMENT:
                return "badge rounded-pill bg-info";
                break;
            case self::STATUS_COMEBACK:
                return "badge rounded-pill bg-danger";
                break;
            case self::STATUS_CLOSED:
                return "badge rounded-pill bg-success";
                break;
            default:
                return "badge rounded-pill bg-light";
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function supervisor()
    {
        return $this->belongsTo(User::class, 'supervisor_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function depositFile()
    {
        return $this->belongsTo(Media::class, 'deposit_media_id');
    }

    public function settlementFile()
    {
        return $this->belongsTo(Media::class, 'settlement_media_id');
    }

    public function file()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }

    public function deliveryFile()
    {
        return $this->belongsTo(Media::class, 'delivery_media_id');
    }

    public function supervisorFile()
    {
        return $this->belongsTo(Media::class, 'supervisor_media_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class)->orderBy('id', 'desc');
    }

    public function histories()
    {
        return $this->hasMany(History::class, 'request_id')->orderBy('id', 'desc');
    }

    public function downloadLink()
    {
        if ($this->media_id)
            return '/uploads/word_file/'. $this->file->files['doc'];
    }

    public function downloadLinkDelivery()
    {
        if ($this->delivery_media_id)
            return '/uploads/word_file/'. $this->deliveryFile->files['doc'];
    }

    public function downloadLinkSupervisorFile()
    {
        if ($this->supervisorFile)
            return '/uploads/word_file/'. $this->supervisorFile->files['doc'];
    }

    public function downloadLinkDeposit()
    {
        if ($this->deposit_media_id)
            return '/uploads/receipt/'. $this->depositFile->files['original'];
    }

    public function downloadLinkSettlement()
    {
        if ($this->settlement_media_id)
            return '/uploads/receipt/'. $this->settlementFile->files['original'];
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDollarPrice()
    {
        return $this->price_dollar;
    }

    public function getFinalDepositAmount($code = null)
    {
        $amount = $this->getPrice();
        $percent = (new SettingRepository())->getByKey(Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->value;
        $finalAmount = RequestService::calculationPercentAmount($amount, $percent);

        if ($code) {
            $repo = new DiscountRepository();
            $discount = $repo->getByDiscountCode($code);
            if ($discount) {
                $finalAmount = $finalAmount - DiscountService::calculateDiscountAmount($finalAmount, $discount->percent);
            }
        }
        return $finalAmount;
    }

    public function getFinalDepositAmountDollar($code = null)
    {
        $amount = $this->getDollarPrice();
        $percent = (new SettingRepository())->getByKey(Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->value;
        $finalAmount = RequestService::calculationPercentAmount($amount, $percent);

        if ($code) {
            $repo = new DiscountRepository();
            $discount = $repo->getByDiscountCode($code);
            if ($discount) {
                $finalAmount = $finalAmount - DiscountService::calculateDiscountAmount($finalAmount, $discount->percent);
            }
        }
        return $finalAmount;
    }

    public function getFormattedPrice()
    {
        return number_format($this->price);
    }

    public function getFormattedPriceDollar()
    {
        return number_format($this->price_dollar);
    }

    public function getPercentPrice()
    {
        $percent = (new SettingRepository())->getByKey(Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->value;
        return RequestService::calculationPercentAmount($this->getPrice(), $percent);
    }

    public function getPercentPriceDollar()
    {
        $percent = (new SettingRepository())->getByKey(Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->value;
        return RequestService::calculationPercentAmount($this->getDollarPrice(), $percent);
    }

    public function getFormattedPercent()
    {
        return number_format($this->getPercentPrice());
    }
    public function getFormattedPercentDollar()
    {
        return number_format($this->getPercentPriceDollar());
    }

    public function getDepositAmount()
    {
        return $this->deposit_payment;
    }

    public function getFormattedDepositAmount()
    {
        return number_format($this->getDepositAmount());
    }

    public function getFinalAmount()
    {
        return $this->final_payment;
    }

    public function getFormattedFinalAmount()
    {
        return number_format($this->getFinalAmount());
    }

    public function getSettlementPayable()
    {
        return $this->getPrice() - $this->getDepositAmount();
    }

    public function getSettlementPayableDollar()
    {
        return $this->price_dollar - $this->getDepositAmount();
    }

    public function getFormattedSettlementPayable()
    {
        return number_format($this->getSettlementPayable());
    }

    public function getFormattedSettlementPayableDollar()
    {
        return number_format($this->getSettlementPayableDollar());
    }

    public function expertPayment()
    {
        return $this->hasOne(ExpertPayment::class);
    }
}
