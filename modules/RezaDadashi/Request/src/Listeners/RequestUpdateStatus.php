<?php

namespace RezaDadashi\Request\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use RezaDadashi\Discount\Repositories\DiscountRepository;
use RezaDadashi\Payment\Models\Payment;
use RezaDadashi\Payment\Repositories\PaymentRepository;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Notifications\RequestCreatedNotification;
use RezaDadashi\Request\Repositories\RequestRepository;
use RezaDadashi\User\Notifications\VerifySMSNotification;
use RezaDadashi\User\Repositories\UserRepository;

class RequestUpdateStatus
{
    private $discountRepository;
    public function __construct(DiscountRepository $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $requestRepository = resolve(RequestRepository::class);
        $request = $requestRepository->find($event->payment->request_id);
        $userRepository = resolve(UserRepository::class);

        if ($event->payment->discount) {
            $this->discountRepository->used($event->payment->discount);
        }
        if ($event->payment->type == Payment::TYPE_DEPOSIT) {
            $request->deposit_payment = $event->payment->amount;
            $request->save();

            /** Send Verify Code & Password Notification To User */
            if (! auth()->user()->hasVerifiedEmail()) {
                auth()->user()->notify(new VerifySMSNotification());
            }
            /** Send Verify Code & Password Notification To User */


            /** Send Notification To Experts */
            $notifiableUsers = collect();
            if ($request->type == Request::REQUEST_PERSIAN_TO_ENGLISH || $request->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
                $notifiableUsers = $userRepository->getAllTranslators();
            } else {
                $notifiableUsers = $userRepository->getAllEditors();
            }
            Notification::send($notifiableUsers, (new RequestCreatedNotification($request)));


            $requestRepository->changeStatus($request->id, Request::STATUS_DEPOSIT);
        } else {
            $request->final_payment = $event->payment->amount;
            $request->save();
            $requestRepository->changeStatus($request->id, Request::STATUS_SETTLEMENT);
        }


    }
}
