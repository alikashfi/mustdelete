<?php

namespace RezaDadashi\Request\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use RezaDadashi\Request\Channels\ExpertPaymentSmsChannel;
use RezaDadashi\Request\Channels\RequestTakedSmsChannel;
use RezaDadashi\Request\Models\Request;

class ExpertPaymentNotification extends Notification
{
    use Queueable;

    private $totalCount;
    private $totalAmount;

    /**
     * Create a new notification instance.
     *
     * @param $totalCount
     * @param $totalAmount
     */
    public function __construct($totalCount, $totalAmount)
    {
        $this->totalCount = $totalCount;
        $this->totalAmount = $totalAmount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ExpertPaymentSmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return [
            'count'  => $this->totalCount,
            'amount'  => $this->totalAmount,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
