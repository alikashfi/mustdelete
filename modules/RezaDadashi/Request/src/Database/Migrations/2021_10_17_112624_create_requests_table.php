<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->enum('type', \RezaDadashi\Request\Models\Request::$requests);
            $table->json('word_count');
            $table->foreignId('user_id');
            $table->foreignId('deposit_media_id')->nullable();
            $table->foreignId('settlement_media_id')->nullable();
            $table->foreignId('media_id')->nullable();
            $table->foreignId('delivery_media_id')->nullable();
            $table->foreignId('supervisor_media_id')->nullable(); //فایلی که ناظر برای کارشناس بارگذاری می کند
            $table->text('supervisor_description')->nullable(); //توضیحاتی که ناظر برای کارشناس بارگذاری می کند
            $table->string('price', 10);
            $table->tinyInteger('is_dollar')->nullable();
            $table->string('price_dollar', 10);
            $table->string('deposit_payment', 10)->default(0);
            $table->string('final_payment', 10)->default(0);
            $table->foreignId('supervisor_id')->nullable();
            $table->foreignId('owner_id')->nullable();
            $table->enum('status', \RezaDadashi\Request\Models\Request::$statuses)
                ->default(\RezaDadashi\Request\Models\Request::STATUS_PENDING);
            $table->timestamps();

            // اگر فایل به هردلیلی حذف شد، ستون deposit_media_id در این جدول null می شود.
            $table->foreign('deposit_media_id')->references('id')->on('media')->onDelete('SET NULL');

            // اگر فایل به هردلیلی حذف شد، ستون settlement_media_id در این جدول null می شود.
            $table->foreign('settlement_media_id')->references('id')->on('media')->onDelete('SET NULL');

            // اگر فایل به هردلیلی حذف شد، ستون media_id در این جدول null می شود.
            $table->foreign('media_id')->references('id')->on('media')->onDelete('SET NULL');

            // اگر فایل به هردلیلی حذف شد، ستون delivery_media_id در این جدول null می شود.
            $table->foreign('delivery_media_id')->references('id')->on('media')->onDelete('SET NULL');

            // اگر فایل به هردلیلی حذف شد، ستون supervisor_media_id در این جدول null می شود.
            $table->foreign('supervisor_media_id')->references('id')->on('media')->onDelete('SET NULL');

            // اگر کاربر(مشتری) از جدول کاربران حذف شد، این درخواست هم حذف می شود.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            // اگر ناظر از جدول کاربران حذف شد، این ستون null می شود.
            $table->foreign('supervisor_id')->references('id')->on('users')->onDelete('SET NULL');

            // اگر انجام دهنده درخواست از جدول کاربران حذف شد، این ستون null می شود.
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
