<?php

namespace RezaDadashi\Request\Rules;

use Illuminate\Contracts\Validation\Rule;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Repositories\UserRepository;

class ValidOwner implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = (new UserRepository())->findOrFail($value);
        return $user->isExpert() == true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'این کاربر دسترسی انجام درخواست را ندارد';
    }
}
