<?php

namespace RezaDadashi\Request\Rules;

use Illuminate\Contracts\Validation\Rule;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Repositories\RequestRepository;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Repositories\UserRepository;

class ValidOwnerByRequest implements Rule
{
    private $requestId;
    public function __construct($requestId)
    {
        $this->requestId = $requestId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = (new UserRepository())->findOrFail($value);
        $requestRepository = (new RequestRepository());
        $request = $requestRepository->findOrFail($this->requestId);
        $ownerPermission = $requestRepository->getPermissionByRequestType($request);

        return $user->hasPermissionTo($ownerPermission) == true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'این کاربر دسترسی برای انجام این درخواست را ندارد';
    }
}
