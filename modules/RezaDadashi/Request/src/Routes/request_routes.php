<?php

use Illuminate\Support\Facades\Route;


Route::group([
    'middleware' => ['web'],
    'prefix' => '/request/direct'
], function ($router) {

    Route::get('/{request_id}/{user_token}/{type}', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'directLink'])
        ->name('requests.directLink');

});


Route::group([
    'middleware' => ['web', 'auth'],
    'prefix' => '/requests'
], function ($router) {

    //request after register
    Route::get('/new', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'showNewRequestForm'])
        ->name('requests.newRequest');

    Route::post('/new', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'store'])
        ->name('requests.store');

    Route::get('/{request}/deposit', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'showDepositForm'])
        ->name('requests.deposit');

    Route::post('/{request}/deposit', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'deposit'])
        ->name('requests.deposit');

    Route::post('/{request}/uploadPaymentReceipt', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'uploadReceipt'])
        ->name('requests.uploadReceipt');

    Route::get('/{request}/settlement', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'showSettlementForm'])
        ->name('requests.showSettlementForm');

    Route::post('/{request}/settlement', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'settlement'])
        ->name('requests.settlement');

    Route::get('/{request}', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'show'])
        ->middleware('verified')
        ->name('requests.show');

    Route::post('/{request}/supervisor-note', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'supervisorNote'])
        ->name('requests.supervisorNote');

    Route::patch('/{request}/setSupervisor', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'setSupervisor'])
        ->middleware('verified')
        ->name('requests.setSupervisor');


    Route::patch('/{request}/setOwner', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'setOwner'])
        ->middleware('verified')
        ->name('requests.setOwner');


    Route::post('/{request}/update', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'update'])
        ->middleware('verified')
        ->name('requests.update');

    Route::post('/{request}/updateDeliveryFile', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'updateDeliveryFile'])
        ->middleware('verified')
        ->name('requests.deliveryFile');

    Route::post('/{request}/satisfaction', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'satisfaction'])
        ->middleware('verified')
        ->name('requests.satisfaction');

    Route::get('/{request}/factor', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'factor'])
        ->middleware('verified')
        ->name('requests.factor');

    Route::post('/{request}/changeWordCount', [\RezaDadashi\Request\Http\Controllers\RequestController::class, 'changeUserEnteredWordCount'])
        ->name('requests.changeUserEnteredWordCount');


    Route::get('/', [
        'uses' => 'RezaDadashi\Request\Http\Controllers\RequestController@index',
        'as' => 'requests.index',
        'middleware' => ['verified']
    ]);
});




Route::group([
    'middleware' => ['web', 'auth'],
], function ($router) {

    Route::get('/my-requests', [
        'uses' => 'RezaDadashi\Request\Http\Controllers\RequestController@my',
        'as' => 'requests.my',
        'middleware' => ['verified']
    ]);

});



Route::group([
    'middleware' => ['web', 'auth', 'verified'],
    'prefix' => '/reports'
], function () {

    Route::get('/requests', [
        'uses' => 'RezaDadashi\Request\Http\Controllers\ReportController@requestsReport',
        'as' => 'reports.request',
    ]);

    Route::post('/exportExcel', [\RezaDadashi\Request\Http\Controllers\ReportController::class, 'exportExcel'])
        ->name('reports.exportExcel');


});


