@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('requests.index') }}">@lang('show_request_requests_title')</a></li>
    <li class="breadcrumb-item"><a href="#">@lang('show_request_requests_view')</a></li>
@endsection

@section('content')
    <div class="row">

        @if(
            (auth()->id() == $request->supervisor_id ||
             auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_REQUESTS))
         )
            @include('Request::Admin.layouts.manage')
        @endif

        <div class="col-md-12 col-lg-12">
            <div class="p-3 card">

                <div class="d-flex align-items-center justify-content-between pb-2">
                    <div>
                        <span class="bi bi-list"></span>
                        <span> @lang('show_request_requests_view') ({{ $request->id }})</span>
                    </div>
                    <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                       data-bs-target="#groups-toggle" aria-expanded="false"></i>
                </div>
                <div class="collapse show border-top" id="groups-toggle">

                    @include('Request::Admin.layouts.info')
                    @include('Request::Admin.layouts.supervisor-expert')
                    @include('Request::Admin.layouts.wave')
                    @include('Request::Admin.layouts.payments')

                </div>

            </div>
        </div>

        @if(auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_REQUESTS) || auth()->id() == $request->supervisor_id)
            @include('Request::Admin.layouts.history')
        @endif

    </div>
@endsection
