@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('requests.index') }}">@lang('requests_list_of_customer_requests')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('requests_list_of_customer_requests')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="dropdown bg-white border border-2 col-lg-4 my-2">
                            <div class="dropdown-toggle d-flex align-items-center justify-content-between p-2" data-bs-toggle="dropdown">@lang('requests_search_title')</div>
                            <div class="dropdown-menu col-4 w-100 mt-1 p-3 shadow">
                                <form method="GET">
                                    <input type="number" name="id" value="{{ request('id') }}" placeholder="@lang('request_id')" class="form-control mb-3">
                                    <input type="number" name="mobile" value="{{ request('mobile') }}" placeholder="@lang('common_mobile_number')" class="form-control mb-3">
                                    <input type="email" name="email" value="{{ request('email') }}" placeholder="@lang('common_email')" class="form-control mb-3">
                                    <input type="text" name="supervisor" value="{{ request('supervisor') }}" placeholder="@lang('request_supervisor')" class="form-control mb-3">
                                    <input type="text" name="expert" value="{{ request('expert') }}" placeholder="@lang('request_expert')" class="form-control mb-3">

                                    <select name="type" class="form-select mb-3">
                                        <option value="">-- @lang('request_type') --</option>
                                        @foreach(\RezaDadashi\Request\Models\Request::$requests as $requestType)
                                            <option
                                                @if(request('type') == $requestType) selected @endif
                                            value="{{ $requestType }}">
                                                @lang($requestType)
                                            </option>
                                        @endforeach
                                    </select>

                                    <select name="status" class="form-select mb-3">
                                        <option value="">-- @lang('request_status') --</option>
                                        @foreach(\RezaDadashi\Request\Models\Request::$statuses as $requestStatus)
                                            <option
                                                @if(request('status') == $requestStatus) selected @endif
                                            value="{{ $requestStatus }}">
                                                @lang($requestStatus)
                                            </option>
                                        @endforeach
                                    </select>

                                    <div class="text-end">
                                        <button class="btn btn-primary" type="submit">
                                            <span>@lang('common_search')</span>
                                            <i class="bi bi-search text-white"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead class="">
                                <tr>
                                    <th scope="col">@lang('request_id')</th>
                                    <th scope="col">@lang('request_user')</th>
                                    <th scope="col">@lang('request_created_at')</th>
                                    <th scope="col">@lang('request_type')</th>
                                    <th scope="col">@lang('request_supervisor')</th>
                                    <th scope="col">@lang('request_status')</th>
                                    <th scope="col">@lang('common_actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr class="">
                                        <td>{{ $request->id }}</td>
                                        <td>
                                            @if(auth()->user()->hasPermissionTo(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPER_ADMIN))
                                                {{ $request->user->name }}
                                            @endif
                                        </td>
                                        <td>{{ getJalali($request->created_at) }}</td>
                                        <td><span
                                                class="{{ $request->tableColor() }}">@lang($request->type)</span></td>
                                        <td>{{ $request->supervisor ? $request->supervisor->name : 'نامشخص' }}</td>
                                        <td><span
                                                class="{{ $request->getStatus() }}">@lang($request->status)</span></td>
                                        <td>
                                            <a class="text-decoration-none"
                                               href="{{ route('requests.show', $request->id) }}">@lang('request_view')</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center d-flex justify-content-center"> {{ $requests->links('pagination::bootstrap-4') }} </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
