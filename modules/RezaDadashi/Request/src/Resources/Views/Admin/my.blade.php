@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('requests.index') }}">@lang('my_request_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('my_request_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                           data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">

                        <div class="d-flex justify-content-between flex-column flex-lg-row">
                            <div class="dropdown bg-white border border-2 col-lg-4 my-2">
                                <div class="dropdown-toggle d-flex align-items-center justify-content-between p-2" data-bs-toggle="dropdown">@lang('my_request_search_title')</div>
                                <div class="dropdown-menu col-4 w-100 mt-1 p-3 shadow">
                                    <form method="GET">
                                        <input type="number" name="id" value="{{ request('id') }}" placeholder="@lang('request_id')" class="form-control mb-3">
                                        <input type="text" name="supervisor" value="{{ request('supervisor') }}" placeholder="@lang('request_supervisor')" class="form-control mb-3">
                                        <input type="text" name="expert" value="{{ request('expert') }}" placeholder="@lang('request_expert')" class="form-control mb-3">

                                        <select name="type" class="form-select mb-3">
                                            <option value="">-- @lang('request_type') --</option>
                                            @foreach(\RezaDadashi\Request\Models\Request::$requests as $requestType)
                                                <option
                                                    @if(request('type') == $requestType) selected @endif
                                                value="{{ $requestType }}">
                                                    @lang($requestType)
                                                </option>
                                            @endforeach
                                        </select>

                                        <div class="text-end">
                                            <button class="btn btn-primary" type="submit">
                                                <span>@lang('common_search')</span>
                                                <i class="bi bi-search text-white"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="text-end"><a class="btn btn-success my-2" href="{{ route('requests.newRequest') }}">@lang('my_request_new_request')</a></div>
                        </div>

                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead class="">
                                <tr>
                                    <th scope="col">@lang('request_id')</th>
                                    <th scope="col">@lang('request_user')</th>
                                    <th scope="col">@lang('request_created_at')</th>
                                    <th scope="col">@lang('request_type')</th>
                                    <th scope="col">@lang('request_status')</th>
                                    <th scope="col">@lang('common_actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr class="">
                                        <td scope="row">{{ $request->id }}</td>
                                        <td scope="row">{{ $request->user->name }}</td>
                                        <td scope="row">{{ getJalali($request->created_at) }}</td>
                                        <td scope="row"><span
                                                class="{{ $request->tableColor() }}">@lang($request->type)</span></td>
                                        <td scope="row"><span
                                                class="{{ $request->getStatus() }}">@lang($request->status)</span></td>
                                        <td>
                                            <a class="text-decoration-none"
                                               href="{{ route('requests.show', $request->id) }}">@lang('request_view')</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center d-flex justify-content-center"> {{ $requests->links('pagination::bootstrap-4') }} </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
