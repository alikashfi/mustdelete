@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('reports.request') }}">@lang('reports_requests_title')</a>
    </li>
@endsection



@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-12">

            <div class="p-3 card">

                <form class="row justify-content-center" action="{{ route('reports.exportExcel') }}" method="post">

                    @csrf

                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-text">
                                <span class="bi bi-calendar3"></span>
                            </span>
                            <input id="start_date" name="start_date" type="text"
                                   class="form-control" placeholder="@lang('reports_requests_start_date')">
                            <span class="input-group-text">
                                <span class="bi bi-calendar3"></span>
                            </span>
                            <input id="end_date" name="end_date" type="text"
                                   class="form-control" placeholder="@lang('reports_requests_end_date')">
                        </div>
                    </div>

                    <div class="col-auto mt-3 mt-lg-0">
                        <button type="submit" class="btn btn-primary">@lang('reports_requests_button')</button>
                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection


@section("js")
    <script src="{{ asset('/assets/persianDatePicker/js/persianDatepicker.min.js') }}"></script>

    <script>
        $("#start_date").persianDatepicker({
            formatDate: "YYYY/0M/0D"
        });
        $("#end_date").persianDatepicker({
            formatDate: "YYYY/0M/0D"
        });
    </script>
@endsection

@section("css")
    <link rel="stylesheet" href="{{ asset('/assets/persianDatePicker/css/persianDatepicker-default.css') }}">
@endsection
