<!doctype html>
<html dir="{{ App::getLocale() == 'fa' ? 'rtl' : 'ltr' }}">
@include('Dashboard::layouts.head')
<body class="min-vh-100 d-flex justify-content-center align-items-center _bg-white">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 bg-white border rounded-2">

                <div class="row justify-content-center align-items-center p-3">
                    <div class="col-6 text-start">
                        <img width="50" src="{{ asset('/assets/img/logo.png') }}" alt="">
                        <div class="fw-bold">@lang('app_name')</div>
                    </div>
                    <div class="col-6 col-lg-6 text-center">
                        <div>
                            <span>@lang('request_created_at') :</span>
                            <span class="fw-bold">{{ jdate($request->created_at)->format('Y/m/d') }}</span>
                        </div>
                        <div>
                            <span>@lang('factor_request_id') :</span>
                            <span class="fw-bold">{{ $request->id }}</span>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="border border-warning border-3"></div>
                </div>
                <div class="row my-2">
                    <div class="bg-light border p-2 text-center">@lang('factor_title')</div>
                </div>
                <div class="row my-4 bg-light border p-2">
                    <div class="col-lg-6">
                        <div class="d-flex justify-content-center align-items-center">
                            <i class="bi bi-person-circle fs-5 mx-2"></i>
                            <span>@lang('factor_customer_name') :</span>
                            <span>{{ $request->user->name }}</span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="d-flex justify-content-center align-items-center">
                            <i class="bi bi-telephone-fill fs-5 mx-2"></i>
                            <span>@lang('common_mobile_number') :</span>
                            <span>{{ $request->user->mobile }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <table class="table table-bordered mt-2">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">@lang('factor_paid_amount')</th>
                            <th scope="col">@lang('factor_paid_type')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>
                                {{ $request->getFormattedDepositAmount() }}
                                @if($request->is_dollar)
                                    $
                                @else
                                    @lang('Toman')
                                @endif
                            </td>
                            <td>@lang('factor_paid_deposit')</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>
                                {{ $request->getFormattedFinalAmount() }}
                                @if($request->is_dollar)
                                    $
                                @else
                                    @lang('Toman')
                                @endif
                            </td>
                            <td>@lang('factor_paid_settlement')</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <table class="table table-bordered mt-3">
                            <tr>
                                <th scope="row">@lang('factor_total_amount') :</th>
                                <td>

                                    @if($request->is_dollar)
                                        {{ $request->getFormattedPriceDollar() }} $
                                    @else
                                        @lang('Toman') {{ $request->getFormattedPrice() }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">@lang('factor_total_amount_paid') :</th>
                                <td>
                                    {{ number_format($request->deposit_payment + $request->final_payment) }}
                                    @if($request->is_dollar)
                                        $
                                    @else
                                        @lang('Toman')
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-lg-12 text-center">
                        <button onclick="window.print();" type="button" class="btn btn-primary">@lang('factor_print_title')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
