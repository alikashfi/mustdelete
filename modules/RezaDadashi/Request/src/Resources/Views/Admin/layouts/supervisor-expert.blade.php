<div class="p-3 row">

    <div class="col-md-12 my-2">
        <div class="text-end">
            <div class="d-flex flex-column flex-lg-row justify-content-center align-items-center">
                @if ($request->user_id == auth()->id() && $request->status == \RezaDadashi\Request\Models\Request::STATUS_PENDING)
                    <a href="{{ route('requests.deposit', $request->id) }}" class="btn btn-primary me-1 my-1">@lang('show_request_payment_of_deposit')</a>
                @endif
                @if (
                    auth()->user()->hasPermissionTo(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPERVISOR) &&
                    $request->status == \RezaDadashi\Request\Models\Request::STATUS_DONE)
                    <form action="{{ route('requests.setSupervisor', $request->id) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="status" value="1">
                        <button class="btn btn-success me-1 my-1" type="submit">@lang('show_request_accept_request_as_an_supervisor')</button>
                    </form>
                @endif
                @if (
                     $request->status == \RezaDadashi\Request\Models\Request::STATUS_DEPOSIT &&
                     auth()->user()->hasPermissionTo((new \RezaDadashi\Request\Repositories\RequestRepository())->getPermissionByRequestType($request)))
                    <form action="{{ route('requests.setOwner', $request->id) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <button class="btn btn-secondary me-1 my-1" type="submit">@lang('show_request_take_request_as_a_expert')</button>
                    </form>
                @endif
            </div>
        </div>
    </div>

    @if(
     ($request->status == \RezaDadashi\Request\Models\Request::STATUS_TAKE || $request->status == \RezaDadashi\Request\Models\Request::STATUS_SUPERVISOR_REJECT) &&
     (auth()->id() == $request->owner_id ||
     auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_REQUESTS))
     )
        <div class="col-lg-12 d-flex">
            <form class="w-100" action="{{ route('requests.deliveryFile', $request->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="text-center">
                    <label class="btn btn-upload-file" for="final-media">@lang('btn_upload_file')</label>
                    <button class="btn btn-success" type="submit">@lang('common_upload')</button>
                </div>
                <x-input class="d-none file-upload" name="file" type="file" id="final-media" accept=".doc, .docx" required />
                <div class="file-upload-preview d-none mt-1 d-flex align-items-center justify-content-center">
                    <i class="bi bi-file-check fs-5"></i>
                    <span style="font-size: 10px;" id="filename"></span>
                </div>
            </form>
        </div>
    @endif

    @if(auth()->id() == $request->user_id && $request->status == \RezaDadashi\Request\Models\Request::STATUS_SUPERVISOR_ACCEPT)
        <div class="col-lg-12 d-flex justify-content-center my-2">
            <a class="btn btn-primary" href="{{ route('requests.showSettlementForm', $request->id) }}">@lang('show_request_payment_of_final')</a>
        </div>
    @endif

    @if(auth()->id() == $request->user_id && $request->status == \RezaDadashi\Request\Models\Request::STATUS_SETTLEMENT)
        <form action="{{ route('requests.satisfaction', $request->id) }}" method="post">
            @csrf
            <div class="d-flex justify-content-center align-items-center my-3 border border-1 rounded-2 py-3">
                <div class="form-check mx-3">
                    <input class="form-check-input @error('satisfaction') is-invalid @enderror" type="radio" name="satisfaction" value="1" id="satisfaction-yes">
                    <label class="form-check-label text-success cursor-pointer" for="satisfaction-yes">
                        <i class="bi bi-check2"></i>
                        <span>@lang('show_request_satisfaction_and_end_registration')</span>
                    </label>
                    <x-validation-error field="satisfaction" />
                </div>
                <div class="form-check mx-3">
                    <input class="form-check-input @error('satisfaction') is-invalid @enderror" type="radio" name="satisfaction" value="0" id="satisfaction-no">
                    <label class="form-check-label text-danger cursor-pointer" for="satisfaction-no">
                        <i class="bi bi-x-lg"></i>
                        <span>@lang('show_request_express_dissatisfaction_and_follow_up')</span>
                    </label>
                    <x-validation-error field="satisfaction" />
                </div>

                <button class="btn btn-success" type="submit">@lang('common_record')</button>
            </div>
        </form>
    @endif

</div>
