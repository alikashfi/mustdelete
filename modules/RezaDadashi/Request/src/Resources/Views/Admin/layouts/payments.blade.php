@if(auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPERVISOR) || auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_REQUESTS))

    @if($request->payments)
        <div class="p-3 row mt-4">
        <div class="">@lang('show_request_request_payment_list')</div>
        <div class="table-responsive pt-2">
            <table class="table align-middle">
                <thead class="table-warning">
                <tr>
                    <th scope="col">@lang('common_id')</th>
                    <th scope="col">@lang('common_name')</th>
                    <th scope="col">@lang('payments_price')</th>
                    <th scope="col">@lang('payments_type')</th>
                    <th scope="col">@lang('payments_date')</th>
                    <th scope="col">@lang('register_step_3_promo_code')</th>
                    <th scope="col">@lang('payments_status')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($request->payments as $payment)
                    <tr class="{{ $payment->getTableCss() }}">
                        <th scope="row">{{ $payment->id }}</th>
                        <td>{{ $payment->user->name }}</td>
                        <td>{{ number_format($payment->amount) }}</td>
                        <td>@lang("payment_type_" . $payment->type)</td>
                        <td>{{ getJalali($payment->created_at) }}</td>
                        <td>
                            @if($payment->discount)
                                <span class="badge bg-primary rounded-pill">{{ $payment->discount->code }}</span>
                                <span class="badge bg-danger rounded-pill">{{ $payment->discount->percent }} %</span>
                            @endif
                        </td>
                        <td>@lang("payment_status_" . $payment->status)</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif


    @if($request->depositFile || $request->settlementFile)
        <div class="p-3 row mt-1">
        <div class="">@lang('show_request_receipts_payment_receipts')</div>
        <div class="table-responsive pt-2">
            <table class="table align-middle">
                <thead class="table-info">
                <tr>
                    <th scope="col">@lang('show_request_receipts_id_received')</th>
                    <th scope="col">@lang('common_name')</th>
                    <th scope="col">@lang('common_file')</th>
                    <th scope="col">@lang('show_request_receipts_created_at')</th>
                    <th scope="col">@lang('show_request_receipts_step')</th>
                </tr>
                </thead>
                <tbody>
                    @if($request->depositFile)
                        <tr>
                            <td scope="col">{{ $request->depositFile->id }}</td>
                            <td scope="col">{{ $request->depositFile->user->name }}</td>
                            <td scope="col">
                                <a href="{{ $request->downloadLinkDeposit() }}">{{ $request->depositFile->filename }}</a>
                            </td>
                            <td>{{ getJalali($request->depositFile->created_at) }}</td>
                            <td>@lang('show_request_receipts_deposit')</td>
                        </tr>
                    @endif
                    @if($request->settlementFile)
                        <tr>
                            <td scope="col">{{ $request->settlementFile->id }}</td>
                            <td scope="col">{{ $request->settlementFile->user->name }}</td>
                            <td scope="col">
                                <a href="{{ $request->downloadLinkSettlement() }}">{{ $request->settlementFile->filename }}</a>
                            </td>
                            <td>{{ getJalali($request->settlementFile->created_at) }}</td>
                            <td>@lang('show_request_receipts_settlement')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @endif

@endif
