<div class="row mt-4">
    <div class="col-lg-4 text-center">
        <div
            class="d-flex justify-content-center align-items-center flex-lg-row flex-column my-2">
            <span class="badge bg-secondary border rounded-pill m-2">@lang('request_type')</span>
            <span
                class="text-decoration-none d-flex justify-content-center align-items-center">
                <span class="badge bg-light border text-dark">@lang($request->type)</span>
            </span>
        </div>
        <div>
            <span class="badge bg-dark text-white rounded-pill m-2">@lang('show_request_number_words_entered_by_customer')</span>
            <i class="bi bi-grid-3x3-gap text-danger"></i>
            <span>{{ number_format($request->word_count['user']) }}</span>
        </div>
        <div>
            <span class="badge bg-dark text-white rounded-pill m-2">@lang('show_request_number_system_recognition_words')</span>
            <i class="bi bi-grid-3x3-gap text-danger"></i>
            <span>{{ number_format($request->word_count['system']) }}</span>
        </div>
        <div>
            <span class="badge bg-dark text-white rounded-pill m-2">@lang('show_request_number_words_entered_by_supervisor')</span>
            <i class="bi bi-grid-3x3-gap text-danger"></i>
            <span>{{ number_format($request->word_count['supervisor']) }}</span>
        </div>
        @if(
            $request->supervisor_description &&
            (
                $request->status == \RezaDadashi\Request\Models\Request::STATUS_DONE ||
                $request->status == \RezaDadashi\Request\Models\Request::STATUS_SUPERVISOR_REJECT
            ) &&
            (
                auth()->id() == $request->owner_id ||
                auth()->id() == $request->supervisor_id ||
                auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPERVISOR)
            )
        )
            <div>
                <span class="badge bg-danger text-white rounded-pill m-2">@lang('show_request_supervisor_description')</span>
                <span class="fs-12 d-block">{{ $request->supervisor_description }}</span>
            </div>
        @endif
    </div>
    <div class="col-lg-4 text-center">
        <div
            class="d-flex justify-content-center align-items-center flex-lg-row flex-column my-2">
            <i class="bi bi-calendar3 text-primary fs-4 fw-bold"></i>
            <div class="px-2">@lang('request_created_at')</div>
            <div>{{ getJalali($request->created_at) }}</div>
        </div>
        <div
            class="d-flex justify-content-center align-items-center flex-lg-row flex-column my-2">
            <i class="bi bi-bookmark-fill text-warning fs-4 fw-bold"></i>
            <div class="px-2">@lang('request_status')</div>
            <span
                class="{{ $request->getStatus() }}">@lang($request->status)</span>
        </div>
        <div>
            <a href="{{ $request->downloadLink() }}"
               class="text-decoration-none d-flex justify-content-center align-items-center mb-2">
                <i class="bi bi-link-45deg fs-3 d-flex"></i>
                {{ $request->type == 'persian to english' ? __('download persian text') : __('download english text')}}
            </a>
        </div>
        @if($request->delivery_media_id &&
            ($request->status == \RezaDadashi\Request\Models\Request::STATUS_SETTLEMENT ||
            $request->status == \RezaDadashi\Request\Models\Request::STATUS_COMEBACK ||
            $request->status == \RezaDadashi\Request\Models\Request::STATUS_CLOSED ||
            auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPERVISOR))
        )
            <div>
                <a href="{{ $request->downloadLinkDelivery() }}"
                   class="text-decoration-none d-flex justify-content-center align-items-center">
                    <i class="bi bi-link-45deg fs-3 d-flex"></i>
                    @lang('show_request_owner_file_download_link')
                </a>
            </div>
        @endif

        @if($request->supervisorFile &&
            (
                $request->status == \RezaDadashi\Request\Models\Request::STATUS_DONE ||
                $request->status == \RezaDadashi\Request\Models\Request::STATUS_SUPERVISOR_REJECT
            ) &&
            (
                auth()->id() == $request->owner_id ||
                auth()->id() == $request->supervisor_id ||
                auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPERVISOR)
            )
        )

            <div
                class="d-flex justify-content-center align-items-center flex-lg-row flex-column my-2">
                <span class="badge bg-danger text-white border rounded-pill m-2">@lang('show_request_supervisor_file')</span>
                <a href="{{ $request->downloadLinkSupervisorFile() }}"
                   class="text-decoration-none d-flex justify-content-center align-items-center">
                    <i class="bi bi-link-45deg fs-3 d-flex"></i>
                    @lang('common_download')
                </a>
            </div>

        @endif

    </div>

    <div class="col-lg-4 text-center">

{{--        @if($request->supervisor_id &&--}}
{{--            auth()->id() == $request->user_id ||--}}
{{--            auth()->id() == $request->supervisor_id || auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_REQUESTS)--}}
{{--         )--}}
{{--            <a href="{{ route('comments.link', [$request->id, \RezaDadashi\Comment\Models\Comment::COMMENT_SUPERVISOR_CUSTOMER]) }}" class="btn btn-success my-1">--}}
{{--                <i class="bi bi-chat-dots-fill"></i>--}}
{{--                <span>@lang('show_request_comment_customer_supervisor')</span>--}}
{{--            </a>--}}
{{--        @endif--}}

{{--        @if($request->supervisor_id &&--}}
{{--            $request->owner_id &&--}}
{{--            auth()->id() == $request->supervisor_id ||--}}
{{--            auth()->id() == $request->owner_id || auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_MANAGE_REQUESTS)--}}
{{--          )--}}
{{--            <a href="{{ route('comments.link', [$request->id, \RezaDadashi\Comment\Models\Comment::COMMENT_SUPERVISOR_EXPERT]) }}" class="btn btn-danger my-1">--}}
{{--                <i class="bi bi-chat-dots-fill"></i>--}}
{{--                <span>@lang('show_request_comment_supervisor_expert')</span>--}}
{{--            </a>--}}
{{--        @endif--}}



        @if($request->status == \RezaDadashi\Request\Models\Request::STATUS_CLOSED)
            <hr>
            <button onclick="window.open('{{ route('requests.factor', $request->id) }}', 'mypopup', 'width=1200,height=700');" type="button" class="btn btn-warning">@lang('show_request_factor')</button>
        @endif

        @if($request->status == \RezaDadashi\Request\Models\Request::STATUS_DONE &&
           auth()->user()->can(\RezaDadashi\RolePermissions\Models\Permission::PERMISSION_SUPERVISOR)
        )
            <div class="mt-3 border rounded-2 p-2">
                <form action="{{ route('requests.supervisorNote', $request->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="text-start mb-2">
                        @lang('show_request_supervisor_description')
                        <span class="required">*</span>
                    </div>
                    <input type="hidden" name="request_id" value="{{ $request->id }}">

                    <div class="text-center">
                        <label class="btn btn-upload-file" for="file">@lang('btn_upload_file')</label>
                    </div>

                    <x-input class="d-none file-upload" name="file" type="file" id="file" accept=".doc, .docx" />

                    <div class="file-upload-preview d-none mt-1 d-flex align-items-center">
                        <i class="bi bi-file-check fs-5"></i>
                        <span style="font-size: 10px;" id="filename"></span>
                    </div>

                    <textarea name="supervisor_description"
                              class="form-control mt-2" required>{{ $request->supervisor_description }}</textarea>
                    <button class="btn btn-danger mt-2" type="submit">@lang('show_request_reject_request_as_an_supervisor')</button>

                </form>
            </div>
        @endif

    </div>



</div>
