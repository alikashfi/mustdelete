<div class="col-lg-12 mb-2">
    <div class="card">
        <div class="card border">
            <div class="card-header bg-gradient bg-light">
                <div class="d-flex align-items-center justify-content-between">
                    <span>@lang('show_request_requests_actions')</span>
                    <i class="bi bi-chevron-down cursor-pointer fs-6" data-bs-toggle="collapse" data-bs-target="#panel-toggle-actions" aria-expanded="true"></i>
                </div>
            </div>
            <div class="card-body collapse show" id="panel-toggle-actions" style="">
                <form method="post" class="w-100" action="{{ route('requests.update', $request->id) }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="mb-3">
                                <label for="word-count" class="form-label">
                                    @lang('show_request_word_count_(supervisor)')
                                    <span class="required">*</span>
                                </label>
                                <x-input id="word-count" type="number" name="word_count" value="{{ $request->word_count['supervisor'] }}" />
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="mb-3">
                                <label for="request-price" class="form-label">@lang('show_request_total_amount_toman')</label>
                                <x-input disabled id="request-price" type="number" name="price" value="{{ $request->price }}" />
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="mb-3">
                                <label for="request-dollarPrice" class="form-label">@lang('show_request_total_amount_dollar')</label>
                                <x-input disabled id="request-dollarPrice" type="number" name="price_dollar" value="{{ $request->price_dollar }}" />
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="mb-3">
                                <label for="type-request"
                                       class="form-label">
                                    @lang('request_type')
                                    <span class="required">*</span>
                                </label>
                                <x-select name="type" id="type-request">
                                    <option value="">-- @lang('request_type') --</option>
                                    @foreach(\RezaDadashi\Request\Models\Request::$requests as $requestType)
                                        <option
                                            @if($request->type == $requestType) selected @endif
                                        value="{{ $requestType }}">
                                            @lang($requestType)
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="mb-3">
                                <label for="type-status"
                                       class="form-label">
                                    @lang('request_status')
                                    <span class="required">*</span>
                                </label>
                                <x-select name="status" id="type-status">
                                    <option value="">-- @lang('request_status') --</option>
                                    @foreach(\RezaDadashi\Request\Models\Request::$statuses as $requestStatus)
                                        <option
                                            @if($request->status == $requestStatus) selected @endif
                                        value="{{ $requestStatus }}">
                                            @lang($requestStatus)
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="mb-3">
                                <label for="" class="form-label">@lang('show_request_payment_is_dollar')</label>
                                <div class="form-check form-switch">
                                    <input id="is_dollar" name="is_dollar" value="1" class="form-check-input cursor-pointer" type="checkbox" id="pay-is-dollar" @if($request->is_dollar) checked="" @endif>
                                    <label class="form-check-label cursor-pointer" for="pay-is-dollar">@lang('common_yes')</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="mb-3">
                                <label for="request-deposit-payment" class="form-label">
                                    @lang('show_request_amount_paid_deposit')
                                    <span class="required">*</span>
                                </label>
                                <x-input id="request-deposit-payment" type="number" name="deposit_payment" value="{{ $request->deposit_payment }}" />
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="mb-3">
                                <label for="request-final-payment" class="form-label">
                                    @lang('show_request_amount_paid_final')
                                    <span class="required">*</span>
                                </label>
                                <x-input id="request-final-payment" type="number" name="final_payment" value="{{ $request->final_payment }}" />
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="mb-3">
                                <label for="supervisor-request"
                                       class="form-label">@lang('request_supervisor')</label>
                                <x-select name="supervisor_id" id="supervisor-request">
                                    <option value="">-- @lang('request_supervisor') --</option>
                                    @foreach($supervisors as $supervisorItem)
                                        <option
                                            @if($request->supervisor_id == $supervisorItem->id) selected @endif
                                        value="{{ $supervisorItem->id }}">
                                            {{ $supervisorItem->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="mb-3">
                                <label for="owner-request"
                                       class="form-label">@lang('request_expert')</label>
                                <x-select name="owner_id" id="owner-request">
                                    <option value="">-- @lang('request_expert') --</option>
                                    @foreach($owners as $ownersItem)
                                        <option
                                            @if($request->owner_id == $ownersItem->id) selected @endif
                                        value="{{ $ownersItem->id }}">
                                            {{ $ownersItem->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </div>

                        <div class="text-end">
                            <button type="submit" class="btn btn-primary">@lang('common_update')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
