<div style="height: 100px; overflow: hidden;">
    <svg viewBox="0 0 500 150" preserveAspectRatio="none"
         style="height: 100%; width: 100%;">
        <path
            d="M-0.00,49.85 C130.02,142.20 382.84,-55.62 500.00,49.85 L500.00,149.60 L-0.00,149.60 Z"
            style="stroke: none; fill: #08f;"></path>
    </svg>
</div>
