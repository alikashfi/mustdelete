<div class="col-12 mt-2">
    <div class="card">
        <div class="card border">
            <div class="card-header bg-gradient bg-light">
                <div class="d-flex align-items-center justify-content-between">
                    <span>@lang('show_request_history_title')</span>
                    <i class="bi bi-chevron-down cursor-pointer fs-6" data-bs-toggle="collapse" data-bs-target="#panel-toggle-history" aria-expanded="true"></i>
                </div>
            </div>
            <div class="_card-body collapse" id="panel-toggle-history" style="">
                <ul class="list-group list-group-flush">
                    @foreach($request->histories as $history)
                        <li class="list-group-item d-flex align-items-center">
                            <i class="bi bi-clock text-primary"></i>
                            <span class="text-black-50 px-2">{{ getJalali($history->created_at) }}</span>
                            :
                            <span class="text-success px-2">{{ $history->user->name }}</span>
                            <span class="text-danger"><i class="bi bi-plus-square-dotted"></i></span>
                            <span class="px-2">
                                @php
                                $comment = $history->body;
                                $isChangeHistory = explode(':', $comment);
                                @endphp
                                @if(isset($isChangeHistory[0]) && isset($isChangeHistory[1]))
                                    @lang($isChangeHistory[0] . ":") : {{ $isChangeHistory[1] }}
                                @else
                                    @lang($comment)
                                @endif
                            </span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
