@extends('User::Front.master')

@section('content')
    <div id="content" class="container">

        <div class="row d-flex justify-content-center">

            <div
                class="my-5 col-md-8 col-sm-12 bg-light py-5 px-4
                 d-flex flex-column rounded shadow">

                <div class="text-start fs-6 mb-3">@lang('orders_top_step_title')</div>

                {{-- Step 1 --}}
                <div class="p-3 card w-100">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_1_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_1_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step1" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step1">
                        </div>
                    </div>
                </div>


                {{-- Step 2 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_2_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_2_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step2" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step2">
                        </div>
                    </div>
                </div>


                {{-- Step 3 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_3_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_3_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step3" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step3">
                        </div>
                    </div>
                </div>



                {{-- Step 4 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_4_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_4_description') }}</strong>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                               data-bs-target="#step4" aria-expanded="false"></i>
                        </div>
                        <div class="collapse show border-top border-danger mt-3" id="step4">

                            <div class="row mt-3">
                                <div class="col-md-12 col-sm-12">
                                    <table class="table">
                                        <tbody>
                                            @if(! $request->is_dollar)
                                            <tr class="">
                                                <td scope="row">{{ __('register_step_4_total_order_amount') }}</td>
                                                <td>{{ $request->getFormattedPrice() }} تومان</td>
                                            </tr>
                                            @endif
                                            @if($request->is_dollar)
                                            <tr class="">
                                                <td scope="row">{{ __('register_step_4_total_order_amount_to_dollar') }}</td>
                                                <td>{{ $request->getFormattedPriceDollar() }} $</td>
                                            </tr>
                                            @endif
                                            <tr class="">
                                                <td scope="row">{{ __('register_step_4_deposit_amount_paid') }}</td>
                                                <td>
                                                    {{ $request->getFormattedDepositAmount() }}
                                                    @if($request->is_dollar)
                                                        $
                                                    @else
                                                        تومان
                                                    @endif
                                                </td>
                                            </tr>
                                            @if(!$request->is_dollar)
                                                <tr class="">
                                                    <td scope="row">{{ __('register_step_4_final_amount_payable') }}</td>
                                                    <td>
                                                        {{ $request->getFormattedSettlementPayable() }} تومان
                                                    </td>
                                                </tr>
                                            @else
                                                <tr class="">
                                                    <td scope="row">{{ __('register_step_4_final_amount_payable_dollar') }}</td>
                                                    <td>
                                                        {{ $request->getFormattedSettlementPayableDollar() }} $
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="alert alert-success" role="alert">
                                        <h4 class="alert-heading"><i
                                                class="bi bi-megaphone px-1"></i>{{ __('register_step_3_payment_system') }}
                                        </h4>
                                        <p>{{ __('register_step_3_payment_system_description') }}</p>

                                        @if((new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value == 1)
                                            <p class="mb-0">{{ __('register_step_3_card_to_card_description', ['card_number' => (new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_CARD_NUMBER)->value]) }}</p>
                                        @endif

                                        <hr>

                                        <div id="payment-type-select" class="d-flex justify-content-center">
                                            @if(! $request->is_dollar)
                                            <div class="form-check mx-3">
                                                <input class="form-check-input" type="radio" value="online" name="payment_type" id="online-payment" checked>
                                                <label class="form-check-label cursor-pointer" for="online-payment">
                                                    @lang('register_step_4_online_payment')
                                                </label>
                                            </div>
                                            @endif
                                            @if((new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value == 1)
                                            <div class="form-check mx-3">
                                                <input class="form-check-input" type="radio" value="file" name="payment_type" id="upload-payment-file">
                                                <label class="form-check-label cursor-pointer" for="upload-payment-file">
                                                    @lang('register_step_4_upload_payment_receipts')
                                                </label>
                                            </div>
                                            @endif
                                        </div>

                                        <div id="payment-box">

                                            @if(! $request->is_dollar)
                                            <div id="online" class="text-center mt-3 _d-none">
                                                <form action="{{ route('requests.settlement', $request->id) }}" method="post">
                                                    @csrf
                                                    <button type="submit"
                                                            class="btn btn-primary">{{ __('common_online_payment') }}</button>
                                                </form>
                                            </div>
                                            @endif

                                            <div id="file" class="mt-3 @if(! $request->is_dollar) d-none @endif">
                                                <form action="{{ route('requests.uploadReceipt', $request->id) }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <label for="file-receipt" class="form-label"><i
                                                            class="bi bi-cloud-arrow-up px-1"></i>{{ __('register_step_3_upload_payment_receipts') }}
                                                        <small>{{ __('register_step_3_upload_payment_receipts_extensions') }}</small></label>
                                                    <div class="input-group">
                                                        <input type="file" name="file" class="form-control" id="file-receipt" accept=".jpg, .jpeg, .png">
                                                        <button class="btn btn-success" type="submit">{{ __('common_continue') }}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>



            </div>

        </div>

    </div>
@endsection

@section('js')
@endsection
