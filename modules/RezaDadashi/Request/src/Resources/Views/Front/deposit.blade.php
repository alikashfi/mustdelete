@extends('User::Front.master')

@section('content')
    <div id="content" class="container">

        <div class="row d-flex justify-content-center">

            <div
                class="my-5 col-md-8 col-sm-12 bg-light py-5 px-4
                 d-flex flex-column rounded shadow">

                <div class="text-start fs-6 mb-3">@lang('orders_top_step_title')</div>

                {{-- Step 1 --}}
                <div class="p-3 card w-100">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_1_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_1_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step1" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step1">
                        </div>
                    </div>
                </div>


                {{-- Step 2 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_2_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_2_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step2" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step2">
                        </div>
                    </div>
                </div>


                {{-- Step 3 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">@lang('register_step_3_title')</strong>
                                <strong class="fs-6 fw-300">@lang('register_step_3_description')</strong>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                               data-bs-target="#step3" aria-expanded="false"></i>
                        </div>
                        <div class="collapse show border-top border-danger mt-3" id="step3">

                            <div class="row mt-3">
                                <div class="col-md-12 col-sm-12">
                                    <table class="table xxxd align-middle">
                                        <tbody>
                                        <tr>
                                            <td scope="row">@lang('view_payment_invoice')</td>
                                            <td>
                                                <select class="form-select-sm w-100" id="view_payment_invoice_type">
                                                    <option selected value="toman">@lang('view_payment_invoice_to_toman')</option>
                                                    <option value="dollar">@lang('view_payment_invoice_to_dollar')</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row">@lang('register_step_3_system_word_count')</td>
                                            <td>
                                                {{ number_format($request->word_count['system']) }}
                                                <a onclick="changeWordCountToggler()" class="text-decoration-none" href="javascript:;">@lang('register_step_3_btn_change_word_count')</a>
                                            </td>
                                        </tr>
                                        <tr class=d-none" id="changeWordCountTr">
                                            <td scope="row">@lang('register_step_3_number_words_entered_by_you')</td>
                                            <td>

                                                <div class="input-group input-group-sm">
                                                    <input id="number" type="text" class="form-control" value="{{ number_format($request->word_count['user']) }}">
                                                    <button type="button" onclick="updateWordCount()" class="btn btn-success">
                                                        @lang('common_record')
                                                        <span id="btnLoading" class="spinner-border spinner-border-sm d-none"></span>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- <tr> --}}
                                        {{--     <td scope="row">@lang('register_step_3_percent_discount_code')</td> --}}
                                        {{--     <td id="discountPercent">0 %</td> --}}
                                        {{-- </tr> --}}
                                        <tr>
                                            <td scope="row">@lang('register_step_3_percent_deposit')</td>
                                            <td>{{ (new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PERCENT_DEPOSIT_PAYMENT)->value }} %</td>
                                        </tr>
                                        <tr class="inToman">
                                            <td scope="row">@lang('register_step_3_deposit_amount_total')</td>
                                            <td>{{ $request->getFormattedPrice() }} تومان</td>
                                        </tr>
                                        <tr class="inToman">
                                            <td scope="row">@lang('register_step_3_deposit_amount_payable')</td>
                                            <td id="payableAmount">{{ $request->getFormattedPercent() }} تومان</td>
                                        </tr>
                                        <tr class="inDollar d-none">
                                            <td scope="row">@lang('register_step_3_deposit_amount_total_dollar')</td>
                                            <td>{{ $request->getFormattedPriceDollar() }}  $</td>
                                        </tr>
                                        <tr class="inDollar d-none">
                                            <td scope="row">@lang('register_step_3_deposit_amount_payable_dollar')</td>
                                            <td id="payableAmountDollar">{{ $request->getFormattedPercentDollar() }}  $</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="alert alert-success" role="alert">
                                        <h4 class="alert-heading"><i
                                                class="bi bi-megaphone px-1"></i>{{ __('register_step_3_payment_system') }}
                                        </h4>
                                        <div>{{ __('register_step_3_payment_system_description') }}</div>

                                        @if((new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value == 1)
                                            <p class="mb-0">{{ __('register_step_3_card_to_card_description', [
                                                'card_number' => (new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_CARD_NUMBER)->value,
                                                'name' => 'احمد آقامحمدی عمید'
                                            ]) }}</p>
                                        @endif
                                        <hr>

                                        <div id="payment-type-select" class="d-flex justify-content-center">
                                            @if((new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value == 1)
                                            <div class="form-check mx-3 online_payment">
                                                <input class="form-check-input" type="radio" value="online" name="payment_type" id="online-payment" checked>
                                                <label class="form-check-label cursor-pointer" for="online-payment">
                                @lang('register_step_3_online_payment')
                                                </label>
                                            </div>
                                            @endif
                                            @if((new \RezaDadashi\Setting\Repositories\SettingRepository())->getByKey(\RezaDadashi\Setting\Models\Setting::SETTING_KEY_PAYMENT_FILE_RECEIPT)->value == 1)
                                                <div class="form-check mx-3">
                                                    <input class="form-check-input" type="radio" value="file" name="payment_type" id="upload-payment-file">
                                                    <label class="form-check-label cursor-pointer" for="upload-payment-file">
                                                        @lang('register_step_3_upload_payment_receipts')
                                                    </label>
                                                </div>
                                            @endif
                                        </div>

                                        <div id="payment-box">
                                            <div id="online" class="text-center mt-3 _d-none online_payment">

                                                <form action="{{ route('requests.deposit', $request->id) }}" method="post">
                                                    <div id="discount-alert" class="alert d-none text-start"></div>

{{--                                                    <div class="input-group mb-3">--}}
{{--                                                        <input type="text" id="code" name="code" class="form-control text-end" placeholder="@lang('register_step_3_promo_code')">--}}
{{--                                                        <button onclick="checkDiscountCode()" class="btn btn-success" type="button">--}}
{{--                                                            <span id="loading" class="spinner-border spinner-border-sm d-none"></span>--}}
{{--                                                            @lang('register_step_3_promo_code_apply')--}}
{{--                                                        </button>--}}
{{--                                                    </div>--}}

                                                    @csrf
                                                    <button type="submit"
                                                            class="btn btn-primary">{{ __('common_online_payment') }}</button>
                                                </form>
                                            </div>

                                            <div id="file" class="mt-3 d-none">
                                                <form action="{{ route('requests.uploadReceipt', $request->id) }}" method="post" enctype="multipart/form-data">
                                                    @csrf

                                                    <input id="is_dollar" type="hidden" name="is_dollar" value="">
                                                    <label for="file-receipt" class="form-label"><i
                                                            class="bi bi-cloud-arrow-up px-1"></i>{{ __('register_step_3_upload_payment_receipts') }}
                                                        <small>{{ __('register_step_3_upload_payment_receipts_extensions') }}</small>
                                                    </label>

                                                    <div class="text-center">
                                                        <label class="btn btn-upload-file" for="receipt-file-upload">@lang('btn_upload_file')</label>
                                                    </div>
                                                    <x-input class="d-none file-upload" name="file" type="file" id="receipt-file-upload" accept=".jpg, .jpeg, .png" required />
                                                    <div class="file-upload-preview d-none mt-1 d-flex align-items-center">
                                                        <i class="bi bi-file-check fs-5"></i>
                                                        <span style="font-size: 10px;" id="filename"></span>
                                                    </div>

                                                    <div class="text-center mt-2">
                                                        <button class="btn btn-success" type="submit">{{ __('common_continue') }}</button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                {{-- Step 4 --}}
                {{-- <div class="p-3 card w-100 mt-3"> --}}
                {{--     <div> --}}
                {{--         <div class="d-flex align-items-center justify-content-between"> --}}
                {{--             <div class="text-black"> --}}
                {{--                 <i class="step-circle"></i> --}}
                {{--                 <strong class="fs-6 fw-500 mx-2">{{ __('register_step_4_title') }}</strong> --}}
                {{--                 <strong class="fs-6 fw-300">{{ __('register_step_4_description') }}</strong> --}}
                {{--             </div> --}}
                {{--              --}}{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
                {{--              --}}{{--                               data-bs-target="#step4" aria-expanded="false"></i>--}}
                {{--         </div> --}}
                {{--         <div class="collapse border-top border-danger mt-3" id="step4"> --}}
                {{--         </div> --}}
                {{--     </div> --}}
                {{-- </div> --}}
            </div>

        </div>

    </div>
@endsection

@section('js')
<script>
    function changeWordCountToggler()
    {
        $('.table').find('tr#changeWordCountTr').toggleClass('d-none');
    }

    function updateWordCount()
    {
        let _this = $(this);
        const number = $('#number').val();
        const url = '{{ route('requests.changeUserEnteredWordCount', ["request", $request->id]) }}';
        if(number) {
            $('#btnLoading').removeClass('d-none');

            $.ajax({
                type: 'POST',
                url: url,
                dataType : 'json',
                data: {
                    _method: "POST",
                    _token: '{{ csrf_token() }}',
                    number: number,
                    request_id: '{{ $request->id }}'
                },
                success: function(data, textStatus, xhr)
                {
                    $('#btnLoading').addClass('d-none');

                    if (xhr.status == 200) {
                        notification(data.message);
                        $(_this).closest('tr').remove();
                    }
                },
                error:function(req, status, error)
                {
                    $('#btnLoading').addClass('d-none');
                    notification('خطایی رخ داد!', 'error');
                }
            });
        }
    }

    function checkDiscountCode()
    {
        let _this = $(this);
        const code = $('#code').val();
        const url = '{{ route('discounts.check', ["code", $request->id]) }}';
        if(code) {
            $('#loading').removeClass('d-none');
            $.get(url.replace("code", code))
            .done(function (data) {
                $('#loading').addClass('d-none');
                $('#discount-alert')
                    .removeClass('d-none alert-danger')
                    .addClass('alert-info')
                    .text('تخفیف با موفقیت اعمال شد');
                $('#discountPercent').text(data.discountPercent + '%');
                $('#payableAmount').text(data.payableAmount + ' تومان');
                $('#payableAmountDollar').text(data.payableAmountDollar + ' $');
            })
            .fail(function (data) {
                $('#loading').addClass('d-none');
                $('#discount-alert')
                    .removeClass('d-none alert-info')
                    .addClass('alert-danger')
                    .text('کد تخفیف وارد شده معتبر نیست');
            });
        }
    }

    /* region change payment invoice type */
    $(document).on('change', '#view_payment_invoice_type', function(e) {
        let select = $(this);
        if(select.val() == 'dollar') {
            $(select).closest('table').find('tr.inToman').addClass('d-none');
            $(select).closest('table').find('tr.inDollar').removeClass('d-none');

            $('#upload-payment-file').prop('checked', true);
            $('#payment-box #online').addClass('d-none');
            $('#payment-box #file').removeClass('d-none');
            $('#is_dollar').val('1');
            $('.online_payment').hide();
        } else {
            $(select).closest('table').find('tr.inToman').removeClass('d-none');
            $(select).closest('table').find('tr.inDollar').addClass('d-none');

            $('#online-payment').prop('checked', true);
            $('#payment-box #file').addClass('d-none');
            $('#payment-box #online').removeClass('d-none');
            $('#is_dollar').val('0');
            $('.online_payment').show();
        }
    });
    /* endregion change payment invoice type */
</script>
@endsection
