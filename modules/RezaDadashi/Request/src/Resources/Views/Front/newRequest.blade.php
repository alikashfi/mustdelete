@extends('User::Front.master')

@section('content')
    <div id="content" class="container">

        <div class="row d-flex justify-content-center">

            <div
                class="my-5 col-md-8 col-sm-12 bg-light py-5 px-4
                 d-flex flex-column rounded shadow">

                <div class="text-start fs-6 mb-3">@lang('orders_top_step_title')</div>

                {{-- Step 1 --}}
                <div class="p-3 card w-100">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_1_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_1_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step1" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step1">
                        </div>
                    </div>
                </div>


                {{-- Step 2 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_2_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_2_description') }}</strong>
                            </div>
                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"
                               data-bs-target="#step2" aria-expanded="false"></i>
                        </div>
                        <div class="collapse show border-top border-danger mt-3" id="step2" style="">

                            <form action="{{ route('requests.store') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="row pt-3">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="file" class="form-label"><i
                                                    class="bi bi-cloud-arrow-up px-1"></i>{{ __('register_step_2_file_title') }}
                                                <small>{{ __('register_step_2_file_title_extensions') }}</small>
                                                <span class="required">*</span>
                                            </label>

                                            <div class="text-center">
                                                <label class="btn btn-upload-file" for="file">@lang('btn_upload_file')</label>
                                            </div>

                                            <x-input class="d-none file-upload" name="file" type="file" id="file" accept=".doc, .docx" required />

                                            <div class="file-upload-preview d-none mt-1 d-flex align-items-center">
                                                <i class="bi bi-file-check fs-5"></i>
                                                <span style="font-size: 10px;" id="filename"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="type-request"
                                                   class="form-label">{{ __('register_step_2_request_type') }}
                                                <span class="required">*</span>
                                            </label>
                                            <x-select name="type" id="type-request" class="form-select" required>
                                                @foreach(\RezaDadashi\Request\Models\Request::$requests as $requestType)
                                                    <option
                                                        @if(old('type') == $requestType) selected @endif
                                                    value="{{ $requestType }}">
                                                        @lang($requestType)
                                                    </option>
                                                @endforeach
                                            </x-select>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-end">
                                    <button class="btn btn-primary" type="submit">@lang('common_continue')</button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>


                {{-- Step 3 --}}
                <div class="p-3 card w-100 mt-3">
                    <div>
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="text-black">
                                <i class="step-circle"></i>
                                <strong class="fs-6 fw-500 mx-2">{{ __('register_step_3_title') }}</strong>
                                <strong class="fs-6 fw-300">{{ __('register_step_3_description') }}</strong>
                            </div>
{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
{{--                               data-bs-target="#step3" aria-expanded="false"></i>--}}
                        </div>
                        <div class="collapse border-top border-danger mt-3" id="step3">
                        </div>
                    </div>
                </div>

                {{-- Step 4 --}}
                {{-- <div class="p-3 card w-100 mt-3"> --}}
                {{--     <div> --}}
                {{--         <div class="d-flex align-items-center justify-content-between"> --}}
                {{--             <div class="text-black"> --}}
                {{--                 <i class="step-circle"></i> --}}
                {{--                 <strong class="fs-6 fw-500 mx-2">{{ __('register_step_4_title') }}</strong> --}}
                {{--                 <strong class="fs-6 fw-300">{{ __('register_step_4_description') }}</strong> --}}
                {{--             </div> --}}
                {{--              --}}{{--                            <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse"--}}
                {{--              --}}{{--                               data-bs-target="#step4" aria-expanded="false"></i>--}}
                {{--         </div> --}}
                {{--         <div class="collapse border-top border-danger mt-3" id="step4"> --}}
                {{--         </div> --}}
                {{--     </div> --}}
                {{-- </div> --}}
            </div>

        </div>

    </div>
@endsection
