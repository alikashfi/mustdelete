<?php

namespace RezaDadashi\Request\Providers;

use Illuminate\Support\Facades\Gate;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Policies\RequestPolicy;
use RezaDadashi\RolePermissions\Models\Permission;

class RequestServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/request_routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Request');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');

        Gate::policy(Request::class, RequestPolicy::class);
    }

    public function boot()
    {
        config()->set('sidebar.items.requests', [
            'icon' => 'bi bi-briefcase-fill',
            'title' => ['fa' => 'درخواست ها', 'en' => 'Requests'],
            'url' => route('requests.index'),
            'permission' => [
                Permission::PERMISSION_SUPERVISOR,
                Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH,
                Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN,
                Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR,
                Permission::PERMISSION_MANAGE_REQUESTS
            ]
        ]);

        config()->set('sidebar.items.my-requests', [
            'icon' => 'bi bi-bag-check-fill',
            'title' => ['fa' => 'درخواست های من', 'en' => 'My Requests'],
            'url' => route('requests.my'),
        ]);

        config()->set('sidebar.items.reports', [
            'icon' => 'bi bi-activity',
            'title' => ['fa' => 'گزارشگیری', 'en' => 'Reports'],
            'url' => route('reports.request'),
            'permission' => [
                Permission::PERMISSION_SUPER_ADMIN,
            ]
        ]);
    }
}
