<?php

namespace RezaDadashi\Request\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Request\Models\History;

class HistoryRepository extends Repository
{
    public function model()
    {
        return History::class;
    }

    public function addHistory($requestId, $body)
    {
        $this->create([
            'user_id' => auth()->id(),
            'request_id' => $requestId,
            'body' => $body,
        ]);
    }
}
