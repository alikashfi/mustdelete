<?php

namespace RezaDadashi\Request\Repositories;

use Illuminate\Support\Facades\DB;
use RezaDadashi\Core\Repositories\Repository;
use RezaDadashi\Request\Models\Request;
use RezaDadashi\Request\Services\RequestService;
use RezaDadashi\RolePermissions\Models\Permission;

class RequestRepository extends Repository
{
    public function model()
    {
        return Request::class;
    }

    public function persianToEnglish()
    {
        $this->query->orWhere('type', Request::REQUEST_PERSIAN_TO_ENGLISH);
        return $this;
    }

    public function englishToPersian()
    {
        $this->query->orWhere('type', Request::REQUEST_ENGLISH_TO_PERSIAN);
        return $this;
    }

    public function englishEditing()
    {
        $this->query->orWhere('type', Request::REQUEST_ENGLISH_EDITING);
        return $this;
    }

    public function getPermissionByRequestType(Request $request)
    {
        if ($request->type == Request::REQUEST_PERSIAN_TO_ENGLISH) {
            return Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH;
        }
        if ($request->type == Request::REQUEST_ENGLISH_TO_PERSIAN) {
            return Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN;
        }
        if ($request->type == Request::REQUEST_ENGLISH_EDITING) {
            return Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR;
        }
        return false;
    }

    public function updateRequest($requestId, $data)
    {
        $oldRequest = $this->findOrFail($requestId);
        $dataUpdate = [];


        if ($data->word_count) {
            $dataUpdate['word_count->supervisor'] = $data->word_count;
            $totalAmount = RequestService::calculationPrice($data->type, $data->word_count);
            $dataUpdate['price'] = $totalAmount['toman'];
            $dataUpdate['price_dollar'] = $totalAmount['dollar'];
        }

        //should be enter
        $dataUpdate['deposit_payment'] = $data->deposit_payment;

        //request update
        $data['final_payment'] ? $dataUpdate['final_payment'] = $data->final_payment : $dataUpdate['final_payment'] = $oldRequest->final_payment;
        $data['is_dollar'] ? $dataUpdate['is_dollar'] = $data->is_dollar : $dataUpdate['is_dollar'] = 0;
        $data['type'] ? $dataUpdate['type'] = $data->type : $oldRequest->type;
        $data['status'] ? $dataUpdate['status'] = $data->status : $oldRequest->status;
        $data['supervisor_id'] ? $dataUpdate['supervisor_id'] = $data->supervisor_id : $oldRequest->supervisor_id;
        $data['owner_id'] ? $dataUpdate['owner_id'] = $data->owner_id : $oldRequest->owner_id;

        $oldRequest->update($dataUpdate);
    }

    public function changeStatus($id, $status)
    {
        return Request::query()
            ->where('id', $id)
            ->update([
                'status' => $status
            ]);
    }

    public function getRequestsCountByStatus($status = null)
    {
        $query = Request::query();
        if (!is_null($status)) {
            $query = $query->where('status', $status);
        }
        $query = $query->count();
        return $query;
    }

    public function getRequestsCount()
    {
        $query = Request::query()
            ->selectRaw('type, count(*) as count')
            ->groupBy('type')
            ->get()
            ->toArray();

        $types = [];
        foreach ($query as $key => $type) {
            $types[$type['type']] = $type['count'];
        }

        return $types;
    }

    public function getByUserId($userId)
    {
        $this->query
            ->where('user_id', $userId)
            ->paginate();
        return $this;
    }

    public function searchId($id = null)
    {
        if ($id)
            $this->query->where('id', $id);
        return $this;
    }

    public function searchEmail($email = null)
    {
        if ($email)
            $this->query->whereHas('user', function ($query) use($email) {
                return $query->where('email', 'like', '%'. $email .'%');
            });
        return $this;
    }

    public function searchMobile($mobile = null)
    {
        if ($mobile)
            $this->query->whereHas('user', function ($query) use($mobile) {
                return $query->where('mobile', $mobile);
            });
        return $this;
    }

    public function searchSupervisor($supervisor = null)
    {
        if ($supervisor)
            $this->query->whereHas('supervisor', function ($query) use($supervisor) {
                return $query->where('name', 'like', '%'. $supervisor .'%');
            });
        return $this;
    }

    public function searchExpert($expert = null)
    {
        if ($expert)
            $this->query->whereHas('owner', function ($query) use($expert) {
                return $query->where('name', 'like', '%'. $expert .'%');
            });
        return $this;
    }

    public function searchType($type = null)
    {
        if ($type)
            $this->query->where('type', $type);
        return $this;
    }

    public function searchStatus($status = null)
    {
        if ($status)
            $this->query->where('status', $status);
        return $this;
    }

    public function getMyDoingRequestsByUserId($userId)
    {
        return Request::query()
            ->where(function ($query) use($userId) {
                return $query->where('supervisor_id', $userId)
                    ->orWhere('owner_id', $userId);
            })
            ->where(function ($query) {
                return $query->whereNotIn('status', [
                    Request::STATUS_SETTLEMENT,
                    Request::STATUS_CLOSED,
                ]);
            })
            ->get();
    }

    public function exportExcel(array $request = [])
    {
        return Request::query()
            ->where(function ($query) use($request) {
                if (isset($request['start_date']))
                    $query->whereDate('created_at', '>=', $request['start_date']);
                if (isset($request['end_date']))
                    $query->whereDate('created_at', '<=', $request['end_date']);

                return $query;
            })
            ->get();
    }
}
