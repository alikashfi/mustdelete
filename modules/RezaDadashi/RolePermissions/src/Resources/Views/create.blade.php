<div class="p-3 card">
    <div class="card border-primary">
        <div class="card-header bg-primary text-white">
            @lang('create_new_role')
        </div>
        <div class="card-body">
            <form action="{{ route('role-permissions.store') }}" method="post" autocomplete="off">
                @csrf
                <div class="my-2">
                    <label for="title" class="form-label">
                        @lang('role_title')
                        <span class="required">*</span>
                    </label>
                    <x-input type="text" value="{{ old('name') }}" name="name" class="text-end" />
                </div>
                <div class="bg-light p-3 border">
                    @lang('role_select_permissions')
                    <span class="required">*</span>
                </div>
                <div class="my-3">
                    @foreach($permissions as $permission)
                        <div class="form-check">
                            <input name="permissions[{{ $permission->name }}]" value="{{ $permission->name }}"
                                   class="form-check-input @error('permissions') is-invalid @enderror" type="checkbox"
                                   id="permission_{{ $permission->id }}"
                                   @if(is_array(old('permissions')) && array_key_exists($permission->name, old('permissions'))) checked @endif
                            >
                            <label class="form-check-label"
                                   for="permission_{{ $permission->id }}">@lang($permission->name)</label>
                            @error('permissions')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    @endforeach
                </div>

                <div class="text-end">
                    <button type="submit" class="btn btn-outline-dark mt-2">@lang('common_add')</button>
                </div>
            </form>
        </div>
    </div>
</div>
