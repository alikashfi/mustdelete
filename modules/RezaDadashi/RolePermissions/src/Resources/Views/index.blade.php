@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('role-permissions.index') }}">@lang('create_permissions_title')</a></li>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12 col-lg-8">


            <div class="p-3 card">
                <div>
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <div>
                            <span class="bi bi-list"></span>
                            <span>@lang('create_permissions_title')</span>
                        </div>
                        <i class="bi bi-chevron-down cursor-pointer fs-6 collapsed" data-bs-toggle="collapse" data-bs-target="#groups-toggle" aria-expanded="false"></i>
                    </div>
                    <div class="collapse show border-top" id="groups-toggle">
                        <div class="table-responsive pt-2">
                            <table class="table table-striped align-middle">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">@lang('role_permissions_table_role')</th>
                                        <th scope="col">@lang('role_permissions_table_permissions')</th>
                                        <th scope="col">@lang('common_actions')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($roles as $role)
                                        <tr>
                                            <th scope="row">{{ $role->id }}</th>
                                            <td>@lang($role->name)</td>
                                            <td>
                                                <ul class="list-unstyled">
                                                    @foreach($role->permissions as $permission)
                                                        <li>
                                                            <span class="badge bg-success">@lang($permission->name)</span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>
                                                <a class="bi bi-pencil-square text-success fs-6" href="{{ route('role-permissions.edit', $role->id) }}"></a>
                                                @if($role->name != \RezaDadashi\RolePermissions\Models\Role::ROLE_SUPER_ADMIN)
                                                    <a data-route="{{ route('role-permissions.destroy', $role->id) }}" class="deleteItem bi bi-trash text-danger fs-6 px-3" href="javascript:;"></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-4 mt-3 mt-lg-0">

            @include('RolePermissions::create')

        </div>

    </div>
@endsection
