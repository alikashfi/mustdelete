@extends('Dashboard::master')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('role-permissions.index') }}">نقش های کاربری</a></li>
    <li class="breadcrumb-item"><a href="#">ویرایش نقش کاربری</a></li>
@endsection

@section('content')
<div class="p-3 card">
    <div class="card border-dark">
        <div class="card-header bg-dark text-white">
            ویرایش نقش کاربری
        </div>
        <div class="card-body">
            <form action="{{ route('role-permissions.update', $role->id) }}" method="post">
                @csrf
                @method('PATCH')
                <input type="hidden" name="id" value="{{ $role->id }}">
                <div class="my-2">
                    <label for="title" class="form-label">عنوان</label>
                    <x-input disabled name="name" value="{{ $role->name }}" type="text" class="text-end" id="title" />
                </div>
                <div class="bg-light p-3 border">انتخاب مجوز ها</div>

                <div class="my-3">
                    @foreach($permissions as $permission)
                        <div class="form-check form-switch">
                            <input name="permissions[{{ $permission->name }}]" value="{{ $permission->name }}"
                                   class="form-check-input @error('permissions') is-invalid @enderror" type="checkbox"
                                   id="permission_{{ $permission->id }}"
                                    @if($role->hasPermissionTo($permission->name)) checked @endif
                                    >
                            <label class="form-check-label"
                                   for="permission_{{ $permission->id }}">@lang($permission->name)</label>
                            @error('permissions')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    @endforeach
                </div>

                <div class="text-end">
                    <button type="submit" class="btn btn-outline-dark mt-2">بروزرسانی</button>
                    <a href="{{ route('role-permissions.index') }}" class="btn btn-outline-danger mt-2">انصراف</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
