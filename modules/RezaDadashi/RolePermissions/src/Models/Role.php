<?php

namespace RezaDadashi\RolePermissions\Models;

class Role extends \Spatie\Permission\Models\Role
{
    /** Roles */

    const ROLE_SUPER_ADMIN = 'super admin';
    const ROLE_SUPERVISOR = 'supervisor';
    const ROLE_TRANSLATOR_PERSIAN_TO_ENGLISH = 'translator persian to english';
    const ROLE_TRANSLATOR_ENGLISH_TO_PERSIAN = 'translator english to persian';
    const ROLE_TRANSLATOR_ENGLISH_EDITOR = 'english editor';
    const ROLE_MANAGE_REQUESTS = 'manage requests';
    const ROLE_MANAGE_QUIZZES = 'manage quizzes';

    public static $roles = [
        self::ROLE_SUPER_ADMIN => [ // سوپر ادمین
            Permission::PERMISSION_SUPER_ADMIN,
            Permission::PERMISSION_SUPERVISOR,
            Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH,
            Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN,
            Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR,
        ],
        self::ROLE_SUPERVISOR => [ // ناظر
            Permission::PERMISSION_SUPERVISOR,
        ],
        self::ROLE_TRANSLATOR_PERSIAN_TO_ENGLISH => [ // مترجم فارسی به انگلیسی
            Permission::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH
        ],
        self::ROLE_TRANSLATOR_ENGLISH_TO_PERSIAN => [ // مترجم انگلیسی به فارسی
            Permission::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN
        ],
        self::ROLE_TRANSLATOR_ENGLISH_EDITOR => [ // ویراستار انگلیسی
            Permission::PERMISSION_TRANSLATOR_ENGLISH_EDITOR
        ],
        self::ROLE_MANAGE_REQUESTS => [ // مدیریت درخواست ها
            Permission::PERMISSION_MANAGE_REQUESTS
        ],
        self::ROLE_MANAGE_QUIZZES => [ // مدیریت آزمون ها
            Permission::PERMISSION_MANAGE_QUIZZES
        ],
    ];
}
