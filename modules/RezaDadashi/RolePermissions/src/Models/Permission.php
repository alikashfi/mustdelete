<?php

namespace RezaDadashi\RolePermissions\Models;

class Permission extends \Spatie\Permission\Models\Permission
{
    /** Permissions */
    const PERMISSION_SUPER_ADMIN = 'super admin';
    const PERMISSION_SUPERVISOR = 'supervisor';
    const PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH = 'translator persian to english';
    const PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN = 'translator english to persian';
    const PERMISSION_TRANSLATOR_ENGLISH_EDITOR = 'english editor';
    const PERMISSION_MANAGE_ROLE_PERMISSION = 'manage role_permissions';
    const PERMISSION_MANAGE_USERS = 'manage users';
    const PERMISSION_MANAGE_REQUESTS = 'manage requests';
    const PERMISSION_MANAGE_PAYMENTS = 'manage payments';
    const PERMISSION_MANAGE_SETTINGS = 'manage settings';
    const PERMISSION_MANAGE_DISCOUNTS = 'manage discounts';
    const PERMISSION_MANAGE_QUIZZES = 'manage quizzes';
    const PERMISSION_MANAGE_SETTLEMENT_PARTNERS = 'manage settlement partners';



    public static $permissions = [
        self::PERMISSION_SUPER_ADMIN, // سوپر ادمین
        self::PERMISSION_SUPERVISOR, // ناظر
        self::PERMISSION_TRANSLATOR_PERSIAN_TO_ENGLISH, // مترجم فارسی به انگلیسی
        self::PERMISSION_TRANSLATOR_ENGLISH_TO_PERSIAN, // مترجم انگلیسی به فارسی
        self::PERMISSION_TRANSLATOR_ENGLISH_EDITOR, // ویراستار انگلیسی
        self::PERMISSION_MANAGE_ROLE_PERMISSION, // مدیریت نقش های کاربری
        self::PERMISSION_MANAGE_USERS, // مدیریت کاربران
        self::PERMISSION_MANAGE_REQUESTS, // مدیریت درخواست ها
        self::PERMISSION_MANAGE_PAYMENTS, // مدیریت تراکنش ها
        self::PERMISSION_MANAGE_SETTINGS, // مدیریت تنظیمات
        self::PERMISSION_MANAGE_DISCOUNTS, // مدیریت تخفیف ها
        self::PERMISSION_MANAGE_QUIZZES, // مدیریت آزمون ها
        self::PERMISSION_MANAGE_SETTLEMENT_PARTNERS, // مدیریت تسویه با همکاران
    ];
}
