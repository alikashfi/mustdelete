<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web', 'auth', 'verified']
], function ($router) {
    $router->resource('/role-permissions', \RezaDadashi\RolePermissions\Http\Controllers\RolePermissionsController::class);
});
