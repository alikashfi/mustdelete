<?php

namespace RezaDadashi\RolePermissions\Providers;

use RezaDadashi\RolePermissions\Database\Seeds\RolePermissionTableSeeder;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\RolePermissions\Models\Role;
use RezaDadashi\RolePermissions\Policies\RolePermissionPolicy;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class RolePermissionsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/role_permissions_routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'RolePermissions');
        $this->loadJsonTranslationsFrom(__DIR__ . '/../Resources/Lang');

        //Attach RolePermission Seeder to Main Seeder
        DatabaseSeeder::$seeders[] = RolePermissionTableSeeder::class;

        //Policies
        Gate::policy(Role::class, RolePermissionPolicy::class);

        Gate::before(function ($user) {
            return $user->hasPermissionTo(Permission::PERMISSION_SUPER_ADMIN) ? true : null;
        });
    }

    public function boot()
    {
        config()->set('models.permission', \RezaDadashi\RolePermissions\Models\Permission::class);
        config()->set('models.role', \RezaDadashi\RolePermissions\Models\Role::class);

        config()->set('sidebar.items.role-permissions', [
            'icon' => 'bi bi-shield-fill-check',
            'title' => ['fa' => 'نقش های کاربری', 'en' => 'Roles & Permissions'],
            'url' => route('role-permissions.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_ROLE_PERMISSION
            ]
        ]);
    }
}
