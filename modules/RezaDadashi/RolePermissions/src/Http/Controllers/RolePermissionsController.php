<?php

namespace RezaDadashi\RolePermissions\Http\Controllers;

use App\Http\Controllers\Controller;
use RezaDadashi\Core\Responses\AjaxResponses;
use RezaDadashi\RolePermissions\Http\Requests\RoleRequest;
use RezaDadashi\RolePermissions\Http\Requests\RoleUpdateRequest;
use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\RolePermissions\Models\Role;
use RezaDadashi\RolePermissions\Repositories\PermissionRepository;
use RezaDadashi\RolePermissions\Repositories\RoleRepository;

class RolePermissionsController extends Controller
{
    private $rolesRepository;
    private $permissionRepository;

    public function __construct(RoleRepository $rolesRepository, PermissionRepository $permissionRepository)
    {
        $this->rolesRepository = $rolesRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        /** Check Permission */
        $this->authorize('manage', Role::class);

        $roles = $this->rolesRepository->all();
        $permissions = $this->permissionRepository->all();
        return view('RolePermissions::index', compact('roles', 'permissions'));
    }

    public function store(RoleRequest $request)
    {
        /** Check Permission */
        $this->authorize('manage', Role::class);

        $this->rolesRepository->create([
            'name' => $request->name
        ])->syncPermissions($request->permissions);

        newFeedback();
        return redirect()->to(route('role-permissions.index'));
    }


    public function edit($roleId)
    {
        /** Check Permission */
        $this->authorize('manage', Role::class);

        $role = $this->rolesRepository->findOrFail($roleId);
        $permissions = $this->permissionRepository->all();
        return view('RolePermissions::edit', compact('role', 'permissions'));
    }

    public function update(RoleUpdateRequest $request, $roleId)
    {
        /** Check Permission */
        $this->authorize('manage', Role::class);

        $this->rolesRepository->updateRole($roleId, $request);
        return redirect()->route('role-permissions.index');
    }

    public function destroy($roleId)
    {
        /** Check Permission */
        $this->authorize('manage', Role::class);

        $role = $this->rolesRepository->findOrFail($roleId);
        $this->rolesRepository->delete($role);
        return AjaxResponses::successResponse();
    }
}
