<?php

namespace RezaDadashi\RolePermissions\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|unique:roles,name',
            'permissions' => 'required|array|min:1'
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'name' => 'عنوان',
            'permissions' => 'مجوز ها',
        ] : [];
    }
}
