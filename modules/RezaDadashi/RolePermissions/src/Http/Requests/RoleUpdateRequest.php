<?php

namespace RezaDadashi\RolePermissions\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class RoleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:roles,id',
            'name' => 'nullable|min:3|unique:roles,name,' . request()->id,
            'permissions' => 'required|array|min:1'
        ];
    }

    public function attributes()
    {
        return App::getLocale() == 'fa' ? [
            'id' => 'شناسه',
            'name' => 'عنوان',
            'permissions' => 'مجوزها',
        ] : [];
    }
}
