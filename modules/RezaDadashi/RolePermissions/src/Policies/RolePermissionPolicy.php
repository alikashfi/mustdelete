<?php

namespace RezaDadashi\RolePermissions\Policies;

use RezaDadashi\RolePermissions\Models\Permission;
use RezaDadashi\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage(User $user)
    {
        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_ROLE_PERMISSION);
    }
}
