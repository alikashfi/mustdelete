<?php

namespace RezaDadashi\RolePermissions\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use Spatie\Permission\Models\Permission;

class PermissionRepository extends Repository
{
    public function model()
    {
        return Permission::class;
    }
}
