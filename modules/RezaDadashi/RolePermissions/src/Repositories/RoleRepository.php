<?php

namespace RezaDadashi\RolePermissions\Repositories;

use RezaDadashi\Core\Repositories\Repository;
use Spatie\Permission\Models\Role;

class RoleRepository extends Repository
{
    public function model()
    {
        return Role::class;
    }

    public function updateRole($roleId, $request)
    {
        $role = $this->find($roleId);
        //return $role->syncPermissions($request->permissions)->update(['name' => $request->name]);
        return $role->syncPermissions($request->permissions);
    }
}
