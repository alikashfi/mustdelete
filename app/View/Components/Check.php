<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Check extends Component
{
    public $name;
    public $placeholder;
    public $select;
    /**
     * @var null
     */
    public $class;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $placeholder
     * @param $select
     * @param $class
     */
    public function __construct($name, $placeholder, $select = null, $class = null)
    {
        $this->name = $name;
        $this->placeholder = $placeholder;
        $this->select = $select;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.check');
    }
}
